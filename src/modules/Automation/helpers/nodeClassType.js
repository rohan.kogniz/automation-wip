const nodeClassType = (type, styles) => {
	switch (type) {
		default:
			return;
		case "signal":
			return styles.signal;
		case "activity":
			return styles.activity;
		case "trigger":
			return styles.trigger;
		case "flow":
			return styles.flow;
	}
};

export { nodeClassType as default }