import {
    getNodeChildren,
	getLoopExitNode,
	isLoopBreakNode,
} from './layoutHelpers';
import toTree from './testUtilities/automationTreeFromSchemaAlt'

test("getNodeChildren: null", () => {
	const n = {};
	const x = toTree({ nodes: n });
	expect(getNodeChildren(x)).toEqual([]);
});

test("getNodeChildren: noChildren", () => {
	const n = {
		id: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
		children: [],
	};
	const x = toTree({ nodes: n });
	expect(getNodeChildren(x)).toEqual([]);
});

test("getNodeChildren: oneChild", () => {
	const n = {
		id: "0",
		children: [
			{
				id: "1",
			},
		],
	};
	const x = toTree({ nodes: n });
	expect(getNodeChildren(x).length).toEqual(1);
});

test("getNodeChildren: oneChild has exit node", () => {
	const n = {
		id: "0",
		children: [
			{
				id: "1",
				exit: true,
			},
		],
	};
	const x = toTree({ nodes: n });
	expect(getNodeChildren(x).length).toEqual(0);
});

test("getLoopExitNode: null", () => {
	const n = {};
	const x = toTree({ nodes: n });
	expect(getLoopExitNode(x)).toEqual(undefined);
});

test("getLoopExitNode: one exit node", () => {
	const n = {
		id: "0",
		children: [
			{
				id: "1",
				exit: true,
			},
		],
	};
	const x = toTree({ nodes: n });
	expect(getLoopExitNode(x)).not.toEqual(undefined);
});

test("getLoopExitNode: 0 exit node", () => {
	const n = {
		id: "0",
		children: [
			{
				id: "1",
			},
		],
	};
	const x = toTree({ nodes: n });
	expect(getLoopExitNode(x)).toEqual(undefined);
});

test("isLoopBreakNode: null", () => {
	const n = {};
	const x = toTree({ nodes: n });
	expect(isLoopBreakNode(x)).toEqual(false);
});

test("isLoopBreakNode: true", () => {
	const n = {
		id: "1",
		break: true,
	};
	const x = toTree({ nodes: n });
	expect(isLoopBreakNode(x)).toEqual(true);
});

test("isLoopBreakNode: false", () => {
	const n = {
		id: "1",
		break: false,
	};
	const x = toTree({ nodes: n });
	expect(isLoopBreakNode(x)).toEqual(false);
});

test("isLoopBreakNode: false 2", () => {
	const n = {
		id: "1",
	};
	const x = toTree({ nodes: n });
	expect(isLoopBreakNode(x)).toEqual(false);
});
