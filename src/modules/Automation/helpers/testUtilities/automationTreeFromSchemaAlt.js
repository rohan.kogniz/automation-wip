const makeNode = (subtree) => {
	let { id } = subtree;
	let x = document.createElement("x");
	x.setAttribute("data-node-id", id);
	if (subtree.exit) {
		x.setAttribute("data-exit", subtree.exit);
	}
	if (subtree.break) {
		x.setAttribute("data-break", subtree.break);
	}
	return x;
};

// toTree: use DOM to implement a tree datastructure
const toTree = ({ head, nodes }) => {
	if (nodes) {
		let { children } = nodes;
		let x = makeNode(nodes);
		if (head) {
			head.appendChild(x);
		} else {
			head = x;
		}

		if (children) {
			children.map((item) => {
				return toTree({ head: x, nodes: item });
			});
		}
	}
	return head;
};

export { toTree as default };
