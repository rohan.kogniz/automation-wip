export const hasNodeAttribute = (treeNode, attrib) => {
	const val = treeNode.getAttribute(attrib);
	return !!val;
};

export const getNodeChildren = (treeNode) => {
	return Array.prototype.filter.call(treeNode.children, (v) => {
		return !hasNodeAttribute(v, "data-exit");
	});
};


export const getLoopExitNode = (treeNode) => {
	let v = Array.prototype.filter.call(treeNode.children, (v) => {
		return hasNodeAttribute(v, "data-exit");
	});
	if (v && v.length > 0) {
		return v[0];
	}
};

export const isLoopBreakNode = (treeNode) => {
	return hasNodeAttribute(treeNode, "data-break")
}
