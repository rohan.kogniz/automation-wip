import React from "react";
import classname from "classnames";
import styles from "./style.module.scss";

const LayoutAlgorithm = ({
	automationTree: data,
	renderNode,
	getNodeChildren,
	getLoopExitNode,
	isLoopBreakNode,
}) => {
	let count = 0;

	const isLeaf = (node) => {
		// repeat nodes that contain empty loops can never be
		// exit nodes
		if (getLoopExitNode(node)) {
			return false;
		}
		return getNodeChildren(node).length === 0;
	};

	const ConditionalWrap = ({ condition, wrapper, children }) =>
		condition ? wrapper(children) : children;

	// dfs: do a post order traversal
	const dfs = (node, hasSiblings = false) => {
		let children = getNodeChildren(node);
		let exitNode = getLoopExitNode(node);
		const numberOfChildren = children.length;

		console.log("node", node);
		console.log("children", children);
		console.log("exitNode", exitNode);
		console.log("numberOfChildren", numberOfChildren);
		console.log("isLeaf", isLeaf(node));

		count = count + 1;
		return (
			<React.Fragment key={count}>
				<div className={styles.rows}>
					{hasSiblings && (
						<div className={styles.vConnectorLeft}></div>
					)}
					{/* Leading horizontal connector */}
					<div className={styles.hConnector}></div>
					{/* Render current node */}
					{renderNode(node)}
					{/* Trailing horizontal connector */}
					<div
						className={classname(styles.hConnector, {
							[styles.dashed]: isLoopBreakNode(node),
							[styles.grow]: isLeaf(node),
						})}
					></div>

					{!isLeaf(node) && (
						<>
							{/* If the current node is of type repeat, wrap the children in a repeatBlock div */}
							<ConditionalWrap
								condition={exitNode}
								wrapper={(children) => (
									<div className={styles.repeatBlock}>
										{/* add a space between the left border of repeatBlock div and vConnectorLeft */}
										{numberOfChildren > 1 && (
											<div
												className={styles.hConnector}
											></div>
										)}
										{/* add arrows on the top border and bottom border of repeatBlock div */}
										<div className={styles.arrows}>
											{numberOfChildren > 0 ? children : (<div className={styles.emptyLoop} />)}
										</div>
										{/* add a space between the right border of repeatBlock div and vConnectorRight */}
										{numberOfChildren > 1 && (
											<div
												className={styles.hConnector}
											></div>
										)}
									</div>
								)}
							>
								<div className={styles.cols}>
									{/* Render child nodes (except if it is a endrepeat) */}
									{children.map((v) =>
										dfs(v, numberOfChildren > 1)
									)}
								</div>
							</ConditionalWrap>

							{/* if there is more than 1 child, draw a horizontal connector */}
							{numberOfChildren > 1 && (
								<div
									className={classname(
										styles.hConnector,
										styles.grow
									)}
								></div>
							)}
							{/* Render child node that corresponds to endrepeat */}
							{exitNode && (
								<div className={styles.cols}>
									{dfs(exitNode)}
								</div>
							)}
						</>
					)}
					{hasSiblings && (
						<div
							className={classname(styles.vConnectorRight, {
								[styles.dashed]:
									isLoopBreakNode(node) ||
									(numberOfChildren === 1 &&
										isLoopBreakNode(children[0])),
							})}
						></div>
					)}
				</div>
			</React.Fragment>
		);
	};

	return <div>{dfs(data)}</div>;
};

export { LayoutAlgorithm as default };
