import React from "react";
import { findNodeSchema } from "modules/Automation/selector/automationSchema";
import { findNode } from "modules/Automation/selector/automationTree";
import {
	getNodeChildren,
	getLoopExitNode,
	isLoopBreakNode,
} from "modules/Automation/helpers/layoutHelpers";
import AutomationNode from "../AutomationNode";
import AutomationLayout from "../AutomationLayout";

const Editor = ({ automationTree, automationNodes, automationSchema }) => {
	const renderNode = (n) => {
		const getNode = (n) => findNode(automationNodes, n);
		const getNodeSchma = (n) => {
			const { config } = getNode(n);
			return findNodeSchema(automationSchema, config.schemaName);
		};

		return (
			<AutomationNode node={getNode(n)} nodeSchema={getNodeSchma(n)} />
		);
	};

	return (
		<AutomationLayout
			renderNode={renderNode}
			automationTree={automationTree}
			getNodeChildren={getNodeChildren}
			getLoopExitNode={getLoopExitNode}
			isLoopBreakNode={isLoopBreakNode}
		/>
	);
};

export { Editor as default };
