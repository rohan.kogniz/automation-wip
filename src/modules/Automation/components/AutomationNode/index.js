import React from "react";
import classname from "classnames";
import styles from "./style.module.scss";
import nodeClassType from "../../helpers/nodeClassType";

const NodeView = ({ node, nodeSchema }) => {
	const data = nodeSchema.name;
	const type = node.nodeType;
	return (
		<div className={classname(styles.node, nodeClassType(type, styles))}>
			<p>{data}</p>
		</div>
	);
};

export { NodeView as default };
