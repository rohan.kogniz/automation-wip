const Data = {
	activities: {
		incident_management_get_one_instances: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Get one incident instance",
			description: "Retrieve one incident instance by its ID",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "incident_management_get_one_instances",
				},
				schema: {
					const: "incident_management",
				},
				path: {
					const: "/incident/instance/{instance_id}",
				},
				method: {
					const: "get",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								path: {
									type: "object",
									additionalProperties: false,
									required: ["instance_id"],
									properties: {
										instance_id: {
											in: "path",
											name: "instance_id",
											description:
												"The identifier of the incident instance to retrieve",
											type: "string",
											autocomplete: [
												{
													resource:
														"incident_management/template",
													url: "/cust_incidents/templates",
													method: "get",
													display: "name",
													value: "id",
												},
												{
													resource:
														"incident_management/instance",
													url: "/cust_incidents/templates/{#[0].value}/instances",
													method: "get",
													display: [
														"{employee.firstName} {employee.lastName}",
														"{locationId}",
													],
													value: "id",
												},
											],
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							allOf: [
								{
									$ref: "#/components/IncidentManagementInstance",
								},
								{
									type: "object",
									properties: {
										employee: {
											$ref: "#/components/IncidentManagementInstancePerson",
										},
										fields: {
											type: "array",
											items: {
												$ref: "#/components/IncidentManagementInstanceField",
											},
										},
									},
								},
							],
						},
					},
				},
			},
			components: {
				IncidentManagementInstance: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "Identifier of this instance",
							readOnly: true,
						},
						templateId: {
							type: "string",
							description:
								"The template used to create this instance",
						},
						parentInstanceId: {
							type: "string",
							description:
								"The parent instance holding this instance",
						},
						date: {
							type: "string",
							description: "Date of the incident",
						},
						status: {
							type: "string",
							description: "Status of the incident",
							enum: [0, 1],
						},
						locationId: {
							type: "string",
							description: "The location of this incident",
						},
						requiresVerification: {
							type: "boolean",
							description:
								"True if the incident instance requires a verification",
						},
						employeeId: {
							type: "string",
							description:
								"The person identifier in this instance",
						},
						userId: {
							type: "string",
							description:
								"The operator identifier in this instance",
						},
					},
				},
				IncidentManagementInstancePerson: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "The person identifier",
							readOnly: true,
						},
						firstName: {
							type: "string",
							description: "First name of the person",
							readOnly: true,
						},
						lastName: {
							type: "string",
							description: "Last name of the person",
							readOnly: true,
						},
						email: {
							type: "string",
							description: "Email of the person",
							readOnly: true,
						},
					},
				},
				IncidentManagementInstanceField: {
					type: "object",
					properties: {
						fieldId: {
							type: "number",
							description: "The field identifier",
							readOnly: true,
						},
						fieldName: {
							type: "string",
							description: "Unique name of the field",
						},
						fieldValue: {
							type: ["string", "object"],
							description: "Value of the field",
						},
						instanceId: {
							type: "number",
							description:
								"Identifier of the instance this field belongs to",
							readOnly: true,
						},
					},
				},
			},
		},
		incident_management_get_many_instances: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Get many instances",
			description: "Get available instances",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "incident_management_get_many_instances",
				},
				schema: {
					const: "incident_management",
				},
				path: {
					const: "/incident/instances",
				},
				method: {
					const: "get",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								query: {
									type: "object",
									additionalProperties: false,
									required: ["parentIds"],
									properties: {
										parentIds: {
											in: "query",
											name: "parentIds",
											description:
												"Parent identifiers of the instances to retrieve",
											type: "string",
											autocomplete: [
												{
													resource:
														"incident_management/template",
													url: "/cust_incidents/templates",
													method: "get",
													display: "name",
													value: "id",
												},
												{
													resource:
														"incident_management/instance",
													url: "/cust_incidents/templates/{#[0].value}/instances",
													method: "get",
													display: [
														"{employee.firstName} {employee.lastName}",
														"{locationId}",
													],
													value: "id",
												},
											],
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							description: "The incident instances",
							type: "array",
							items: {
								allOf: [
									{
										$ref: "#/components/IncidentManagementInstance",
									},
									{
										description:
											"The person and fields components",
										type: "object",
										properties: {
											employee: {
												$ref: "#/components/IncidentManagementInstancePerson",
											},
											fields: {
												type: "array",
												items: {
													$ref: "#/components/IncidentManagementInstanceField",
												},
											},
										},
									},
								],
							},
						},
					},
				},
			},
			components: {
				IncidentManagementInstance: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "Identifier of this instance",
							readOnly: true,
						},
						templateId: {
							type: "string",
							description:
								"The template used to create this instance",
						},
						parentInstanceId: {
							type: "string",
							description:
								"The parent instance holding this instance",
						},
						date: {
							type: "string",
							description: "Date of the incident",
						},
						status: {
							type: "string",
							description: "Status of the incident",
							enum: [0, 1],
						},
						locationId: {
							type: "string",
							description: "The location of this incident",
						},
						requiresVerification: {
							type: "boolean",
							description:
								"True if the incident instance requires a verification",
						},
						employeeId: {
							type: "string",
							description:
								"The person identifier in this instance",
						},
						userId: {
							type: "string",
							description:
								"The operator identifier in this instance",
						},
					},
				},
				IncidentManagementInstancePerson: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "The person identifier",
							readOnly: true,
						},
						firstName: {
							type: "string",
							description: "First name of the person",
							readOnly: true,
						},
						lastName: {
							type: "string",
							description: "Last name of the person",
							readOnly: true,
						},
						email: {
							type: "string",
							description: "Email of the person",
							readOnly: true,
						},
					},
				},
				IncidentManagementInstanceField: {
					type: "object",
					properties: {
						fieldId: {
							type: "number",
							description: "The field identifier",
							readOnly: true,
						},
						fieldName: {
							type: "string",
							description: "Unique name of the field",
						},
						fieldValue: {
							type: ["string", "object"],
							description: "Value of the field",
						},
						instanceId: {
							type: "number",
							description:
								"Identifier of the instance this field belongs to",
							readOnly: true,
						},
					},
				},
			},
		},
		incident_management_create_many_instances: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Create one or many instances",
			description: "Create incident instances",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "incident_management_create_many_instances",
				},
				schema: {
					const: "incident_management",
				},
				path: {
					const: "/incident/instance",
				},
				method: {
					const: "post",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								body: {
									type: "object",
									additionalProperties: false,
									required: ["templateId", "employeeId"],
									properties: {
										templateId: {
											type: "string",
											description:
												"The template used to create this instance",
											autocomplete: [
												{
													resource:
														"incident_management/template",
													url: "/cust_incidents/templates",
													method: "get",
													display: "name",
													value: "id",
												},
											],
										},
										parentInstanceId: {
											type: "string",
											description:
												"The parent instance holding this instance",
											autocomplete: [
												{
													resource:
														"incident_management/template",
													url: "/cust_incidents/templates",
													method: "get",
													display: "name",
													value: "id",
												},
												{
													resource:
														"incident_management/instance",
													url: "/cust_incidents/templates/{#[0].value}/instances",
													method: "get",
													display: [
														"{employee.firstName} {employee.lastName}",
														"{locationId}",
													],
													value: "id",
												},
											],
										},
										date: {
											type: "string",
											description: "Date of the incident",
										},
										status: {
											type: "string",
											description:
												"Status of the incident",
											enum: [0, 1],
										},
										locationId: {
											type: "string",
											description:
												"The location of this incident",
											autocomplete: [
												{
													resource: "camgrpid",
													url: "/cameragrp_list",
													method: "post",
													display: "camgrps.name",
													value: "camgrps.camgrpid",
												},
											],
										},
										requiresVerification: {
											type: "boolean",
											description:
												"True if the incident instance requires a verification",
										},
										employeeId: {
											type: "string",
											description:
												"The person identifier in this instance",
										},
										userId: {
											type: "string",
											description:
												"The operator identifier in this instance",
											autocomplete: [
												{
													resource: "operator",
													url: "/operators_list",
													method: "post",
													display:
														"{operators.op_name_first} {operators.op_name_last}",
													value: "operators.operatorid",
												},
											],
										},
										fields: {
											description:
												"The list of fields to create",
											type: "array",
											items: {
												type: "object",
												additionalProperties: false,
												required: [
													"fieldName",
													"fieldValue",
												],
												properties: {
													fieldName: {
														type: "string",
														description:
															"Unique name of the field",
													},
													fieldValue: {
														type: [
															"string",
															"object",
														],
														description:
															"Value of the field",
													},
												},
											},
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							description:
								"The list of identifiers of the new incident instances",
							type: "array",
							items: {
								description:
									"The results containing new instances identifiers",
								type: "object",
								properties: {
									incidentInstanceId: {
										description:
											"The identifier of the new incident instance",
										type: "number",
									},
								},
							},
						},
					},
				},
			},
			components: {
				IncidentManagementInstance: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "Identifier of this instance",
							readOnly: true,
						},
						templateId: {
							type: "string",
							description:
								"The template used to create this instance",
						},
						parentInstanceId: {
							type: "string",
							description:
								"The parent instance holding this instance",
						},
						date: {
							type: "string",
							description: "Date of the incident",
						},
						status: {
							type: "string",
							description: "Status of the incident",
							enum: [0, 1],
						},
						locationId: {
							type: "string",
							description: "The location of this incident",
						},
						requiresVerification: {
							type: "boolean",
							description:
								"True if the incident instance requires a verification",
						},
						employeeId: {
							type: "string",
							description:
								"The person identifier in this instance",
						},
						userId: {
							type: "string",
							description:
								"The operator identifier in this instance",
						},
					},
				},
				IncidentManagementInstancePerson: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "The person identifier",
							readOnly: true,
						},
						firstName: {
							type: "string",
							description: "First name of the person",
							readOnly: true,
						},
						lastName: {
							type: "string",
							description: "Last name of the person",
							readOnly: true,
						},
						email: {
							type: "string",
							description: "Email of the person",
							readOnly: true,
						},
					},
				},
				IncidentManagementInstanceField: {
					type: "object",
					properties: {
						fieldId: {
							type: "number",
							description: "The field identifier",
							readOnly: true,
						},
						fieldName: {
							type: "string",
							description: "Unique name of the field",
						},
						fieldValue: {
							type: ["string", "object"],
							description: "Value of the field",
						},
						instanceId: {
							type: "number",
							description:
								"Identifier of the instance this field belongs to",
							readOnly: true,
						},
					},
				},
			},
		},
		incident_management_edit_one_instance: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Edit one incident instance",
			description:
				"Edit the different fields of an existing incident instance",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "incident_management_edit_one_instance",
				},
				schema: {
					const: "incident_management",
				},
				path: {
					const: "/incident/instance/{instance_id}",
				},
				method: {
					const: "patch",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								path: {
									type: "object",
									additionalProperties: false,
									required: ["instance_id"],
									properties: {
										instance_id: {
											in: "path",
											name: "instance_id",
											description:
												"The identifier of the incident instance to edit",
											type: "string",
										},
									},
								},
								body: {
									type: "object",
									additionalProperties: false,
									required: [],
									properties: {
										parentInstanceId: {
											type: "string",
											description:
												"The parent instance holding this instance",
											autocomplete: [
												{
													resource:
														"incident_management/template",
													url: "/cust_incidents/templates",
													method: "get",
													display: "name",
													value: "id",
												},
												{
													resource:
														"incident_management/instance",
													url: "/cust_incidents/templates/{#[0].value}/instances",
													method: "get",
													display: [
														"{employee.firstName} {employee.lastName}",
														"{locationId}",
													],
													value: "id",
												},
											],
										},
										date: {
											type: "string",
											description: "Date of the incident",
										},
										status: {
											type: "string",
											description:
												"Status of the incident",
											enum: [0, 1],
										},
										locationId: {
											type: "string",
											description:
												"The location of this incident",
											autocomplete: [
												{
													resource: "camgrpid",
													url: "/cameragrp_list",
													method: "post",
													display: "camgrps.name",
													value: "camgrps.camgrpid",
												},
											],
										},
										requiresVerification: {
											type: "boolean",
											description:
												"True if the incident instance requires a verification",
										},
										fields: {
											type: "array",
											description:
												"The list of fields to update",
											items: {
												type: "object",
												additionalProperties: false,
												required: [
													"fieldName",
													"fieldValue",
												],
												properties: {
													fieldName: {
														type: "string",
														description:
															"Unique name of the field",
													},
													fieldValue: {
														type: [
															"string",
															"object",
														],
														description:
															"Value of the field",
													},
												},
											},
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {},
			components: {
				IncidentManagementInstance: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "Identifier of this instance",
							readOnly: true,
						},
						templateId: {
							type: "string",
							description:
								"The template used to create this instance",
						},
						parentInstanceId: {
							type: "string",
							description:
								"The parent instance holding this instance",
						},
						date: {
							type: "string",
							description: "Date of the incident",
						},
						status: {
							type: "string",
							description: "Status of the incident",
							enum: [0, 1],
						},
						locationId: {
							type: "string",
							description: "The location of this incident",
						},
						requiresVerification: {
							type: "boolean",
							description:
								"True if the incident instance requires a verification",
						},
						employeeId: {
							type: "string",
							description:
								"The person identifier in this instance",
						},
						userId: {
							type: "string",
							description:
								"The operator identifier in this instance",
						},
					},
				},
				IncidentManagementInstancePerson: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "The person identifier",
							readOnly: true,
						},
						firstName: {
							type: "string",
							description: "First name of the person",
							readOnly: true,
						},
						lastName: {
							type: "string",
							description: "Last name of the person",
							readOnly: true,
						},
						email: {
							type: "string",
							description: "Email of the person",
							readOnly: true,
						},
					},
				},
				IncidentManagementInstanceField: {
					type: "object",
					properties: {
						fieldId: {
							type: "number",
							description: "The field identifier",
							readOnly: true,
						},
						fieldName: {
							type: "string",
							description: "Unique name of the field",
						},
						fieldValue: {
							type: ["string", "object"],
							description: "Value of the field",
						},
						instanceId: {
							type: "number",
							description:
								"Identifier of the instance this field belongs to",
							readOnly: true,
						},
					},
				},
			},
		},
		survey_get_many_links: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Get many survey links",
			description: "Get the survey links for many users",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "survey_get_many_links",
				},
				schema: {
					const: "survey",
				},
				path: {
					const: "/survey/{surveyId}/links",
				},
				method: {
					const: "get",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								path: {
									type: "object",
									additionalProperties: false,
									required: ["surveyId"],
									properties: {
										surveyId: {
											in: "path",
											name: "surveyId",
											description:
												"Existing survey to get the link",
											type: "string",
											autocomplete: [
												{
													resource: "survey",
													url: "/survey_list",
													method: "get",
													display: "rows.name",
													value: "rows.id",
												},
											],
										},
									},
								},
								query: {
									type: "object",
									additionalProperties: false,
									required: ["personIds"],
									properties: {
										personIds: {
											in: "query",
											name: "personIds",
											description:
												"Person ids to get the survey links for",
											type: "string",
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							type: "array",
							items: {
								$ref: "#/components/SurveyLink",
							},
						},
					},
				},
			},
			components: {
				SurveyLink: {
					type: "object",
					properties: {
						personid: {
							type: "number",
							description: "Identifier of an enrolled person",
							readOnly: true,
						},
						surveyid: {
							type: "number",
							description: "Identifier of a survey",
							readOnly: true,
						},
						link: {
							type: "string",
							description: "URL to the survey for that person",
							readOnly: true,
						},
					},
				},
				SurveyAnswer: {
					type: "object",
					properties: {
						personid: {
							type: "number",
							description: "Identifier of an enrolled person",
							readOnly: true,
						},
						surveyid: {
							type: "number",
							description: "Identifier of a survey",
							readOnly: true,
						},
						successful: {
							type: "number",
							description:
								"Indicate if the survey was successful",
							readOnly: true,
						},
					},
				},
			},
		},
		survey_get_one_answer: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Get one answer to a survey",
			description: "Get the latest answer to a survey from a person",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "survey_get_one_answer",
				},
				schema: {
					const: "survey",
				},
				path: {
					const: "/survey/{surveyId}/answer",
				},
				method: {
					const: "get",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								path: {
									type: "object",
									additionalProperties: false,
									required: ["surveyId"],
									properties: {
										surveyId: {
											in: "path",
											name: "surveyId",
											description:
												"Existing survey to get the link",
											type: "string",
											autocomplete: [
												{
													resource: "survey",
													url: "/survey_list",
													method: "get",
													display: "rows.name",
													value: "rows.id",
												},
											],
										},
									},
								},
								query: {
									type: "object",
									additionalProperties: false,
									required: ["personId"],
									properties: {
										personId: {
											in: "query",
											name: "personId",
											description:
												"The person id to get the answer for",
											type: "string",
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							type: "array",
							items: {
								$ref: "#/components/SurveyAnswer",
							},
						},
					},
				},
			},
			components: {
				SurveyLink: {
					type: "object",
					properties: {
						personid: {
							type: "number",
							description: "Identifier of an enrolled person",
							readOnly: true,
						},
						surveyid: {
							type: "number",
							description: "Identifier of a survey",
							readOnly: true,
						},
						link: {
							type: "string",
							description: "URL to the survey for that person",
							readOnly: true,
						},
					},
				},
				SurveyAnswer: {
					type: "object",
					properties: {
						personid: {
							type: "number",
							description: "Identifier of an enrolled person",
							readOnly: true,
						},
						surveyid: {
							type: "number",
							description: "Identifier of a survey",
							readOnly: true,
						},
						successful: {
							type: "number",
							description:
								"Indicate if the survey was successful",
							readOnly: true,
						},
					},
				},
			},
		},
		people_get_many: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Get many people",
			description: "Get many enrolled person",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "people_get_many",
				},
				schema: {
					const: "people",
				},
				path: {
					const: "/people",
				},
				method: {
					const: "post",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								query: {
									type: "object",
									additionalProperties: false,
									required: [],
									properties: {
										camgrpid: {
											in: "query",
											name: "camgrpid",
											description:
												"Location id of the people to retrieve",
											type: "string",
											autocomplete: [
												{
													resource: "camgrpid",
													url: "/cameragrp_list",
													method: "post",
													display: "camgrps.name",
													value: "camgrps.camgrpid",
												},
											],
										},
										enrolledId: {
											in: "query",
											name: "enrolledId",
											description:
												"Identifier of the enrolled person.",
											type: "string",
										},
										employeeId: {
											in: "query",
											name: "employeeId",
											description:
												"Company identifier of the enrolled person.",
											type: "string",
										},
									},
								},
								body: {
									type: "object",
									additionalProperties: false,
									required: [],
									properties: {
										attributes: {
											type: "array",
											items: {
												type: "object",
												additionalProperties: false,
												required: ["n", "v"],
												properties: {
													n: {
														type: "string",
														description:
															"Name of the property.",
													},
													v: {
														type: "string",
														description:
															"Value of the property.",
													},
												},
											},
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {
				200: {
					"application/json": {
						schema: {
							description: "A list of enrolled people.",
							type: "array",
							items: {
								allOf: [
									{
										$ref: "#/components/Person",
									},
									{
										description: "The attributes component",
										type: "object",
										properties: {
											attributes: {
												$ref: "#/components/PersonAttribute",
											},
										},
									},
								],
							},
						},
					},
				},
			},
			components: {
				Person: {
					type: "object",
					properties: {
						id: {
							type: "number",
							description: "Unique identifier of this person",
							readOnly: true,
						},
						firstName: {
							type: "string",
							description: "First name of the user",
						},
						lastName: {
							type: "string",
							description: "Last name of the user",
						},
						defaultLocationId: {
							type: "string",
							description: "Location ID of the user",
						},
					},
				},
				PersonAttribute: {
					type: "object",
					additionalProperties: false,
					properties: {
						n: {
							type: "string",
							description: "Name of the property.",
						},
						v: {
							type: "string",
							description: "Value of the property.",
						},
					},
				},
			},
		},
		message_send_sms: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Send SMS",
			description: "Send SMS to many enrolled person",
			nodeType: "activity",
			nodeSubtype: "InternalAPIActivity",
			required: ["path", "method", "schema", "schemaName", "parameters"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "message_send_sms",
				},
				schema: {
					const: "message",
				},
				path: {
					const: "/message/sms",
				},
				method: {
					const: "post",
				},
				parameters: {
					allOf: [
						{
							additionalProperties: false,
							properties: {
								body: {
									type: "object",
									additionalProperties: false,
									required: ["personId", "message"],
									properties: {
										personId: {
											type: "string",
											description:
												"Unique identifier of an enrolled person (message target)",
										},
										message: {
											type: "string",
											description:
												"The text message to transmit",
										},
									},
								},
							},
						},
					],
				},
			},
			responses: {},
			components: {
				Message: {
					type: "object",
					properties: {
						personId: {
							type: "string",
							description:
								"Unique identifier of an enrolled person (message target)",
						},
						message: {
							type: "string",
							description: "The text message to transmit",
						},
					},
				},
			},
		},
		count_function_def_activity: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Count Activity",
			description: "Count the number of items in an array.",
			nodeType: "activity",
			nodeSubtype: "FunctionDefActivity",
			required: ["schemaName", "type", "config"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "count_function_def_activity",
				},
				type: {
					const: "CountFunctionDef",
				},
				config: {
					$ref: "#/components/CountFunctionDef",
				},
			},
			components: {
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
			},
		},
		merge_array_activity: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Merge Array",
			description:
				"Merge several arrays together into one array of objects",
			nodeType: "activity",
			nodeSubtype: "MergeArrayActivity",
			required: ["mergeInstruction", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "merge_array_activity",
				},
				mergeInstruction: {
					type: "array",
					description: "Instructions for the merge.",
					minItems: 1,
					items: {
						type: "object",
						additionalProperties: false,
						required: ["key", "value"],
						properties: {
							key: {
								type: "string",
								description:
									"Name of the key to retrieve the merged value",
							},
							value: {
								type: "string",
								description: "Array of values to merge",
							},
						},
					},
				},
			},
		},
		filter_array_activity: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Filter Array",
			description:
				"Keep all the elements of an input array that satisfies one or many conditions.",
			nodeType: "activity",
			nodeSubtype: "FilterArrayActivity",
			required: ["input", "condition"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "filter_array_activity",
				},
				input: {
					type: "string",
					description: "Key to the output of a node to filter.",
				},
				condition: {
					type: "object",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "FilterCondition",
								},
								config: {
									$ref: "#/components/FilterCondition",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "GroupCondition",
								},
								config: {
									$ref: "#/components/GroupCondition",
								},
							},
						},
					],
				},
			},
			components: {
				FilterCondition: {
					name: "Filter condition",
					description:
						"Assert the output of an activity using equality and comparison symbols",
					required: ["left", "condition", "right"],
					additionalProperties: false,
					properties: {
						left: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Left-hand side of the operator, can be a path to an output of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
						condition: {
							type: "string",
							description: "The equality or comparison symbols",
							enum: [
								"=",
								"!=",
								">",
								"<",
								">=",
								"<=",
								"IN",
								"NOT IN",
							],
						},
						right: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Right-hand side of the operator, can be a path to an ouput of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
					},
				},
				GroupCondition: {
					name: "Group condition",
					description:
						"Use a logical operator (AND, OR) to compare multiple conditions with each other",
					additionalProperties: false,
					required: ["operator", "conditions"],
					properties: {
						operator: {
							type: "string",
							description:
								"The operator used to compare conditions",
							enum: ["AND", "OR"],
						},
						conditions: {
							type: "array",
							description:
								"The conditions that needs to be compared",
							minItems: 1,
							items: {
								anyOf: [
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "FilterCondition",
											},
											config: {
												$ref: "#/components/FilterCondition",
											},
										},
									},
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "GroupCondition",
											},
											config: {
												$ref: "#/components/GroupCondition",
											},
										},
									},
								],
							},
						},
					},
				},
				FunctionDef: {
					$schema: "http://json-schema.org/draft-07/schema#",
					name: "Functions",
					description:
						"Manipulate inputs to produce different kind of outputs",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "CountFunctionDef",
									description: "Count Function",
								},
								config: {
									$ref: "#/components/CountFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "EmptyFunctionDef",
									description: "Is Empty Function",
								},
								config: {
									$ref: "#/components/EmptyFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "TimestampFunctionDef",
									description: "Timestamp Function",
								},
								config: {
									$ref: "#/components/TimestampFunctionDef",
								},
							},
						},
					],
					components: {
						CountFunctionDef: {
							name: "Count Function",
							description:
								"Count the number of items in an array.",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
											type: "string",
										},
										{
											description:
												"Find the length of the array",
											type: "array",
										},
									],
								},
							},
						},
						EmptyFunctionDef: {
							name: "Is Empty Function",
							description:
								"Return true if the string, array of object is empty",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
											type: "string",
										},
										{
											description:
												"Check if the array is empty",
											type: "array",
										},
										{
											description:
												"Check if the object is empty",
											type: "object",
										},
									],
								},
							},
						},
						TimestampFunctionDef: {
							name: "Timestamp Function",
							description:
								"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
						},
					},
				},
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
	},
	triggers: {
		trigger: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Trigger",
			description:
				"Evaluate different conditions and connect them with logical operators",
			nodeType: "trigger",
			nodeSubtype: "Trigger",
			required: ["condition", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "trigger",
				},
				condition: {
					type: "object",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "FilterCondition",
								},
								config: {
									$ref: "#/components/FilterCondition",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "GroupCondition",
								},
								config: {
									$ref: "#/components/GroupCondition",
								},
							},
						},
					],
				},
			},
			components: {
				FilterCondition: {
					name: "Filter condition",
					description:
						"Assert the output of an activity using equality and comparison symbols",
					required: ["left", "condition", "right"],
					additionalProperties: false,
					properties: {
						left: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Left-hand side of the operator, can be a path to an output of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
						condition: {
							type: "string",
							description: "The equality or comparison symbols",
							enum: [
								"=",
								"!=",
								">",
								"<",
								">=",
								"<=",
								"IN",
								"NOT IN",
							],
						},
						right: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Right-hand side of the operator, can be a path to an ouput of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
					},
				},
				GroupCondition: {
					name: "Group condition",
					description:
						"Use a logical operator (AND, OR) to compare multiple conditions with each other",
					additionalProperties: false,
					required: ["operator", "conditions"],
					properties: {
						operator: {
							type: "string",
							description:
								"The operator used to compare conditions",
							enum: ["AND", "OR"],
						},
						conditions: {
							type: "array",
							description:
								"The conditions that needs to be compared",
							minItems: 1,
							items: {
								anyOf: [
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "FilterCondition",
											},
											config: {
												$ref: "#/components/FilterCondition",
											},
										},
									},
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "GroupCondition",
											},
											config: {
												$ref: "#/components/GroupCondition",
											},
										},
									},
								],
							},
						},
					},
				},
				FunctionDef: {
					$schema: "http://json-schema.org/draft-07/schema#",
					name: "Functions",
					description:
						"Manipulate inputs to produce different kind of outputs",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "CountFunctionDef",
									description: "Count Function",
								},
								config: {
									$ref: "#/components/CountFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "EmptyFunctionDef",
									description: "Is Empty Function",
								},
								config: {
									$ref: "#/components/EmptyFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "TimestampFunctionDef",
									description: "Timestamp Function",
								},
								config: {
									$ref: "#/components/TimestampFunctionDef",
								},
							},
						},
					],
					components: {
						CountFunctionDef: {
							name: "Count Function",
							description:
								"Count the number of items in an array.",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
											type: "string",
										},
										{
											description:
												"Find the length of the array",
											type: "array",
										},
									],
								},
							},
						},
						EmptyFunctionDef: {
							name: "Is Empty Function",
							description:
								"Return true if the string, array of object is empty",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
											type: "string",
										},
										{
											description:
												"Check if the array is empty",
											type: "array",
										},
										{
											description:
												"Check if the object is empty",
											type: "object",
										},
									],
								},
							},
						},
						TimestampFunctionDef: {
							name: "Timestamp Function",
							description:
								"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
						},
					},
				},
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
	},
	flows: {
		repeat_flow: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Repeat",
			description:
				"Repeat the contained nodes, running them the specified number of times",
			nodeType: "flow",
			nodeSubtype: "RepeatSyncControlFlow",
			required: ["schemaName", "nIteration"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "repeat_flow",
				},
				nIteration: {
					type: "number",
					description:
						"The number of time to repeat the contained nodes",
				},
			},
		},
		repeat_each_flow: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Repeat Each",
			description:
				"Take a list of items and repeat the contained node for every items in the list",
			nodeType: "flow",
			nodeSubtype: "RepeatEachAsyncControlFlow",
			required: ["schemaName", "arrayToLoop"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "repeat_each_flow",
				},
				arrayToLoop: {
					type: "string",
					description:
						"Key to the list to repeat the contained nodes",
				},
			},
		},
		repeat_end_flow: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "End Repeat",
			description: "Nodes to execute when the repeat loop is completed",
			nodeType: "flow",
			nodeSubtype: "EndRepeatControlFlow",
			required: ["schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "repeat_end_flow",
				},
			},
		},
		repeat_exit_flow: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Exit from loop",
			description: "Stop the current repeat loop iteration",
			nodeType: "flow",
			nodeSubtype: "ExitRepeatControlFlow",
			required: ["repeatControlFlowKey", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "repeat_exit_flow",
				},
				repeatControlFlowKey: {
					type: "string",
					description: "Key of the loop to exit",
				},
			},
		},
	},
	signals: {
		wait_second_signal: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Wait",
			description: "Wait for a specified number of seconds",
			nodeType: "signal",
			nodeSubtype: "TemporalSignal",
			required: ["relativeTime", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "wait_second_signal",
				},
				relativeTime: {
					type: "number",
					description: "Number of seconds to wait",
					minimum: 60,
				},
			},
		},
		wait_for_time_signal: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Wait for time",
			description: "Wait for a specified time",
			nodeType: "signal",
			nodeSubtype: "TemporalSignal",
			required: ["absoluteTime", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "wait_for_time_signal",
				},
				absoluteTime: {
					type: "string",
					description: "Datetime to wait for in an ISO format",
					example: "2021-06-08T20:47:38Z",
				},
			},
		},
		match_signal: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Wait for signal",
			description: "Wait for an external signal",
			nodeType: "signal",
			nodeSubtype: "MatchSignal",
			required: ["match", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "match_signal",
				},
				match: {
					type: "object",
					description: "The signal structure to wait for.",
				},
				condition: {
					type: "object",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "FilterCondition",
								},
								config: {
									$ref: "#/components/FilterCondition",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "GroupCondition",
								},
								config: {
									$ref: "#/components/GroupCondition",
								},
							},
						},
					],
				},
			},
			components: {
				FilterCondition: {
					name: "Filter condition",
					description:
						"Assert the output of an activity using equality and comparison symbols",
					required: ["left", "condition", "right"],
					additionalProperties: false,
					properties: {
						left: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Left-hand side of the operator, can be a path to an output of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
						condition: {
							type: "string",
							description: "The equality or comparison symbols",
							enum: [
								"=",
								"!=",
								">",
								"<",
								">=",
								"<=",
								"IN",
								"NOT IN",
							],
						},
						right: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Right-hand side of the operator, can be a path to an ouput of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
					},
				},
				GroupCondition: {
					name: "Group condition",
					description:
						"Use a logical operator (AND, OR) to compare multiple conditions with each other",
					additionalProperties: false,
					required: ["operator", "conditions"],
					properties: {
						operator: {
							type: "string",
							description:
								"The operator used to compare conditions",
							enum: ["AND", "OR"],
						},
						conditions: {
							type: "array",
							description:
								"The conditions that needs to be compared",
							minItems: 1,
							items: {
								anyOf: [
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "FilterCondition",
											},
											config: {
												$ref: "#/components/FilterCondition",
											},
										},
									},
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "GroupCondition",
											},
											config: {
												$ref: "#/components/GroupCondition",
											},
										},
									},
								],
							},
						},
					},
				},
				FunctionDef: {
					$schema: "http://json-schema.org/draft-07/schema#",
					name: "Functions",
					description:
						"Manipulate inputs to produce different kind of outputs",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "CountFunctionDef",
									description: "Count Function",
								},
								config: {
									$ref: "#/components/CountFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "EmptyFunctionDef",
									description: "Is Empty Function",
								},
								config: {
									$ref: "#/components/EmptyFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "TimestampFunctionDef",
									description: "Timestamp Function",
								},
								config: {
									$ref: "#/components/TimestampFunctionDef",
								},
							},
						},
					],
					components: {
						CountFunctionDef: {
							name: "Count Function",
							description:
								"Count the number of items in an array.",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
											type: "string",
										},
										{
											description:
												"Find the length of the array",
											type: "array",
										},
									],
								},
							},
						},
						EmptyFunctionDef: {
							name: "Is Empty Function",
							description:
								"Return true if the string, array of object is empty",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
											type: "string",
										},
										{
											description:
												"Check if the array is empty",
											type: "array",
										},
										{
											description:
												"Check if the object is empty",
											type: "object",
										},
									],
								},
							},
						},
						TimestampFunctionDef: {
							name: "Timestamp Function",
							description:
								"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
						},
					},
				},
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
		incident_management_new_instance_signal: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "New incident instance is created",
			description: "A new incident instance was created",
			nodeType: "signal",
			nodeSubtype: "MatchSignal",
			required: ["match", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "incident_management_new_instance_signal",
				},
				match: {
					type: "object",
					required: ["name", "path", "method"],
					properties: {
						name: {
							const: "incident_management",
						},
						path: {
							const: "/cust_incidents/instances",
						},
						method: {
							const: "POST",
						},
					},
				},
				condition: {
					type: "object",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "FilterCondition",
								},
								config: {
									$ref: "#/components/FilterCondition",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "GroupCondition",
								},
								config: {
									$ref: "#/components/GroupCondition",
								},
							},
						},
					],
				},
			},
			components: {
				FilterCondition: {
					name: "Filter condition",
					description:
						"Assert the output of an activity using equality and comparison symbols",
					required: ["left", "condition", "right"],
					additionalProperties: false,
					properties: {
						left: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Left-hand side of the operator, can be a path to an output of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
						condition: {
							type: "string",
							description: "The equality or comparison symbols",
							enum: [
								"=",
								"!=",
								">",
								"<",
								">=",
								"<=",
								"IN",
								"NOT IN",
							],
						},
						right: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Right-hand side of the operator, can be a path to an ouput of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
					},
				},
				GroupCondition: {
					name: "Group condition",
					description:
						"Use a logical operator (AND, OR) to compare multiple conditions with each other",
					additionalProperties: false,
					required: ["operator", "conditions"],
					properties: {
						operator: {
							type: "string",
							description:
								"The operator used to compare conditions",
							enum: ["AND", "OR"],
						},
						conditions: {
							type: "array",
							description:
								"The conditions that needs to be compared",
							minItems: 1,
							items: {
								anyOf: [
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "FilterCondition",
											},
											config: {
												$ref: "#/components/FilterCondition",
											},
										},
									},
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "GroupCondition",
											},
											config: {
												$ref: "#/components/GroupCondition",
											},
										},
									},
								],
							},
						},
					},
				},
				FunctionDef: {
					$schema: "http://json-schema.org/draft-07/schema#",
					name: "Functions",
					description:
						"Manipulate inputs to produce different kind of outputs",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "CountFunctionDef",
									description: "Count Function",
								},
								config: {
									$ref: "#/components/CountFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "EmptyFunctionDef",
									description: "Is Empty Function",
								},
								config: {
									$ref: "#/components/EmptyFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "TimestampFunctionDef",
									description: "Timestamp Function",
								},
								config: {
									$ref: "#/components/TimestampFunctionDef",
								},
							},
						},
					],
					components: {
						CountFunctionDef: {
							name: "Count Function",
							description:
								"Count the number of items in an array.",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
											type: "string",
										},
										{
											description:
												"Find the length of the array",
											type: "array",
										},
									],
								},
							},
						},
						EmptyFunctionDef: {
							name: "Is Empty Function",
							description:
								"Return true if the string, array of object is empty",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
											type: "string",
										},
										{
											description:
												"Check if the array is empty",
											type: "array",
										},
										{
											description:
												"Check if the object is empty",
											type: "object",
										},
									],
								},
							},
						},
						TimestampFunctionDef: {
							name: "Timestamp Function",
							description:
								"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
						},
					},
				},
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
		survey_new_answer_signal: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "New answer to a survey",
			description: "A new answer to a survey was received",
			nodeType: "signal",
			nodeSubtype: "MatchSignal",
			required: ["match", "schemaName"],
			properties: {
				key: {
					type: "string",
					description: "Identifier of this node.",
				},
				schemaName: {
					const: "survey_new_answer_signal",
				},
				match: {
					type: "object",
					required: ["name", "path", "method"],
					properties: {
						name: {
							const: "survey",
						},
						path: {
							const: "/pub_submit_survey",
						},
						method: {
							const: "POST",
						},
					},
				},
				condition: {
					type: "object",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "FilterCondition",
								},
								config: {
									$ref: "#/components/FilterCondition",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							properties: {
								type: {
									const: "GroupCondition",
								},
								config: {
									$ref: "#/components/GroupCondition",
								},
							},
						},
					],
				},
			},
			components: {
				FilterCondition: {
					name: "Filter condition",
					description:
						"Assert the output of an activity using equality and comparison symbols",
					required: ["left", "condition", "right"],
					additionalProperties: false,
					properties: {
						left: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Left-hand side of the operator, can be a path to an output of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
						condition: {
							type: "string",
							description: "The equality or comparison symbols",
							enum: [
								"=",
								"!=",
								">",
								"<",
								">=",
								"<=",
								"IN",
								"NOT IN",
							],
						},
						right: {
							oneOf: [
								{
									type: ["string", "number"],
									description:
										"Right-hand side of the operator, can be a path to an ouput of a previous activity",
								},
								{
									$ref: "#/components/FunctionDef",
								},
							],
						},
					},
				},
				GroupCondition: {
					name: "Group condition",
					description:
						"Use a logical operator (AND, OR) to compare multiple conditions with each other",
					additionalProperties: false,
					required: ["operator", "conditions"],
					properties: {
						operator: {
							type: "string",
							description:
								"The operator used to compare conditions",
							enum: ["AND", "OR"],
						},
						conditions: {
							type: "array",
							description:
								"The conditions that needs to be compared",
							minItems: 1,
							items: {
								anyOf: [
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "FilterCondition",
											},
											config: {
												$ref: "#/components/FilterCondition",
											},
										},
									},
									{
										type: "object",
										required: ["type", "config"],
										properties: {
											type: {
												const: "GroupCondition",
											},
											config: {
												$ref: "#/components/GroupCondition",
											},
										},
									},
								],
							},
						},
					},
				},
				FunctionDef: {
					$schema: "http://json-schema.org/draft-07/schema#",
					name: "Functions",
					description:
						"Manipulate inputs to produce different kind of outputs",
					oneOf: [
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "CountFunctionDef",
									description: "Count Function",
								},
								config: {
									$ref: "#/components/CountFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "EmptyFunctionDef",
									description: "Is Empty Function",
								},
								config: {
									$ref: "#/components/EmptyFunctionDef",
								},
							},
						},
						{
							type: "object",
							required: ["type", "config"],
							additionalProperties: false,
							properties: {
								type: {
									const: "TimestampFunctionDef",
									description: "Timestamp Function",
								},
								config: {
									$ref: "#/components/TimestampFunctionDef",
								},
							},
						},
					],
					components: {
						CountFunctionDef: {
							name: "Count Function",
							description:
								"Count the number of items in an array.",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
											type: "string",
										},
										{
											description:
												"Find the length of the array",
											type: "array",
										},
									],
								},
							},
						},
						EmptyFunctionDef: {
							name: "Is Empty Function",
							description:
								"Return true if the string, array of object is empty",
							required: ["input"],
							properties: {
								input: {
									oneOf: [
										{
											description:
												"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
											type: "string",
										},
										{
											description:
												"Check if the array is empty",
											type: "array",
										},
										{
											description:
												"Check if the object is empty",
											type: "object",
										},
									],
								},
							},
						},
						TimestampFunctionDef: {
							name: "Timestamp Function",
							description:
								"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
						},
					},
				},
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
	},
};

export { Data as default };
