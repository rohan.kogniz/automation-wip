const input = [{
    custid: 53,
    nodeId: 1475,
    workflowId: 235,
    nodeGuid: 0.1201,
    parentGuid: null,
    nodeType: "signal",
    nodeSubtype: "MatchSignal",
    config: {
        index: -1,
        key: "input",
        schemaName: "incident_management_new_instance_signal",
        match: {
            name: "incident_management",
            path: "/cust_incidents/instances",
            method: "POST",
        },
        condition: {
            type: "FilterCondition",
            config: {
                left: "{{ match.data.template.type }}",
                condition: "=",
                right: "0",
            },
        },
    },
    active: true,
}]

const output = {
	id: "0.1201",
	children: [],
};

const io = [input, output]
export { io as default }