const input = [{
    custid: 53,
    nodeId: 1475,
    workflowId: 235,
    nodeGuid: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
    parentGuid: null,
    nodeType: "signal",
    nodeSubtype: "MatchSignal",
    config: {
        index: -1,
        key: "input",
        schemaName: "incident_management_new_instance_signal",
        match: {
            name: "incident_management",
            path: "/cust_incidents/instances",
            method: "POST",
        },
        condition: {
            type: "FilterCondition",
            config: {
                left: "{{ match.data.template.type }}",
                condition: "=",
                right: "0",
            },
        },
    },
    active: true,
}]

const output = {
	id: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
	children: [],
};

const io = [input, output]
export { io as default }