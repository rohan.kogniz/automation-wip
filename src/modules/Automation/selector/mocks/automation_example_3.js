const input = [{
    custid: 53,
    nodeId: 1475,
    workflowId: 235,
    nodeGuid: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
    parentGuid: null,
    nodeType: "signal",
    nodeSubtype: "MatchSignal",
    config: {
        index: -1,
        key: "input",
        schemaName: "incident_management_new_instance_signal",
        match: {
            name: "incident_management",
            path: "/cust_incidents/instances",
            method: "POST",
        },
        condition: {
            type: "FilterCondition",
            config: {
                left: "{{ match.data.template.type }}",
                condition: "=",
                right: "0",
            },
        },
    },
    active: true,
},
{
    custid: 53,
    nodeId: 1476,
    workflowId: 235,
    nodeGuid: "b0dba1c0-6ecb-4e66-a75c-cc43129cc166",
    parentGuid: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
    nodeType: "activity",
    nodeSubtype: "InternalAPIActivity",
    config: {
        index: -1,
        key: "employees",
        schemaName: "people_get_many",
        schema: "people",
        path: "/people",
        method: "post",
        requestSizeLimit: 100,
        requestSizeOffset: 0,
        requestLimitPath: "query.limit",
        requestOffsetPath: "query.offset",
        parameters: {
            query: {
                camgrpid: "{{ input.locationId }}",
                includeAttribs: "0",
            },
        },
    },
    active: true,
},
{
    custid: 53,
    nodeId: 1477,
    workflowId: 235,
    nodeGuid: "3217c6b3-cd4d-48b9-82ae-c5d26d0f2506",
    parentGuid: "b0dba1c0-6ecb-4e66-a75c-cc43129cc166",
    nodeType: "trigger",
    nodeSubtype: "Trigger",
    config: {
        index: 0,
        schemaName: "trigger",
        condition: {
            type: "FilterCondition",
            config: {
                left: {
                    type: "CountFunctionDef",
                    config: { input: "{{ employees }}" },
                },
                condition: "=",
                right: "0",
            },
        },
    },
    active: true,
}]

const output = {
	id: "037270db-7a05-4bf2-a6ed-469f8fb873c2",
	children: [{
        id: "b0dba1c0-6ecb-4e66-a75c-cc43129cc166",
        children: [{
            id: "3217c6b3-cd4d-48b9-82ae-c5d26d0f2506"
        }]
    }],
};

const io = [input, output]
export { io as default }