const makeNode = (node) => {
	let { nodeGuid } = node;
	let x = document.createElement("x");
	x.setAttribute("data-node-id", nodeGuid);
	if (node.nodeSubtype === "EndRepeatControlFlow") {
		x.setAttribute("data-exit", "true");
	}
	if (node.nodeSubtype === "ExitRepeatControlFlow") {
		x.setAttribute("data-break", "true");
	}
	return x;
};

const build = (nodes) => {
	const head = nodes.filter((n) => n.parentGuid === null);
	if (head.length === 0) {
		return;
	}

	const toTree = (node) => {
		let n = makeNode(node);
		const nodeGuid = node.nodeGuid;
		const children = nodes.filter((n) => n.parentGuid === nodeGuid);
		for (const item of children) {
			n.appendChild(toTree(item));
		}
		return n;
	};

	return toTree(head[0]);
};

export const findNode = (nodes, treeNode) => {
	const nodeGuid = treeNode.getAttribute("data-node-id");
	const id = nodes.findIndex((n) => String(n.nodeGuid) === nodeGuid);
	return nodes[id];
};

export const findPathToNode = (treeNode) => {
	let array = []
	let node = treeNode.parentNode
	while(node && node.hasAttribute('data-node-id')) {
		array.push(node)
		node = node.parentNode
	}
	return array.reverse()
}

export { build as default };
