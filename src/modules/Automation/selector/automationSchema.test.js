import schema from './mocks/schema'
import { findNodeSchema } from "./automationSchema";

test('empty schema 1', () => {
    expect(findNodeSchema(undefined, 'incident_management_get_one_instances')).toEqual(undefined)
})

test('empty schema 2', () => {
    expect(findNodeSchema({}, 'incident_management_get_one_instances')).toEqual(undefined)
})

test('search for incident_management_get_one_instances', () => {
    expect(findNodeSchema(schema, 'incident_management_get_one_instances')).not.toEqual(undefined)
})

test('search for unknown_name', () => {
    expect(findNodeSchema(schema, 'unknown_name')).toEqual(undefined)
})