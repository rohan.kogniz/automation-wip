import toTree from '../helpers/testUtilities/automationTreeFromSchemaAlt'
import automationTree, {
	findNode, findPathToNode
} from "./automationTree";
import data1 from "./mocks/automation_example_1";
import data2 from "./mocks/automation_example_2";
import data3 from "./mocks/automation_example_3";
import data4 from "./mocks/automation_example_4";
import data5 from "./mocks/automation_example_5";
import data6 from "./mocks/automation_example_6";
import data7 from "./mocks/automation_example_7";

test("empty tree", () => {
	expect(automationTree([])).toEqual(toTree({}));
});

test("data1", () => {
	expect(automationTree(data1[0])).toEqual(toTree({ nodes: data1[1] }));
});

test("data2", () => {
	expect(automationTree(data2[0])).toEqual(toTree({ nodes: data2[1] }));
});

test("data3", () => {
	expect(automationTree(data3[0])).toEqual(toTree({ nodes: data3[1] }));
});

test("data4", () => {
	expect(automationTree(data4[0])).toEqual(toTree({ nodes: data4[1] }));
});

test("data5", () => {
	expect(automationTree(data5[0])).toEqual(toTree({ nodes: data5[1] }));
});

test("data6", () => {
	expect(automationTree(data6[0])).toEqual(toTree({ nodes: data6[1] }));
});

test("data7", () => {
	expect(automationTree(data7[0])).toEqual(toTree({ nodes: data7[1] }));
});

test("findNode: empty nodes", () => {
	const x = automationTree(data5[0]);
	expect(findNode([], x)).toEqual(undefined);
});

test("findNode: valid gui", () => {
	const x = automationTree(data5[0]);
	expect(findNode(data6[0], x)).not.toEqual(undefined);
});

test("findNode: invalid guid", () => {
	const n = {
		id: "invalid_guid",
		children: [],
	};
	const x = toTree({ nodes: n });
	expect(findNode(data6[0], x)).toEqual(undefined);
});

test("findNode: numeric guid", () => {
	const n = {
		id: "0.1201",
		children: [],
	};
	const x = toTree({ nodes: n });
	expect(findNode(data7[0], x)).not.toEqual(undefined);
});

// fn: use dfs to find node with a given guid in TreeNode
const fn = (node, guid) => {
	const id = node.getAttribute("data-node-id");
	if ( id == guid) {
		return node
	}
	if (!node.hasChildNodes()) {
		return
	}
	const children = node.childNodes
	let result = undefined
	for (let i=0; i < children.length; i++) {
		result = fn(children[i], guid)
		if (result) {
			return result
		}
	}
	return result
}

test("findPathToNode", () => {
	const testCases = data4[2]
	const x = toTree({ nodes: data4[1] })
	Object.keys(testCases).forEach(guid => {
		const y = fn(x, guid)
		const z = findPathToNode(y)
		expect(z.map(e => e.getAttribute("data-node-id"))).toEqual(testCases[guid])
	})
})
