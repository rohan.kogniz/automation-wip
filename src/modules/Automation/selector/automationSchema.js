export const findNodeSchema = (automationSchema, schemaName) => {
    for(const category in automationSchema) {
        if(automationSchema[category][schemaName]) {
            return automationSchema[category][schemaName]
        }
    }
    return
}