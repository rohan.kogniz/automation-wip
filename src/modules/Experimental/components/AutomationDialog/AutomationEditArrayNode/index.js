import Add from "core/Icons/Add";
import style from "./style.module.scss";
import React, { useEffect, useState } from "react";
import Button from "core/Button";
import withExtraProps from "../helpers/withExtraProps";
import IconButton from "core/IconButton";
import Delete from "core/Icons/Delete";
import List from '../helpers/AutomationEditArrayNodeList'

const Component = ({ getValue, setValue, viewSchema, children }) => {

	const { key, description } = viewSchema;
	const [arrayValue, setArrayValue] = useState(List.fromJS(getValue(key) || []) || [])

	useEffect(() => {
		setValue({
			[key]: arrayValue.toJS(),
		});
	}, [arrayValue]);

	const handleSetValue = (e, index) => {
		setArrayValue(oldArray => {
			const value = {
				...oldArray.get(index),
				...e
			}
			return oldArray.set(index, value)
		})
	};

	const handleGetValue = (key, index) => {
		return arrayValue.get(index) && arrayValue.get(index)[key]
	};

	const handleRemove = (index) => {
		setArrayValue(oldArray => {
			return oldArray.delete(index)
		})
	};

	const handleAdd = () => {
		setArrayValue(oldArray => {
			return oldArray.add()
		})
	};

	// console.log('keys', arrayValue.keys)

	return (
		<div className={style.arrayBuilder}>
			<label><b>{description}</b></label>
			{arrayValue.keys.map((key, index) => (
				<div key={key} className={style.container}>
					<div>
						{withExtraProps(children, {
							setValue: (f) => handleSetValue(f, index),
							getValue: (f) => handleGetValue(f, index),
						})}
					</div>
					<div className={style.removeContainer}>
						<IconButton
							variant='contained'
							onClick={() => handleRemove(index)}
							className={style.iconButton}
							renderIcon={(props) => <Delete {...props} />}
						/>
					</div>
				</div>
			))}
			<div className={style.addContainer}>
				<Button
					variant="contained"
					onClick={handleAdd}
					renderBeginAdornment={(props) => <Add {...props} />}
				>
					Add
				</Button>
			</div>
		</div>
	);
};

export default Component;