import React, { useEffect, useState } from "react";
import TextBox from "core/TextBox";
import style from "./style.module.scss";

const Component = ({ getValue, setValue, viewSchema }) => {
	const { key, description, minimum, maximum } = viewSchema;

	let [numValue, setNumValue] = useState(getValue(key))

	useEffect(() => {
		setValue({
			[key]: numValue
		})
	}, [numValue])

	const handleChange = (e) => {
		setNumValue( parseInt(e.target.value))
	};

	return (
		<div className={style.numberBuilder}>
			<label>{description}</label>
			<TextBox
				min={minimum}
				max={maximum}
				type="number"
				value={String(numValue)}
				margin="dense"
				onChange={handleChange}
				className={style.textBox}
			></TextBox>
		</div>
	);
};

export default Component;