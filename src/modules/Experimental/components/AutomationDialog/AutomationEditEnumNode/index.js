import style from "./style.module.scss";
import React, { useEffect, useRef, useState } from "react";
import TextBox from "core/TextBox";
import ListItem from "core/ListItem";
import AutoCompleteBase from "core/AutoCompleteBase";
import IconButton from "core/IconButton";
import Close from "core/Icons/Clear";

const Component = ({ getValue, setValue, viewSchema }) => {
	const { key, description } = viewSchema;
	let [open, setOpen] = useState(false);
	let [enumValue, setEnumValue] = useState(getValue(key));
	let ref = useRef();

	useEffect(() => {
		setValue({
			[key]: enumValue
		})
	}, [enumValue])

	const showCloseButton = () => {
		return !open && typeof enumValue !== 'undefined';
	};

	const handleInputFocus = () => {
		setOpen(true)
	};
	
	const handleInputBlur = () => {
		setOpen(false)
	};

	const handleInputClick = () => {
		setOpen(true)
	};

	const handleListItemClick = (item) => {
		setOpen(false)
		setEnumValue(item)
	};

	const handleClearText = (e) => {
		setEnumValue(undefined)
		e.preventDefault()
	};

	const renderListItem = (item) => {
		return (
			<ListItem
				data={item}
				title={item}
				onClick={handleListItemClick}
			/>
		);
	};

	return (
		<div className={style.enumBuilder}>
			<label>{description}</label>
			<AutoCompleteBase
				open={open}
				options={viewSchema.enum}
				className={style.textBox}
				renderInput={(params) => (
					<TextBox
						{...params}
						ref={ref}
						value={String(enumValue ?? "")}
						margin="dense"
						onBlur={handleInputBlur}
						onFocus={handleInputFocus}
						onClick={handleInputClick}
						readOnly
						renderEndAdornment={() =>
							showCloseButton() && (
								<IconButton
									onClick={handleClearText}
									renderIcon={(params) => (
										<Close {...params} />
									)}
								/>
							)
						}
					/>
				)}
				renderListItem={renderListItem}
			/>
		</div>
	);
};

export default Component;