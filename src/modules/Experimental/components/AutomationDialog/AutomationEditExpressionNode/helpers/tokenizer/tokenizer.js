import TokenTypeCodes, { reRules, reEof } from "./tokenTypeCodes";

const key = (obj) => {
	for (const k in obj) {
		if (typeof obj[k] === "undefined") continue;
		return k;
	}
	return TokenTypeCodes.err;
};

export default class Tokenizer {
	constructor(dat) {
		this.buf = dat;
		this.re_rules = new RegExp(reRules, "iy");
		// EOF
		this.re_eof = new RegExp(reEof, "y");
		this.lastIndex = 0;
	}

	hasMoreTokens() {
		this.re_eof.lastIndex = this.lastIndex;
		return !this.re_eof.test(this.buf);
	}

	advance() {
		let m0;
		this.firstIndex = this.lastIndex;
		while ((m0 = this.re_rules.exec(this.buf))) {
			this.lastIndex = this.re_rules.lastIndex;
			const k = key(m0.groups);
			if (k === TokenTypeCodes.skip) {
				this.firstIndex = this.lastIndex;
				continue;
			}
			this._tokenType = k;
			this._tokenValue = m0.groups[this._tokenType];
			break;
		}
	}

	getToken() {
		return {
			from: this.firstIndex,
			to: this.lastIndex,
			type: this._tokenType,
			value: this._tokenValue,
		};
	}
}
