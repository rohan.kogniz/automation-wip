
// build regex for parsing tokens. each named group corresponds to a different token type
export const reRules = '(?<skip>\\s+)' +            // skip
    '|(?<fun>ISEMPTY|LENGTH|DATE)' +                // function names
    '|(?<exp>\\{\\{\\s*[a-zA-Z0-9\\.]+\\s*\\}\\})' +   // moustache exp
    '|(?<cmp>≥|≤|<=|>=|<|>|=)' +                    // comparison
    '|(?<grp>AND|OR)' +                             // group operations
    '|(?<literal>\-?[0-9]+|\'[a-zA-Z0-9]+\')' +     // literals
    '|(?<pair>\\[\-?[0-9]+:[\\sa-z0-9]+\\])' +      // key value
    '|(?<var>[a-zA-Z0-9]+)' +                       // variables
    '|(?<err>\\S+)'                                 // error

export const reEof = '\\s*$'

export const reWhiteSpace = '^\\s*'

const codes = {
    skip: 'skip',
    fun: 'fun',
    exp: 'exp',
    var: 'var',
    literal: 'literal',
    pair: 'pair',
    cmp: 'cmp',
    grp: 'grp',
    err: 'err'
}

export { codes as default }