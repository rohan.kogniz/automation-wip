import TokenTypeCodes from "./tokenTypeCodes";
import tokenizer from "./tokenizer";

test("hasMoreTokens: empty input", () => {
    const tk = new tokenizer("")
	expect(tk.hasMoreTokens()).toEqual(false)
});

test("hasMoreTokens: not empty input", () => {
    const tk = new tokenizer("xyz")
	expect(tk.hasMoreTokens()).toEqual(true)
});

test("advance: LENGTH", () => {
    let result;
    const tk = new tokenizer("LENGTH")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(false)

});

test("advance: incomplete string 1", () => {
    let result
    const tk = new tokenizer("LENGTH {")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.err)
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: incomplete string 2", () => {
    let result
    const tk = new tokenizer("LENGTH {{")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.err)
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: incomplete string 3", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.err)
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: incomplete string 4", () => {
    let result
    const tk = new tokenizer("LENGTH {{}")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.err)
    expect(tk.hasMoreTokens()).toEqual(false)
});


test("advance: invalid token", () => {
    let result
    const tk = new tokenizer("LENGTH %^&")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.err)
    expect(tk.hasMoreTokens()).toEqual(false)
});


test("advance: ISEMPTY", () => {
    let result
    let tk1 = new tokenizer("  ISEMPTY")
	expect(tk1.hasMoreTokens()).toEqual(true)
    tk1.advance()
    result = tk1.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([2, 9])
    expect(tk1.hasMoreTokens()).toEqual(false)
});

test("advance: DATE", () => {
    let result
    let tk1 = new tokenizer("DATE ")
	expect(tk1.hasMoreTokens()).toEqual(true)
    tk1.advance()
    result = tk1.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 4])
    expect(tk1.hasMoreTokens()).toEqual(false)
});

test("advance: MOUSTACHE ", () => {
    let result
    const tk = new tokenizer("{{abc}}")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([0, 7])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: MOUSTACHE 2", () => {
    let result
    const tk = new tokenizer("{{ abc }}")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([0, 9])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}}")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(false)
});


test("advance: var ", () => {
    let result
    const tk = new tokenizer("abc")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.var)
    expect([result.from, result.to]).toEqual([0, 3])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: literal ", () => {
    let result
    const tk = new tokenizer("'abc'")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([0, 5])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: pair ", () => {
    let result
    const tk = new tokenizer("[-9:my text]")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.pair)
    expect([result.from, result.to]).toEqual([0, 12])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: id LENGTH", () => {
    let result
    const tk = new tokenizer("abc LENGTH")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.var)
    expect([result.from, result.to]).toEqual([0, 3])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([4, 10])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE > 0 ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} > 0")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 16])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([17, 18])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE >= 0 ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} >= 0")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 17])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([18, 19])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE < 0 ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} < 0")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 16])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([17, 18])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE = 0 ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} = 0")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 16])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([17, 18])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE = 9 ", () => {
    let result
    const tk = new tokenizer("LENGTH  {{ input.a }}  =  9")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([8, 21])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([23, 24])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([26, 27])
    expect(tk.hasMoreTokens()).toEqual(false)
});


test("advance: LENGTH MOUSTACHE < 0 AND ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} > 0 AND")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 16])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([17, 18])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.grp)
    expect([result.from, result.to]).toEqual([19, 22])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: LENGTH MOUSTACHE < 0 OR ", () => {
    let result
    const tk = new tokenizer("LENGTH {{abc}} > 0 OR")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.fun)
    expect([result.from, result.to]).toEqual([0, 6])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.exp)
    expect([result.from, result.to]).toEqual([7, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([15, 16])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([17, 18])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.grp)
    expect([result.from, result.to]).toEqual([19, 21])
    expect(tk.hasMoreTokens()).toEqual(false)
});

test("advance: locationName = 'calgary' OR ", () => {
    let result
    const tk = new tokenizer("locationName = 'calgary' OR ")
	expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.var)
    expect([result.from, result.to]).toEqual([0, 12])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.cmp)
    expect([result.from, result.to]).toEqual([13, 14])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.literal)
    expect([result.from, result.to]).toEqual([15, 24])
    expect(tk.hasMoreTokens()).toEqual(true)
    tk.advance()
    result = tk.getToken()
    expect(result.type).toEqual(TokenTypeCodes.grp)
    expect([result.from, result.to]).toEqual([25, 27])
    expect(tk.hasMoreTokens()).toEqual(false)
});
