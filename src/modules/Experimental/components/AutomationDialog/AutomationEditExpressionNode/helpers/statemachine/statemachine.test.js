import createMachine from './statemachine'

describe('on-off incomplete', () => {
    function initializeOnOffMachine () {
        const machineDefinition = {
            initialState: 'off',
            off: {
                transitions: {
                    switch: {
                        target: 'on',
                        action() {}
                    }
                }
            },
            on: {
                transitions: {
                    switch: {
                        target: 'off',
                        action() {}
                    }
                }
            }
        }    
        const machine = createMachine(machineDefinition)
        return machine
    }

    let machine
    beforeEach(() => {
        machine = initializeOnOffMachine();
    })

    test('init state', () => {
        expect(machine.state).toEqual('off')
    })

    test('switch transition, from off state', () => {
        expect(() => {
            machine.transition(machine.state, 'switch')
        }).not.toThrow()
        const nextState = machine.state
        expect(nextState).toEqual('on')
    })

})

describe('on-off complete', () => {

    function initializeOnOffMachine () {
        const machineDefinition = {
            initialState: 'off',
            off: {
                transitions: {
                    switch: {
                        target: 'on',
                        action() {}
                    }
                }
            },
            on: {
                transitions: {
                    switch: {
                        target: 'off',
                        action() {}
                    }
                }
            }
        }
        machineDefinition.off.onEnter = jest.fn()
        machineDefinition.off.onExit = jest.fn()
        machineDefinition.off.transitions.switch.action = jest.fn()
    
        machineDefinition.on.onEnter = jest.fn()
        machineDefinition.on.onExit = jest.fn()
        machineDefinition.off.transitions.switch.action = jest.fn()
    
        const machine = createMachine(machineDefinition)
        return machine
    }

    let machine
    beforeEach(() => {
        machine = initializeOnOffMachine();
    })

    test('init state', () => {
        expect(machine.state).toEqual('off')
    })

    test('invalid transition', () => {
        const initState = machine.state
        machine.transition(machine.state, 'unknown')
        expect(machine.state).toEqual(initState)
    })

    test('switch transition, from off state', () => {
        const initState = machine.state
        machine.transition(machine.state, 'switch')
        const nextState = machine.state
        expect(nextState).toEqual('on')
        const mock1 = machine.definition[nextState].onEnter.mock 
        const mock2 = machine.definition[initState].onExit.mock
        const mock3 = machine.definition[initState].transitions.switch.action.mock
        expect(mock1.calls.length).toBe(1)
        expect(mock2.calls.length).toBe(1)
        expect(mock3.calls.length).toBe(1)
        // expect(mock1).toHaveBeenCalledBefore(mock2)
    })

})

