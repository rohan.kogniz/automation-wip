const createMachine = function(machineDefinition) {
    const machine = {
        definition: machineDefinition,
        state: machineDefinition.initialState,
        transition(currentState, event) {
            const csd = machineDefinition[currentState]
            const nst = csd.transitions[event]
            if (!nst) {
                return
            }
            const nsd = machineDefinition[nst.target]
            nst.action && nst.action()
            csd.onExit && csd.onExit()
            nsd.onEnter && nsd.onEnter()
            machine.state = nst.target 
        }
    }
    return machine
}

export { createMachine as default }