const lowerClamp = (oldValue, newValue) => {
	if (newValue === -1) throw "unexpected newValue in lowerClamp";
	if (oldValue === -1) return newValue;
	return newValue < oldValue ? newValue : oldValue;
};

const upperClamp = (oldValue, newValue) => {
	if (newValue === -1) throw "unexpected newValue in upperClamp";
	if (oldValue === -1) return newValue;
	return newValue > oldValue ? newValue : oldValue;
};

export class TermNode {
	constructor() {
		this.from = -1;
		this.to = -1;
		this.name = "Term";
		this.lhs = null;
		this.rhs = null;
		this.cmp = null;
	}

	setLhs(value) {
		this.lhs = value;
		this.from = lowerClamp(this.from, value.from);
		this.to = upperClamp(this.to, value.to);
	}

	setRhs(value) {
		this.rhs = value;
		this.from = lowerClamp(this.from, value.from);
		this.to = upperClamp(this.to, value.to);
	}

	setCmp({ value, from, to }) {
		this.cmp = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	accept(visitor) {
		visitor.visitTermNode(this);
	}
}

export class SubTermNode {
	constructor() {
		this.from = -1;
		this.to = -1;
		this.name = "SubTerm";
		this.fun = null;
		this.var = null;
		this.exp = null;
		this.literal = null;
		this.pair = null;
	}

	setFun({ value, from, to }) {
		this.fun = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	setVar({ value, from, to }) {
		this.var = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	setExp({ value, from, to }) {
		this.exp = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	setLiteral({ value, from, to }) {
		this.literal = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	setPair({ value, from, to }) {
		this.pair = value;
		this.from = lowerClamp(this.from, from);
		this.to = upperClamp(this.to, to);
	}

	accept(visitor) {
		visitor.visitSubTermNode(this);
	}
}
