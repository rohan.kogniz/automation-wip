import tokenizer from '../tokenizer/tokenizer'
import compiler from './compilationEngine'

test('compile subterm 1', () => {
    const ce = new compiler(new tokenizer('LENGTH {{abc}}'))
    const as = ce.compileSubTerm()
    expect(as.fun).toEqual('LENGTH')
    expect(as.exp).toEqual('{{abc}}')
    expect([as.from, as.to]).toEqual([0, 14])
})

test('compile subterm 2', () => {
    const ce = new compiler(new tokenizer('0'))
    const as = ce.compileSubTerm()
    expect(as.literal).toEqual('0')
    expect([as.from, as.to]).toEqual([0, 1])
})

test('compile subterm 3', () => {
    const ce = new compiler(new tokenizer('{{abc}}'))
    const as = ce.compileSubTerm()
    expect(as.exp).toEqual('{{abc}}')
    expect([as.from, as.to]).toEqual([0, 7])
})

test('compile subterm error 1', () => {
    const ce = new compiler(new tokenizer('LENGTH'))
    const as = ce.compileSubTerm()
    expect(as).toEqual(undefined)
})

test('compile subterm error 2', () => {
    const ce = new compiler(new tokenizer('LENGTH {{abc'))
    const as = ce.compileSubTerm()
    expect(as).toEqual(undefined)
})

test('compile term', () => {
    const str = 'LENGTH {{abc}} < 0'
    const ce = new compiler(new tokenizer(str))
    const as = ce.compileTerm()
    expect(as.lhs).not.toEqual(undefined)
    expect(as.cmp).toEqual('<')
    expect(as.rhs).not.toEqual(undefined)
    expect([as.from, as.to]).toEqual([0, str.length])
})

test('compile term error 1', () => {
    const ce = new compiler(new tokenizer('LENGTH {{abc}} <'))
    const as = ce.compileTerm()
    expect(as).toEqual(undefined)
})

test('compile term error 2', () => {
    const ce = new compiler(new tokenizer('LENGTH {{abc}}'))
    const as = ce.compileTerm()
    expect(as).toEqual(undefined)
})

test('compile term error 3', () => {
    const ce = new compiler(new tokenizer('LENGTH {{abc'))
    const as = ce.compileTerm()
    expect(as).toEqual(undefined)
})