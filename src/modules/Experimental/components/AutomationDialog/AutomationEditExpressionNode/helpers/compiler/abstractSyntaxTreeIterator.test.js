import tokenizer from "../tokenizer/tokenizer.js";
import iterator from "./abstractSyntaxTreeIterator";

test("empty string", () => {
	const str = "";
	const tk = new tokenizer(str);
	const enter = jest.fn();

	iterator({
		tokenizer: tk,
		enter,
	});
	expect(enter).toBeCalledTimes(0);
});

test("LENGTH {{abc}} > 0", () => {
	const str = "LENGTH {{abc}} > 0";
	const tk = new tokenizer(str);
	let counters = {
		term: 0,
		subterm: 0,
	};
	const enter = ({ name }) => {
		name = name.toLowerCase();
		if (name === "term") {
			counters.term += 1;
		}
		if (name === "subterm") {
			counters.subterm += 1;
		}
	};
    const exit = ({ name }) => {
		name = name.toLowerCase();
		if (name === "term") {
			counters.term += 1;
		}
		if (name === "subterm") {
			counters.subterm += 1;
		}
	};
	iterator({
		tokenizer: tk,
		enter,
        exit
	});
    expect(counters.term).toEqual(2)
    expect(counters.subterm).toEqual(4)
});
