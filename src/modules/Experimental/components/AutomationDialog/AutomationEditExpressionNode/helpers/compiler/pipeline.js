export default class Pipeline {
	constructor(tokenizer) {
		this.tokenizer = tokenizer;
		this.tokens = [null, null, null];
		// fill the pipeline
		this.advance();
		this.advance();
		this.advance();
	}

	advance() {
		this.tokens[0] = this.tokens[1];
		this.tokens[1] = this.tokens[2];
		if (this.tokenizer.hasMoreTokens()) {
			this.tokenizer.advance();
			this.tokens[2] = this.tokenizer.getToken();
		} else {
			this.tokens[2] = null;
		}
	}

	peek() {
		return this.tokens[0];
	}

	peeknext() {
		return this.tokens[1];
	}

	peeknextnext() {
		return this.tokens[2];
	}

	next() {
		this.advance();
	}

	eat(expectedTokenValue) {
		const { value } = this.tokens[0];
		if (expectedTokenValue === value) {
			this.advance();
		} else {
			throw `parse error at ${expectedTokenValue}, found ${value}`;
		}
	}
}
