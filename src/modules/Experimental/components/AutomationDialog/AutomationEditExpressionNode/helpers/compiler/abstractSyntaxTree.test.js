import { TermNode, SubTermNode } from "./abstractSyntaxTree"

test('visit TermNode', () => {
    const visitor = {
        visitTermNode: jest.fn()
    }
    const n = new TermNode()
    n.accept(visitor)

    expect(visitor.visitTermNode).toBeCalledTimes(1)
})

test('visit SubTermNode', () => {
    const visitor = {
        visitSubTermNode: jest.fn()
    }
    const n = new SubTermNode()
    n.accept(visitor)

    expect(visitor.visitSubTermNode).toBeCalledTimes(1)
})