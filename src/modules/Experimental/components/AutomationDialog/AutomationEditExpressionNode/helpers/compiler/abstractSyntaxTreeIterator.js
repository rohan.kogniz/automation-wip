import compiler from "./compilationEngine";

export default function abstractSyntaxTreeIterator ({ tokenizer, enter = undefined, exit = undefined }) {
	const visitor = {
		visitTermNode(node) {
			enter && enter(node);
			node.lhs.accept(visitor);
			node.rhs && node.rhs.accept(visitor);
			exit && exit(node);
		},
		visitSubTermNode(node) {
			enter && enter(node);
			exit && exit(node);
		},
	};
	const ce = new compiler(tokenizer);
	const node = ce.compileTerm();
	node && node.accept(visitor);
};
