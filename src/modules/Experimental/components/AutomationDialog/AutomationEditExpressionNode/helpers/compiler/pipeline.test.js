import codes from "../tokenizer/tokenTypeCodes";
import tokenizer from '../tokenizer/tokenizer'
import pipeline from './pipeline'

test('empty input', () => {
    const tk = new tokenizer('')
    const pi = new pipeline(tk)
    expect(pi.peek()).toEqual(null)
    expect(pi.peeknext()).toEqual(null)
    expect(pi.peeknextnext()).toEqual(null)
})

test('LENGTH', () => {
    const tk = new tokenizer('LENGTH')
    const pi = new pipeline(tk)
    expect(pi.peek().value).toEqual('LENGTH')
    expect(pi.peeknext()).toEqual(null)
    expect(pi.peeknextnext()).toEqual(null)
})

test('LENGTH {{abc}}', () => {
    const tk = new tokenizer('LENGTH {{abc}}')
    const pi = new pipeline(tk)
    expect(pi.peek().value).toEqual('LENGTH')
    expect(pi.peeknext().value).toEqual('{{abc}}')
    expect(pi.peeknextnext()).toEqual(null)
})

test('LENGTH {{abc', () => {
    const tk = new tokenizer('LENGTH {{abc')
    const pi = new pipeline(tk)
    expect(pi.peek().value).toEqual('LENGTH')
    expect(pi.peeknext().type).toEqual(codes.err)
    expect(pi.peeknextnext()).toEqual(null)
})

test('LENGTH {{abc =', () => {
    const tk = new tokenizer('LENGTH {{abc =')
    const pi = new pipeline(tk)
    expect(pi.peek().value).toEqual('LENGTH')
    expect(pi.peeknext().type).toEqual(codes.err)
    expect(pi.peeknextnext().type).toEqual(codes.cmp)
})


test('LENGTH {{abc}} = BLAH AND ', () => {
    const expected = [codes.fun, codes.exp, codes.cmp, codes.var, codes.grp]
    const tk = new tokenizer('LENGTH {{abc}} = BLAH AND ')
    const pi = new pipeline(tk)
    let count = 0
    while (pi.peek()) {
        expect(pi.peek().type).toEqual(expected[count])
        count = count + 1
        pi.next()
    }
})