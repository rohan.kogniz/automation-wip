import codes from "../tokenizer/tokenTypeCodes";
import { SubTermNode, TermNode } from "./abstractSyntaxTree";
import pipeline from "./pipeline";

/*
Term -> SubTerm cmp SubTerm
SubTerm -> (fun)? (var | exp | literal)
cmp -> = | < | > | <= | >=
exp -> moustache
var -> variable
literal -> number | string
fun -> LENGTH | ISEMPTY | DATE0.
*/

export default class Compiler {
	constructor(tokenizer) {
		this.pipeline = new pipeline(tokenizer);
	}

	compileTerm() {
		let ast_node = new TermNode();
	
		let subTerm = this.compileSubTerm();
		if (!subTerm) {
			return;
		}
	
		ast_node.setLhs(subTerm);
	
		let peek = this.pipeline.peek();
		if (!peek) {
			return;
		}
	
		if (peek.type !== codes.cmp) {
			return;
		}
		ast_node.setCmp(peek);
	
		this.pipeline.next();
	
		subTerm = this.compileSubTerm();
		if (!subTerm) {
			return;
		}
		ast_node.setRhs(subTerm);
		return ast_node;
	};

	compileSubTerm () {
		let ast_node = new SubTermNode();
	
		let peek = this.pipeline.peek();
		if (!peek) {
			return;
		}
	
		if (peek.type === codes.fun) {
			ast_node.setFun(peek)
	
			this.pipeline.next();
			peek = this.pipeline.peek();
			if (!peek) {
				return;
			}
		}
	
		if (peek.type === codes.var) {
			ast_node.setVar(peek)
		} else if (peek.type === codes.exp) {
			ast_node.setExp(peek)
		} else if (peek.type === codes.literal) {
			ast_node.setLiteral(peek)
		} else if (peek.type === codes.pair) {
			ast_node.setPair(peek)
		} else {
			return;
		}
	
		this.pipeline.next();
		return ast_node;
	};
}

