const input =  {
	type: "FilterCondition",
	config: {
		left: {
			type: "EmptyFunctionDef",
			config: {
				input: "{{pqr}}",
			},
		},
		condition: "=",
		right: 0,
	},
}

const output = [["ISEMPTY {{pqr}} = 0"]]

const io = [input, output];
export { io as default };