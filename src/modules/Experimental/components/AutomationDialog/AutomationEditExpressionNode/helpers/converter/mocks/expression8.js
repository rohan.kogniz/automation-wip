const input = {
    type: "FilterCondition",
    config: {
        left: {
            type: "CountFunctionDef",
            config: {
                input: "{{ input.a }}",
            },
        },
        condition: "=",
        right: 9,
    },
}

const output = [["LENGTH  {{ input.a }}  =  9"]]

const io = [input, output];
export { io as default };
