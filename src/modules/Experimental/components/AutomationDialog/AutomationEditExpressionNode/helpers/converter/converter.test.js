import { fromJson, toJson } from './converter'
import data1 from './mocks/expression1'
import data2 from './mocks/expression2'
import data3 from './mocks/expression3'
import data4 from './mocks/expression4'
import data5 from './mocks/expression5'
import data6 from './mocks/expression6'
import data7 from './mocks/expression7'
import data8 from './mocks/expression8'

test('fromJson: empty JSON', () => {
    const condition = {}
    expect(fromJson(condition)).toEqual([[]])
})

test('fromJson: simple filter 1', () => {
    expect(fromJson(data1[0])).toEqual(data1[1])
})

test('fromJson: simple filter 2', () => {
    expect(fromJson(data2[0])).toEqual(data2[1])
})

test('fromJson: simple filter 3', () => {
    expect(fromJson(data3[0])).toEqual(data3[1])
})

test('fromJson: simple filter 4', () => {
    expect(fromJson(data4[0])).toEqual(data4[1])
})

test('fromJson: simple filter 5', () => {
    expect(fromJson(data5[0])).toEqual(data5[1])
})

test('fromJson: simple filter 6', () => {
    expect(fromJson(data6[0])).toEqual(data6[1])
})

test('fromJson: simple filter 7', () => {
    expect(fromJson(data7[0])).toEqual(data7[1])
})

test('toJson: empty JSON', () => {
    const condition = {}
    expect(toJson(condition)).toEqual(undefined)
})

test('toJson: empty JSON 2', () => {
    expect(toJson(undefined)).toEqual(undefined)
})

test('toJson: empty array 1', () => {
    const condition = []
    expect(toJson(condition)).toEqual(undefined)
})

test('toJson: empty array 2', () => {
    const condition = [[]]
    expect(toJson(condition)).toEqual([[]])
})

test('toJson: simple filter 1', () => {
    expect(toJson(data1[1])).toEqual(data1[0])
})

test('toJson: simple filter 2', () => {
    expect(toJson(data2[1])).toEqual(data2[0])
})

test('toJson: simple filter 3', () => {
    expect(toJson(data3[1])).toEqual(data3[0])
})

test('toJson: simple filter 4', () => {
    expect(toJson(data4[1])).toEqual(data4[0])
})

test('toJson: simple filter 5', () => {
    expect(toJson(data5[1])).toEqual(data5[0])
})

test('toJson: simple filter 6', () => {
    expect(toJson(data6[1])).toEqual(data6[0])
})

test('toJson: simple filter 7', () => {
    expect(toJson(data7[1])).toEqual(data7[0])
})

// test('toJson: simple filter 8', () => {
//     expect(toJson(data8[1])).toEqual(data8[0])
// })