const input = {
	type: "GroupCondition",
	config: {
		operator: "AND",
		conditions: [
			{
				type: "FilterCondition",
				config: {
					left: {
						type: "CountFunctionDef",
						config: {
							input: "{{pqr}}",
						},
					},
					condition: "=",
					right: 0,
				},
			},
			{
				type: "FilterCondition",
				config: {
					left: {
						type: "CountFunctionDef",
						config: {
							input: "{{rst}}",
						},
					},
					condition: "=",
					right: 0,
				},
			},
		],
	},
};

const output = [["LENGTH {{pqr}} = 0", "LENGTH {{rst}} = 0"]]

const io = [input, output];
export { io as default };