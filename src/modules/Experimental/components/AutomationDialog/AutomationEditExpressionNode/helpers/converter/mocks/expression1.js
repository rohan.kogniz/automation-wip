const input = {
    type: "FilterCondition",
    config: {
        left: "{{ pqr }}",
        condition: "=",
        right: 0,
    }
}

const output = [["{{ pqr }} = 0"]]

const io = [input, output];
export { io as default };