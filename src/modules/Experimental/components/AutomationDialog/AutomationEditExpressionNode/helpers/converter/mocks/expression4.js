const input = {
	type: "FilterCondition",
	config: {
		left: {
			type: "TimestampFunctionDef",
			config: {
				input: "{{pqr}}",
			},
		},
		condition: "=",
		right: 0,
	},
};

const output = [["DATE {{pqr}} = 0"]]

const io = [input, output];
export { io as default };