import _ from "lodash";
import tokenizer from "../tokenizer/tokenizer";
import iterator from "../compiler/abstractSyntaxTreeIterator";

const Mapping = {
	CountFunctionDef: "LENGTH",
	EmptyFunctionDef: "ISEMPTY",
	TimestampFunctionDef: "DATE",
};

const ReverseMapping = {
	LENGTH: "CountFunctionDef",
	ISEMPTY: "EmptyFunctionDef",
	DATE: "TimestampFunctionDef",
};

const parseFilterCondition = (obj) => {
	const lhs = obj.config.left.hasOwnProperty("type")
		? Mapping[obj.config.left.type] + " " + obj.config.left.config.input
		: obj.config.left;

	const rhs = obj.config.right.hasOwnProperty("type")
		? Mapping[obj.config.right.type] + " " + obj.config.right.config.input
		: obj.config.right;

	return lhs + " " + obj.config.condition + " " + rhs;
};

const parseAndGroupCondition = (obj) => {
	const arr = [];
	for (const item of obj.config.conditions) {
		if (item.type === "FilterCondition") {
			arr.push(parseFilterCondition(item));
		} else {
			throw "invalid expression";
		}
	}
	return arr;
};

const parseOrGroupCondition = (obj) => {
	const arr = [];
	for (const item of obj.config.conditions) {
		if (item.type === "FilterCondition") {
			arr.push([parseFilterCondition(item)]);
		} else {
			if (item.config.operator == "AND") {
				arr.push(parseAndGroupCondition(item));
			} else {
				throw "invalid expression";
			}
		}
	}
	return arr;
};

const parse = (obj) => {
	if (obj.type === "FilterCondition") {
		return [[parseFilterCondition(obj)]];
	}
	if (obj.type === "GroupCondition") {
		if (obj.config.operator == "AND") {
			return [parseAndGroupCondition(obj)];
		} else {
			return parseOrGroupCondition(obj);
		}
	}
};

export const fromJson = (obj) => {
	if (_.isEmpty(obj)) {
		return [[]];
	}

	try {
		return parse(obj);
	} catch {
		return [[]];
	}
};

const buildFilterCondition = (str) => {
	const tk = new tokenizer(str);

	let output = {
		type: "FilterCondition",
		config: {},
	};

	const buildSubTerm = (x) => {
		if (x.fun) {
			return {
				type: ReverseMapping[x.fun],
				config: {
					input: x.exp || Number(x.literal),
				},
			};
		}
		return x.exp || Number(x.literal);
	};

	const enter = (e) => {
		if (e.name === "Term") {
			output.config.left = buildSubTerm(e.lhs);
			output.config.right = buildSubTerm(e.rhs);
			output.config.condition = e.cmp;
		}
	};

	iterator({
		tokenizer: tk,
		enter,
	});

	return output;
};

const buildAndCondition = (arr) => {
	if (arr.length === 0) {
		return [[]]
	}

	if (arr.length === 1) {
		return buildFilterCondition(arr[0]);
	}

	let output = {
		type: "GroupCondition",
		config: {
			operator: "AND",
			conditions: [],
		},
	};

	for (const e of arr) {
		output.config.conditions.push(buildFilterCondition(e));
	}
	return output;
};

const buildOrCondition = (arr) => {
	if (arr.length == 1) {
		return buildAndCondition(arr[0]);
	}

	let output = {
		type: "GroupCondition",
		config: {
			operator: "OR",
			conditions: [],
		},
	};

	for (const e of arr) {
		output.config.conditions.push(buildAndCondition(e));
	}
	return output;
};

export const toJson = (obj) => {
	if (_.isEmpty(obj)) {
		return;
	}

	return buildOrCondition(obj)
};
