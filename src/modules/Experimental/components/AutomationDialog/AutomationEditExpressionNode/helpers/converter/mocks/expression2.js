const input = {
    type: "FilterCondition",
    config: {
        left: {
            type: "CountFunctionDef",
            config: {
                input: "{{pqr}}",
            },
        },
        condition: "=",
        right: 0,
    },
}

const output = [["LENGTH {{pqr}} = 0"]]

const io = [input, output];
export { io as default };