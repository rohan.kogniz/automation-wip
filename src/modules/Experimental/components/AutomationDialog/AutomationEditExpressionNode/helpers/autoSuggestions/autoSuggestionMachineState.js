const codes = {
    ini: 'ini',
    lhsFun: 'lhsFun',
    lhsVar: 'lhsVar',
    lhsExp: 'lhsExp',
    cmp: 'cmp',
    rhsFun: 'rhsFun',
    rhsVar: 'rhsVar',
    rhsExp: 'rhsExp',
    err: 'err',
    fin: 'fin'
}

export default codes;