import code from "../tokenizer/tokenTypeCodes";
import State from "./autoSuggestionMachineState"
import createMachine from '../statemachine/statemachine'

const md = {
	initialState: State.ini,
	[State.ini]: {
        transitions: {
            [code.fun]: { target: State.lhsFun },
            [code.var]: { target: State.lhsVar },
            [code.exp]: { target: State.lhsExp },
            [code.cmp]: { target: State.err },
        }
	},
    [State.lhsFun]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.lhsVar },
            [code.exp]: { target: State.lhsExp },
            [code.cmp]: { target: State.err },
        }
	},
    [State.lhsExp]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.err },
            [code.exp]: { target: State.err },
            [code.cmp]: { target: State.cmp },
        }
    },
    [State.lhsVar]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.err },
            [code.exp]: { target: State.err },
            [code.cmp]: { target: State.cmp },
        }
    },    
    [State.cmp]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.rhsFun },
            [code.var]: { target: State.fin },
            [code.exp]: { target: State.fin },
            [code.cmp]: { target: State.err },
            [code.literal]: { target:State.fin },
        }
    },    
    [State.rhsFun]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.fin },
            [code.exp]: { target: State.fin },
            [code.literal]: { target:State.err },
            [code.cmp]: { target: State.err },
        }
    },
    [State.err]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.err },
            [code.exp]: { target: State.err },
            [code.cmp]: { target: State.err },
        }
    },    
    [State.fin]: {
        name: undefined,
        transitions: {
            [code.fun]: { target: State.err },
            [code.var]: { target: State.err },
            [code.exp]: { target: State.err },
            [code.cmp]: { target: State.err },
        }
    }
};

export default function createMachineFromDefinition() {
    let machine = createMachine(md)
    const sm = {
        getState() {
            return machine.state
        },
        transition(tokenType, tokenValue) {
            machine.transition(machine.state, tokenType)
        },
        suggestions() {
            const transitions = md[machine.state].transitions
            return Object.keys(transitions).filter(key => transitions[key].target !== State.err) 
        }
    }
    return sm
}
