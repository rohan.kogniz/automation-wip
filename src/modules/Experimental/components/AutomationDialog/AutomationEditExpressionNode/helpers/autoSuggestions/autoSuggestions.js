import code from "../tokenizer/tokenTypeCodes";
import createMachine from "./machinedefinition";

export default function createTags (tokenizer) {
	const sm = createMachine();
	const tk = tokenizer;

	let tags = [
		{
			from: -1,
			to: -1,
			type: code.skip,
			value: "",
			state: sm.getState(),
			suggestions: sm.suggestions(),
		},
	];

	while (tk.hasMoreTokens()) {
		tk.advance();
		const token = tk.getToken();
		sm.transition(token.type, token.value);
		tags.push({
			...token,
			state: sm.getState(),
			suggestions: sm.suggestions(),
		});
	}

	return new tokenTags(tags);
};

export class tokenTags {
	constructor(tags) {
		this.tags = tags;
	}

	internalIndexAtCursor(pos) {
		if (this.tags.length === 0) {
			throw "tags cannot be empty";
		}

		if (pos <= 0) {
			return 0;
		}
		const maxTo = this.tags[this.tags.length - 1].to;
		if (pos < maxTo) {
			return this.tags.findIndex((item) => pos < item.to) - 1;
		}
		if (pos == maxTo) {
			return this.tags.length - 2;
		}
		return this.tags.length - 1;
	}

	atCursor(pos) {
		return this.tags[this.internalIndexAtCursor(pos)].suggestions;
	}
}
