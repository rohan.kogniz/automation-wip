import createMachine from './machinedefinition'
import code from "../tokenizer/tokenTypeCodes";

let machine
beforeEach(() => {
    machine = createMachine()
})

test('init state', () => {
    expect(machine.getState()).toEqual('ini')
})

test('LENGTH {', () => {
    machine.transition(code.fun, 'LENGTH')
    expect(machine.getState()).toEqual('lhsFun')
    machine.transition(code.err, '{')
    expect(machine.getState()).toEqual('lhsFun')
})


test('LENGTH {{abc}} = 0', () => {
    machine.transition(code.fun, 'LENGTH')
    expect(machine.getState()).toEqual('lhsFun')
    machine.transition(code.exp, '{{abc}}')
    expect(machine.getState()).toEqual('lhsExp')
    machine.transition(code.cmp, '=')
    expect(machine.getState()).toEqual('cmp')
})

test('suggestions', () => {
    expect(machine.suggestions()).toEqual([code.fun, code.var, code.exp])
    machine.transition(code.fun, 'LENGTH')
    expect(machine.suggestions()).toEqual([code.var, code.exp])
    machine.transition(code.exp, '{{abc}}')
    expect(machine.suggestions()).toEqual([code.cmp])
    machine.transition(code.cmp, '=')
    expect(machine.suggestions()).toEqual([code.fun, code.var, code.exp, code.literal])
})