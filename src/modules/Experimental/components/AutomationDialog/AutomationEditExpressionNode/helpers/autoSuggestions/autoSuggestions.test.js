import tokenizer from '../tokenizer/tokenizer'
import createTagger from './autoSuggestions'

test('init state', () => {
    const tags = createTagger(new tokenizer('')).tags
    expect(tags.length).toEqual(1)
})

test('LENGTH {{abc}} = 0', () => {
    const tags = createTagger(new tokenizer('LENGTH {{abc}} = 0')).tags
    expect(tags.length).toEqual(5)
})

test('cursorPos: empty string', () => {
    const tagger = createTagger(new tokenizer(''))
    expect(tagger.internalIndexAtCursor(0)).toEqual(0)
})

test('cursorPos: empty string 2', () => {
    const tagger = createTagger(new tokenizer(' '))
    expect(tagger.internalIndexAtCursor(1)).toEqual(0)
})

test('cursorPos: LENGTH', () => {
    const tagger = createTagger(new tokenizer('LENGTH'))
    expect(tagger.internalIndexAtCursor(6)).toEqual(0)
})

test('cursorPos: LENGTH ', () => {
    const tagger = createTagger(new tokenizer('LENGTH '))
    expect(tagger.internalIndexAtCursor(7)).toEqual(1)
})

test('cursorPos: LENGTH {{abc}} = 0', () => {
    const tagger = createTagger(new tokenizer('LENGTH {{abc}} = 0'))
    expect(tagger.internalIndexAtCursor(0)).toEqual(0)
    expect(tagger.internalIndexAtCursor(2)).toEqual(0)
    expect(tagger.internalIndexAtCursor(6)).toEqual(1)
    expect(tagger.internalIndexAtCursor(7)).toEqual(1)
    expect(tagger.internalIndexAtCursor(13)).toEqual(1)
    expect(tagger.internalIndexAtCursor(14)).toEqual(2)
    expect(tagger.internalIndexAtCursor(15)).toEqual(2)
    expect(tagger.internalIndexAtCursor(16)).toEqual(3)
    expect(tagger.internalIndexAtCursor(17)).toEqual(3)
    expect(tagger.internalIndexAtCursor(18)).toEqual(3)
})