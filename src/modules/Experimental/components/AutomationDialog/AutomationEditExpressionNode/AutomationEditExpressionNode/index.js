import Button from "core/Button";
import Add from "core/Icons/Add";
import React, { useEffect, useState } from "react";
import style from "./style.module.scss";
import IconButton from "core/IconButton";
import Delete from "core/Icons/Delete";
import ExpressionBuilder from "../ExpressionTextBox";
import { fromJson, toJson } from "../helpers/converter/converter";
import List2 from '../../helpers/AutomationEditExpressionNodeList'

const Component = ({ getValue, setValue, viewSchema }) => {

	const { key, description } = viewSchema;
	let [orCategory, setOrCategory] = useState(List2.fromJS(fromJson(getValue(key)) || []))

	useEffect(() => {
		setValue({
			[key]: toJson(orCategory.toJS()),
		});
	}, [orCategory]);

	const handleAddAndCategory = (orIndex) => {
		setOrCategory((oldOrCategory) => {
			return oldOrCategory.addAnd(orIndex)
		});
	};

	const handleAddOrCategory = () => {
		setOrCategory((oldOrCategory) => {
			return oldOrCategory.addOr()
		});
	};

	const handleOnChange = (text, orIndex, andIndex) => {
		setOrCategory((oldOrCategory) => {
			return oldOrCategory.set(orIndex, andIndex, text)
		});
	};

	const handleOnDelete = (orIndex, andIndex) => {
		setOrCategory(oldOrCategory => {
			return oldOrCategory.delete(orIndex, andIndex)
		})
	}

	const renderAndCategory = (orIndex) => {
		return (
			<div>
				<label>
					{orIndex == 0 ? `Only continue if...` : `OR continue if...`}
				</label>
				{orCategory.andKeys(orIndex).map((key, andIndex) => (
					<div key={key} className={style.expressionContainer}>
						<div className={style.expressionBuilder}>
							<ExpressionBuilder
								value={orCategory.get(orIndex, andIndex) || ''}
								onChange={(f) =>
									handleOnChange(f, orIndex, andIndex)
								}
							/>
						</div>
						<div>
							<IconButton
								variant="contained"
								onClick={() => handleOnDelete(orIndex, andIndex)}
								renderIcon={(props) => <Delete {...props} />}
							/>
						</div>
					</div>
				))}
				<div className={style.buttonGroup}>
					<Button
						onClick={() => handleAddAndCategory(orIndex)}
						renderBeginAdornment={(props) => <Add {...props} />}
					>
						And
					</Button>
					{orIndex === orCategory.orKeys.length - 1 && (
						<Button
							onClick={handleAddOrCategory}
							renderBeginAdornment={(props) => <Add {...props} />}
						>
							Or
						</Button>
					)}
				</div>
			</div>
		);
	};

	return (
		<div className={style.container}>
			<label>
				<b>{description}</b>
			</label>
			{orCategory.orKeys.map((key, index) => (
				<div key={key}>{renderAndCategory(index)}</div>
			))}
		</div>
	);
};

export default Component;
