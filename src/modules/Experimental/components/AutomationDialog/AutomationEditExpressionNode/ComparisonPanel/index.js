import Chip from "core/Chip";
import React from "react";
import styles from "./style.module.scss";

const Comparison = ({ setText }) => {
	return (
		<div className={styles.comparison}>
			<p>Comparison:</p>
			<div className={styles.chips}>
				<div>
					<Chip onClick={() => setText(" < ")} label='<' />
				</div>
				<div>
					<Chip onClick={() => setText(" \u2264 ")} label='≤' />
				</div>
				<div>
					<Chip onClick={() => setText(" > ")} label='>' />
				</div>
				<div>
					<Chip onClick={() => setText(" \u2265 ")} label='≥' />
				</div>
				<div>
					<Chip onClick={() => setText(" = ")} label='=' />
				</div>
			</div>
		</div>
	);
};

export { Comparison as default };
