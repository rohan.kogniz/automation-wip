import React, { useContext, useRef, useState } from "react";
import styles from "./style.module.scss";
import TextBox from "core/TextBox";
import ListItem from "core/ListItem";
import MoustacheContext from "modules/Experimental/context/MoustacheContext";

export default function Component({ setText, setPanelhasFocus }) {
	let ref = useRef(null);
	const context = useContext(MoustacheContext);

	let [textValue, setTextValue] = useState("");
	let [options, setOptions] = useState(context);

	const filter = (search) =>
		context.filter((name) =>
			name.toLowerCase().includes(search.toLowerCase())
		);

	const handleFocus = (e) => {
		setPanelhasFocus(true);
	};

	const handleBlur = (e) => {
		setPanelhasFocus(false);
	};

    const handleListItemClick = (selectedText) => {
		setTextValue(selectedText);
		setOptions(filter(selectedText));
        setText(selectedText)
	};

	const handleInputChange = (e) => {
		const typedText = e.target.value;
		setTextValue(typedText);
		setOptions(filter(typedText));
	};

	const handleInputClick = (e) => {
		// ref.current.focus();
	};

	return (
		<div className={styles.moustache}>
			<p>Fields:</p>
			<div>
				<TextBox
					ref={ref}
					type="text"
					value={textValue}
					variant="outlined"
					margin="dense"
					onBlur={handleBlur}
					onFocus={handleFocus}
					onClick={handleInputClick}
					onChange={handleInputChange}
				/>
				<div className={styles.list}>
					{options &&
						options.map((el, index) => (
							<ListItem
                                key={index}
								data={el}
								title={el}
								onClick={handleListItemClick}
							/>
						))}
				</div>
			</div>
		</div>
	);
}
