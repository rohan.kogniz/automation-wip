import Chip from "core/Chip";
import React from "react";
import styles from "./style.module.scss";

const Formulas = ({ setText }) => {
	return (
		<div className={styles.formulas}>
			<p>Formulas:</p>
			<div className={styles.chips}>
				<div>
					<Chip onClick={() => setText("LENGTH ")} label="LENGTH" />
				</div>
				<div>
					<Chip onClick={() => setText("ISEMPTY ")} label="ISEMPTY" />
				</div>
				<div>
					<Chip onClick={() => setText("DATE ")} label="DATE" />
				</div>
			</div>
		</div>
	);
};

export { Formulas as default };
