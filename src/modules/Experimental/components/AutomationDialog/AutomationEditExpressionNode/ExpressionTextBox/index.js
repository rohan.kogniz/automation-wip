import code from "../helpers/tokenizer/tokenTypeCodes";
import React, { useEffect, useState } from "react";
import style from "./style.module.scss";
import TextArea from "core/TextArea";
import tokenizer from "../helpers/tokenizer/tokenizer";
import FormulaPanel from "../FormulaPanel";
import MoustachePanel from "../MoustachePanel2";
import autoSuggestions from "../helpers/autoSuggestions/autoSuggestions";
import ComparisonPanel from "../ComparisonPanel";

const Builder = ({ value, onChange }) => {
	let [text, setText] = useState(value);
	let [show, setShow] = useState(false);
	let [cursorPos, setCursorPos] = useState(0);
	let [suggestions, setAutoSuggestions] = useState(autoSuggestions(new tokenizer(value)));

	useEffect(() => {
		onChange(text)
	}, [text])

	const setTextBoxState = (value, pos = value.length) => {
		setText(value);
		setAutoSuggestions(autoSuggestions(new tokenizer(value)));
		setCursorPos(pos);
	};

	const handleFocus = (e) => {
		setShow(true);
	};

	const handleBlur = (e) => {
		setShow(false);
	};

	const handleMouseDown = (e) => {
		e.preventDefault();
	};

	const handleTextChange = (e) => {
		setTextBoxState(e.target.value, e.target.selectionStart);
	};

	const handleKeyUp = (e) => {
		if (e.key === "ArrowLeft" || e.key === "ArrowRight") {
			setCursorPos(e.target.selectionEnd);
		}
	};

	const x = suggestions.atCursor(cursorPos);
	const showFunctions = x.indexOf(code.fun) > -1;
	const showMoustache = x.indexOf(code.exp) > -1;
	const showComparisons = x.indexOf(code.cmp) > -1;

	const handleSetPanelhasFocus = (val) => {
		setShow(val);
	};

	return (
		<div className={style.expressionBuilder}>
			<TextArea
				value={text}
				onBlur={handleBlur}
				margin="dense"
				onFocus={handleFocus}
				onKeyUp={handleKeyUp}
				onChange={handleTextChange}
				spellCheck="false"
				autoAdjustHeight
			/>
			{show && (
				<div className={style.popup} onMouseDown={handleMouseDown}>
					{showFunctions && (
						<FormulaPanel
							setText={(v) => setTextBoxState(text + v + " ")}
						/>
					)}
					{showMoustache && (
						<MoustachePanel
							setText={(v) => setTextBoxState(text + v + " ")}
							setPanelhasFocus={handleSetPanelhasFocus}
						/>
					)}
					{showComparisons && (
						<ComparisonPanel
							setText={(v) => setTextBoxState(text + v + " ")}
						/>
					)}
				</div>
			)}
		</div>
	);
};

export default Builder;
