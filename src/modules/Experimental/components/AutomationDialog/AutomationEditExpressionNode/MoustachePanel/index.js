import React, { useContext, useRef, useState } from "react";
import styles from "./style.module.scss";
import TextBox from "core/TextBox";
import ListItem from "core/ListItem";
import MoustacheContext from "modules/Experimental/context/MoustacheContext";
import AutoCompleteBase from "core/AutoCompleteBase";

const Moustache = ({ setText, setPanelhasFocus }) => {
    let ref = useRef(null);
	const context = useContext(MoustacheContext);

	let [textValue, setTextValue] = useState("");
	let [options, setOptions] = useState(context);


	const filter = (search) =>
		context.filter((name) =>
			name.toLowerCase().includes(search.toLowerCase())
		);

	const handleInputChange = (e) => {
		const typedText = e.target.value;
		setTextValue(typedText);
		setOptions(filter(typedText));
	};

    const handleInputClick = (e) => {
        ref.current.focus();
	};

	const handleListItemClick = (selectedText) => {
		setTextValue(selectedText);
		setOptions(filter(selectedText));
        setText(selectedText)
	};

	const handleFocus = (e) => {
		setPanelhasFocus(true);
	};

	const handleBlur = (e) => {
		setPanelhasFocus(false)
	}

	const renderListItem = (item) => {
		return (
			<ListItem
				data={item}
				title={item}
				onClick={handleListItemClick}
			/>
		);
	};

	return (
		<div className={styles.moustache}>
			<p>Field</p>
            <div>
            <AutoCompleteBase
					open
					options={options}
                    className={styles.textBox}
					renderInput={(params) => (
						<TextBox
							{...params}
                            ref={ref}
							type="text"
							value={textValue}
							variant='outlined'
							margin='dense'
							onBlur={handleBlur}
							onFocus={handleFocus}
                            onClick={handleInputClick}
							onChange={handleInputChange}
						/>
					)}
					renderListItem={renderListItem}
				/>
            </div>
		</div>
	);
};

export default Moustache;
