import React from "react";
import JsonBuilder from "../AutomationEditJsonNode";
import EnumBuilder from "../AutomationEditEnumNode";
import ArrayBuilder from "../AutomationEditArrayNode";
import ConstBuilder from "../AutomationConstNode";
import NumberBuilder from "../AutomationEditNumberNode";
import ObjectBuilder from "../AutomationEditObjectNode";
import StringBuilder from "../AutomationEditStringNode";
import BooleanBuilder from "../AutomationEditToggleNode";
import ExpressionBuilder from "../AutomationEditExpressionNode/AutomationEditExpressionNode";
import AutoCompleteBuilder from "../AutomationEditAutoCompleteNode";

import style from "./style.module.scss";
import ComponentType from "modules/Experimental/helpers/ComponentType";
import MoustacheContext from "modules/Experimental/context/MoustacheContext";

const Factory = ({
	viewSchema,
	moustache,
	getValue = () => {},
	setValue = () => {},
}) => {
	const Components = {
		[ComponentType.automationConstNode]: ConstBuilder,
		[ComponentType.automationEditJsonNode]: JsonBuilder,
		[ComponentType.automationEditEnumNode]: EnumBuilder,
		[ComponentType.automationEditArrayNode]: ArrayBuilder,
		[ComponentType.automationEditNumberNode]: NumberBuilder,
		[ComponentType.automationEditObjectNode]: ObjectBuilder,
		[ComponentType.automationEditStringNode]: StringBuilder,
		[ComponentType.automationEditToggleNode]: BooleanBuilder,
		[ComponentType.automationEditExpressionNode]: ExpressionBuilder,
		[ComponentType.automationEditAutoCompleteNode]: AutoCompleteBuilder,
	};

	let count = 0;
	const renderComponent = (x) => {
		const renderChildren = (y) => {
			if (y.items) {
				return y.items.map((z) => renderComponent(z));
			}
		};

		if (typeof Components[x.builder] !== "undefined") {
			count += 1;
			return React.createElement(
				Components[x.builder],
				{ key: count, viewSchema: x, getValue, setValue },
				renderChildren(x)
			);
		}
	};

	return (
		<MoustacheContext.Provider value={moustache}>
			<div className={style.container}>
				<header>{viewSchema.description}</header>
				<main>{renderComponent(viewSchema)}</main>
			</div>
		</MoustacheContext.Provider>
	);
};

export { Factory as default };
