import React, { useContext, useEffect, useReducer } from "react";
import style from "./style.module.scss";
import TextBox from "core/TextBox";
import ListItem from "core/ListItem";
import CallWebApi from "helpers/CallWebApi";
import AutoCompleteBase from "core/AutoCompleteBase";
import MoustacheContext from "modules/Experimental/context/MoustacheContext";

function reducer(state, action) {
	switch (action.type) {
		default:
			return state;
		case "SET_OPEN":
			return {
				...state,
				open: action.payload,
			};
		case "SET_TEXT_VALUE":
			return {
				...state,
				textValue: action.payload,
			};
		case "SET_LOADING":
			return {
				...state,
				loading: action.payload,
			};
		case "SET_DATA":
			return {
				...state,
				data: action.payload,
			};
	}
}

const AutoCompleteBuilder = ({ getValue, setValue, viewSchema }) => {
	const { key, description, autocomplete } = viewSchema;
	const [state, dispatch] = useReducer(reducer, {
		open: false,
		data: undefined,
		loading: false,
		textValue: getValue(key) ?? "",
	});

	const context = useContext(MoustacheContext);

	useEffect(() => {
		const FetchData = (index) => {
			dispatch({ type: "SET_LOADING", payload: true });
			CallWebApi({
				url: autocomplete[index].url,
				method: "GET",
			}).then((data) => {
				const [row, display] = autocomplete[index].display.split(".");
				const [, value] = autocomplete[index].value.split(".");
				dispatch({
					type: "SET_DATA",
					payload:
						data[row] &&
						data[row].map((item) => ({
							display: item[display],
							value: item[value],
						})),
				});
				dispatch({ type: "SET_LOADING", payload: false });
			});
		};
		if (autocomplete.length > 0) {
			FetchData(0);
		}
	}, [autocomplete]);

	useEffect(() => {
		if (state.textValue !== "") {
			setValue({
				[key]: state.textValue,
			});
		}
	}, [state.textValue]);

	const data = (searchText) => {
		let items = state.data;
		if (autocompleteMoustache(searchText)) {
			items = context.map((item) => ({
				display: item,
				value: item,
			}));
		}
		return (
			items &&
			items.filter(({ display }) =>
				display.toLowerCase().includes(searchText.toLowerCase())
			)
		);
	};

	const handleInputFocus = () => {
		dispatch({ type: "SET_OPEN", payload: true });
	};

	const handleInputBlur = () => {
		dispatch({ type: "SET_OPEN", payload: false });
	};

	const autocompleteMoustache = (str) => {
		return str.startsWith("{{") && !str.endsWith("}}");
	};

	const handleChange = (e) => {
		dispatch({
			type: "SET_TEXT_VALUE",
			payload: e.target.value,
		});
		if (!state.open) {
			dispatch({ type: "SET_OPEN", payload: true });
		}
	};

	const handleListItemClick = (item) => {
		dispatch({ type: "SET_TEXT_VALUE", payload: item.display });
		dispatch({ type: "SET_OPEN", payload: false });
	};

	const renderListItem = (item) => {
		return (
			<ListItem
				data={item}
				title={item.display}
				onClick={handleListItemClick}
			/>
		);
	};

	const { open, textValue } = state;
	return (
		<div className={style.autoCompleteBuilder}>
			<label>{description}</label>
			<AutoCompleteBase
				open={open}
				options={data(textValue) || []}
				className={style.textBox}
				renderInput={(params) => (
					<TextBox
						{...params}
						value={textValue}
						margin="dense"
						onBlur={handleInputBlur}
						onFocus={handleInputFocus}
						onChange={handleChange}
					/>
				)}
				renderListItem={renderListItem}
			/>
		</div>
	);
};

export default AutoCompleteBuilder;
