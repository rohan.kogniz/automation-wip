import React, { useEffect, useState } from "react";
import style from "./style.module.scss";
import TextArea from "core/TextArea";

const JsonBuilder = ({ getValue, setValue, viewSchema }) => {
	const { key, description } = viewSchema;

	let [textValue, setTextValue] = useState(JSON.stringify(getValue(key), null, 2));
	useEffect(() => {
		try {
			const obj = JSON.parse(textValue);
			setValue({
				[key]: obj,
			});
		} catch {
			setValue({
				[key]: undefined,
			});
		}
	}, [textValue, setValue, key]);

	const handleOnChange = (e) => {
		setTextValue(e.target.value);
	};

	return (
		<div className={style.jsonBuilder}>
			<label>{description}</label>
			<TextArea
				value={textValue}
				margin="dense"
				onChange={handleOnChange}
				spellCheck="false"
				autoAdjustHeight
			/>
		</div>
	);
};

export default JsonBuilder;
