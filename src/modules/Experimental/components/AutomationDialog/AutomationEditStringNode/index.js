import React, { useContext, useEffect, useState } from "react";
import style from "./style.module.scss";
import TextBox from "core/TextBox";
import ListItem from "core/ListItem";
import MoustacheContext from "modules/Experimental/context/MoustacheContext";
import AutoCompleteBase from "core/AutoCompleteBase";

const Component = ({getValue, setValue, viewSchema}) => {
	const { key, description } = viewSchema;

	let [textValue, setTextValue] = useState(getValue(key) ?? "")
	let [open, setOpen] = useState(false)
	const context= useContext(MoustacheContext)

	useEffect(() => {
		setValue({
			[key]: textValue
		})
	}, [textValue])

	const filter = (arr, searchValue) => {
		return arr.filter((item) =>
			item.toLowerCase().includes(searchValue.toLowerCase())
		);
	};

	const isStartOfMoustache = (str) => {
		return str.startsWith("{{") && !str.endsWith("}}");
	};

	const handleOnChange = (e) => {
		setOpen(isStartOfMoustache(e.target.value))
		setTextValue(e.target.value)
	}

	const handleListItemClick = (item) => {
		setOpen(false)
		setTextValue(item)
	};

	const renderListItem = (item) => {
		return (
			<ListItem
				data={item}
				title={item}
				onClick={handleListItemClick}
			/>
		);
	};

	return (
		<div className={style.stringBuilder}>
			<label>{description}</label>
			<AutoCompleteBase
				open={open}
				options={filter(context, textValue)}
				className={style.textBox}
				renderInput={(params) => (
					<TextBox
						{...params}
						value={textValue}
						margin="dense"
						onChange={handleOnChange}
						spellCheck='false'
					/>
				)}
				renderListItem={renderListItem}
			/>
		</div>
	);
}

export default Component;
