import React from "react";

const withExtraProps = (children, extraProps) => {
	return React.Children.map(
		children,
		(e) => e && React.cloneElement(e, extraProps)
	);
};

export { withExtraProps as default };
