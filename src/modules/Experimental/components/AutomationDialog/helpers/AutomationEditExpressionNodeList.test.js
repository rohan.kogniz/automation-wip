import deepFreeze from 'deep-freeze'
import List from './AutomationEditExpressionNodeList'

test('invariant', () => {
    const x = new List()
    // expect(x.toJS()).toEqual(List.fromJS([]).toJS())
    const y = List.fromJS([[]])
    expect(x.toJS()).toEqual(y.toJS())
})

test('constructor: empty array', () => {
    const input = [[]]
    expect((List.fromJS(input)).toJS()).toEqual(input)
    const input2 = []
    expect((List.fromJS(input2)).toJS()).toEqual([])

})

test('constructor: inner array', () => {
    const input = [[1, 2, 3]]
    expect((List.fromJS(input)).toJS()).toEqual(input)
})

test('constructor: inner+outer array', () => {
    const input = [[1, 2, 3], [4, 5, 6]]
    expect((List.fromJS(input)).toJS()).toEqual(input)
})

test('addOr', () => {
    const input0 = [[1, 2, 3], [4, 5, 6]]
    let l1 = List.fromJS(input0)
    deepFreeze(l1)
    let l2 = l1.addOr()
    expect(l2.toJS()).toEqual([[1, 2, 3], [4, 5, 6], [undefined]])

    // const input1 = []
    let l3 = new List()
    deepFreeze(l3)
    let l4 = l3.addOr()
    expect(l4.toJS()).toEqual([[], [undefined]])
    deepFreeze(l4)
    let l5 = l4.addOr()
    expect(l5.toJS()).toEqual([[], [undefined], [undefined]])
})

test('addAnd', () => {
    const input = [[1, 2, 3], [4, 5, 6]]
    let l1 = List.fromJS(input)
    deepFreeze(l1)
    let l2 = l1.addAnd(1)
    expect(l2.toJS()).toEqual([[1, 2, 3], [4, 5, 6, undefined]])

    const input1 = [[]]
    let l3 = List.fromJS(input1)
    deepFreeze(l3)
    let l4 = l3.addAnd(0)
    // expect(l4.toJS()).toEqual([[undefined]])
})

test('set -1', () => {
    const input = [[1, 2, 3], [4, 5, 6]]
    let l1 = List.fromJS(input)
    deepFreeze(l1)
    let l2 = l1.set(1, 1, 7)
    expect(l2.toJS()).toEqual([[1, 2, 3], [4, 7, 6]])
})

test('set -2', () => {
    const input = [[1, 2, 3], [4, 5, 6]]
    let l1 = List.fromJS(input)
    deepFreeze(l1)
    let l2 = l1.set(0, 0, 7)
    expect(l2.toJS()).toEqual([[7, 2, 3], [4, 5, 6]])
})

test('delete', () => {
    const input = [[1, 2, 3], [4, 5, 6]]
    let l1 = List.fromJS(input)
    deepFreeze(l1)
    let l2 = l1.delete(0, 0)
    expect(l2.toJS()).toEqual([[2, 3], [4, 5, 6]])
    deepFreeze(l2)
    let l3 = l2.delete(0, 0)
    expect(l3.toJS()).toEqual([[3], [4, 5, 6]])
    deepFreeze(l3)
    let l4 = l3.delete(0, 0)
    expect(l4.toJS()).toEqual([[4, 5, 6]])
    deepFreeze(l4)
    let l5 = l4.delete(0, 0)
    expect(l5.toJS()).toEqual([[5, 6]])
    deepFreeze(l5)
    let l6 = l5.delete(0, 0)
    expect(l6.toJS()).toEqual([[6]])
    deepFreeze(l6)
    let l7 = l6.delete(0, 0)
    expect(l7.toJS()).toEqual([[]])
    deepFreeze(l7)
    let l8 = l7.delete(0, 0)
    expect(l8.toJS()).toEqual([[]])


})

test('deepFreeze: example', () => {
    let a = []
    // deepFreeze(a)
    a.push(3)
})