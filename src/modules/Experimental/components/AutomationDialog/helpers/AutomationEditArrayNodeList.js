export default class List {
	constructor(data) {
		this.data = data;
	}

	static fromJS(data) {
		return new List(
			data.map((e) => ({
				id: Math.random(),
				payload: e,
			}))
		);
	}

	toJS() {
		return this.data.map((e) => e.payload);
	}

	add() {
		return new List([
			...this.data,
			{
				id: Math.random(),
				payload: undefined,
			},
		]);
	}

	get(i) {
		return this.data[i].payload;
	}

	set(i, value) {
		return new List([
			...this.data.slice(0, i),
			{
				...this.data[i],
				payload: value,
			},
			...this.data.slice(i + 1),
		]);
	}

	delete(i) {
		return new List([
			...this.data.slice(0, i),
			...this.data.slice(i + 1),
		]);
	}

	get keys() {
		return this.data.map(e => e.id)
	}
}
