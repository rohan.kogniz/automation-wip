export default class List {
	constructor(
		data = [
			{
				id: Math.random(),
				payload: [],
			},
		]
	) {
		this.data = data;
	}

	static fromJS(data) {
		return new List(
			data.map((e) => ({
				id: Math.random(),
				payload: e.map((f) => ({
					id: Math.random(),
					payload: f,
				})),
			}))
		);
	}

	toJS() {
		return this.data.map((e) => e.payload.map((f) => f && f.payload));
	}

	addOr() {
		return new List([
			...this.data,
			{
				id: Math.random(),
				payload: [
					{
						id: Math.random(),
						payload: undefined,
					},
				],
			},
		]);
	}

	addAnd(index) {
		return new List([
			...this.data.slice(0, index),
			{
				...this.data[index],
				payload: [
					...this.data[index].payload,
					{
						id: Math.random(),
						payload: undefined,
					},
				],
			},
			...this.data.slice(index + 1),
		]);
	}

	get(i, j) {
		return this.data[i].payload[j].payload;
	}

	set(i, j, value) {
		return new List([
			...this.data.slice(0, i),
			{
				...this.data[i],
				payload: [
					...this.data[i].payload.slice(0, j),
					{
						...this.data[i].payload[j],
						payload: value,
					},
					...this.data[i].payload.slice(j + 1),
				],
			},
			...this.data.slice(i + 1),
		]);
	}

	delete(i, j) {
		if (i >= this.data.length) {
			return this;
		}

		if (j >= this.data[i].payload.length) {
			return this;
		}

		if (
			this.data.length == 1 &&
			this.data[i].payload.length == 1 &&
			i == 0 &&
			j == 0
		) {
			return List.fromJS([[]]);
		}

		if (this.data[i].payload.length == 1 && j == 0) {
			return new List([
				...this.data.slice(0, i),
				...this.data.slice(i + 1),
			]);
		}

		return new List([
			...this.data.slice(0, i),
			{
				...this.data[i],
				payload: [
					...this.data[i].payload.slice(0, j),
					...this.data[i].payload.slice(j + 1),
				],
			},
			...this.data.slice(i + 1),
		]);
	}

	get orKeys() {
		return this.data.map((e) => e.id);
	}

	andKeys(i) {
		return this.data[i].payload.map((e) => e.id);
	}
}
