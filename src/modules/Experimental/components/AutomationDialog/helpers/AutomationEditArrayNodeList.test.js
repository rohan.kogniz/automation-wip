import deepFreeze from "deep-freeze";
import List from "./AutomationEditArrayNodeList";

test("constructor: empty array", () => {
	const input = [];
	expect(List.fromJS(input).toJS()).toEqual(input);
});

test("constructor: inner array", () => {
	const input = [1, 2, 3];
	expect(List.fromJS(input).toJS()).toEqual(input);
});

test("add", () => {
	const input = [1, 2, 3];
	let l1 = List.fromJS(input);
	deepFreeze(l1);
	let l2 = l1.add();
	expect(l2.toJS()).toEqual([1, 2, 3, undefined]);
});

test("set -1", () => {
	const input = [1, 2, 3];
	let l1 = List.fromJS(input);
	deepFreeze(l1);
	let l2 = l1.set(1, 7);
	expect(l2.toJS()).toEqual([1, 7, 3]);
});

test("delete", () => {
	const input = [1, 2, 3];
	let l1 = List.fromJS(input);
	deepFreeze(l1);
	let l2 = l1.delete(0);
	expect(l2.toJS()).toEqual([2, 3]);
	deepFreeze(l2);
	let l3 = l2.delete(0);
	expect(l3.toJS()).toEqual([3]);
	deepFreeze(l3);
	let l4 = l3.delete(0);
	expect(l4.toJS()).toEqual([]);
	deepFreeze(l4);
	let l5 = l4.delete(0);
	expect(l5.toJS()).toEqual([]);
});
