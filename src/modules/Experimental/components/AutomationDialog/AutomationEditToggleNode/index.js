import Toggle from "core/Toggle";
import React, { useState, useEffect } from "react";
import style from "./style.module.scss";

const Component = ({ getValue, setValue, viewSchema }) => {
	const {key, description} = viewSchema;
	let [boolValue, setBoolValue] = useState(getValue(key) ?? false)
	
	useEffect(() => {
		setValue({
			[key]: boolValue
		})
	}, [boolValue])

	const handleChange = (checked) => {
		setBoolValue(checked)
	}

	return (
		<div className={style.booleanBuilder}>
			<label>{description}</label>
			<Toggle
				checked={boolValue}
				className={style.toggle}
				onChange={handleChange}
			></Toggle>
		</div>
	);
};

export default Component;