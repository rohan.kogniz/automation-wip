import React, { useEffect } from "react";

const Component = ({setValue, viewSchema}) => {
	const { key } = viewSchema;

	useEffect(() => {
		setValue( {
			[key]: viewSchema.const
		} )
	}, [])

	return <div data-builder="constBuilder"></div>;
}

export default React.memo(Component, () => true);
