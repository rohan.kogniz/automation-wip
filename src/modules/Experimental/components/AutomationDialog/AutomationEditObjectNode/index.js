import React, { useCallback, useEffect, useState } from "react";
import withExtraProps from "../helpers/withExtraProps";

const Component = ({ getValue, setValue, viewSchema, children }) => {
	const { key } = viewSchema;
	const [objectValue, setObjectValue] = useState(getValue(key) ?? {});

	useEffect(() => {
		setValue({
			[key]: objectValue,
		});
	}, [objectValue]);

	const handleSetValue = useCallback((value) => {
		setObjectValue((preValue) => ({
			...preValue,
			...value,
		}));
	}, []);

	const handleGetValue = useCallback(
		(key) => {
			return objectValue && objectValue[key];
		},
		[objectValue]
	);

	return (
		<div data-builder="objectBuilder">
			{withExtraProps(children, {
				getValue: handleGetValue,
				setValue: handleSetValue,
			})}
		</div>
	);
};

export default React.memo(Component);