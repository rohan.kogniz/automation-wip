import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Get one incident instance",
	description: "Retrieve one incident instance by its ID",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "incident_management_get_one_instances",
		},
		schema: {
			const: "incident_management",
		},
		path: {
			const: "/incident/instance/{instance_id}",
		},
		method: {
			const: "get",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						path: {
							type: "object",
							additionalProperties: false,
							required: ["instance_id"],
							properties: {
								instance_id: {
									in: "path",
									name: "instance_id",
									description:
										"The identifier of the incident instance to retrieve",
									type: "string",
									autocomplete: [
										{
											resource:
												"incident_management/template",
											url: "/cust_incidents/templates",
											method: "get",
											display: "name",
											value: "id",
										},
										{
											resource:
												"incident_management/instance",
											url: "/cust_incidents/templates/{#[0].value}/instances",
											method: "get",
											display: [
												"{employee.firstName} {employee.lastName}",
												"{locationId}",
											],
											value: "id",
										},
									],
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					allOf: [
						{
							$ref: "#/components/IncidentManagementInstance",
						},
						{
							type: "object",
							properties: {
								employee: {
									$ref: "#/components/IncidentManagementInstancePerson",
								},
								fields: {
									type: "array",
									items: {
										$ref: "#/components/IncidentManagementInstanceField",
									},
								},
							},
						},
					],
				},
			},
		},
	},
	output: {
		allOf: [
			{
				$ref: "#/components/IncidentManagementInstance",
			},
			{
				type: "object",
				properties: {
					employee: {
						$ref: "#/components/IncidentManagementInstancePerson",
					},
					fieldsFlat: {
						type: "object",
					},
				},
			},
		],
	},
	components: {
		IncidentManagementInstance: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "Identifier of this instance",
					readOnly: true,
				},
				templateId: {
					type: "string",
					description: "The template used to create this instance",
				},
				parentInstanceId: {
					type: "string",
					description: "The parent instance holding this instance",
				},
				date: {
					type: "string",
					description: "Date of the incident",
				},
				status: {
					type: "string",
					description: "Status of the incident",
					enum: [0, 1],
				},
				locationId: {
					type: "string",
					description: "The location of this incident",
				},
				requiresVerification: {
					type: "boolean",
					description:
						"True if the incident instance requires a verification",
				},
				employeeId: {
					type: "string",
					description: "The person identifier in this instance",
				},
				userId: {
					type: "string",
					description: "The operator identifier in this instance",
				},
			},
		},
		IncidentManagementInstancePerson: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "The person identifier",
					readOnly: true,
				},
				firstName: {
					type: "string",
					description: "First name of the person",
					readOnly: true,
				},
				lastName: {
					type: "string",
					description: "Last name of the person",
					readOnly: true,
				},
				email: {
					type: "string",
					description: "Email of the person",
					readOnly: true,
				},
			},
		},
		IncidentManagementInstanceField: {
			type: "object",
			properties: {
				fieldId: {
					type: "number",
					description: "The field identifier",
					readOnly: true,
				},
				fieldName: {
					type: "string",
					description: "Unique name of the field",
				},
				fieldValue: {
					type: ["string", "object"],
					description: "Value of the field",
				},
				instanceId: {
					type: "number",
					description:
						"Identifier of the instance this field belongs to",
					readOnly: true,
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "incident_management_get_one_instances",
	schema: "incident_management",
	method: "get",
	path: "/incident/instance/{instance_id}",
	parameters: {
		path: {
			instance_id: "0",
		},
	},
};

const moustache = () => ([
	'{{ abc.id }}',
	'{{ abc.templateId }}',
	'{{ abc.parentInstanceId }}',
	'{{ abc.date }}',
	'{{ abc.status }}',
	'{{ abc.locationId }}',
	'{{ abc.requiresVerification }}',
	'{{ abc.employeeId }}',
	'{{ abc.userId }}',
	'{{ abc.employee.id }}',
	'{{ abc.employee.firstName }}',
	'{{ abc.employee.lastName }}',
	'{{ abc.employee.email }}',
	'{{ abc.fieldsFlat.<dynamicField> }}',
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	description: "Get one incident instance",
	required: true,
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management_get_one_instances",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/incident/instance/{instance_id}",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "get",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "path",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description:
								"The identifier of the incident instance to retrieve",
							key: "instance_id",
							required: true,
							autocomplete: [
								{
									resource: "incident_management/template",
									url: "/cust_incidents/templates",
									method: "get",
									display: "name",
									value: "id",
								},
								{
									resource: "incident_management/instance",
									url: "/cust_incidents/templates/{#[0].value}/instances",
									method: "get",
									display: [
										"{employee.firstName} {employee.lastName}",
										"{locationId}",
									],
									value: "id",
								},
							],
						},
					],
				},
			],
		},
	],
};
const io = [input, output, data, moustache];
export { io as default };
