import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Exit from loop",
	description: "Stop the current repeat loop iteration",
	nodeType: "flow",
	nodeSubtype: "ExitRepeatControlFlow",
	required: ["repeatControlFlowKey", "schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "repeat_exit_flow",
		},
		repeatControlFlowKey: {
			type: "string",
			description: "Key of the loop to exit",
		},
	},
};

const data = {
	key: "abc",
	schemaName: "repeat_exit_flow",
	repeatControlFlowKey: "abc",
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: 'Exit from loop',
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "repeat_exit_flow",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditStringNode,
			description: "Key of the loop to exit",
			key: "repeatControlFlowKey",
			required: true,
		}
	],
};
const io = [input, output, data, moustache];
export { io as default };
