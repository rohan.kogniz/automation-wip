import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Repeat Each",
	description:
		"Take a list of items and repeat the contained node for every items in the list",
	nodeType: "flow",
	nodeSubtype: "RepeatEachAsyncControlFlow",
	required: ["schemaName", "arrayToLoop"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "repeat_each_flow",
		},
		arrayToLoop: {
			type: "string",
			description: "Key to the list to repeat the contained nodes",
		},
	},
	output: {},
};

const data = {
	key: "abc",
	schemaName: "repeat_each_flow",
	arrayToLoop: "uvw",
};

const moustache = (input) => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Repeat Each",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "repeat_each_flow",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditStringNode,
			description: "Key to the list to repeat the contained nodes",
			key: "arrayToLoop",
			required: true,
		},
	],
};
const io = [input, output, data, moustache];
export { io as default };
