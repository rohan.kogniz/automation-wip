import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Repeat",
	description:
		"Repeat the contained nodes, running them the specified number of times",
	nodeType: "flow",
	nodeSubtype: "RepeatSyncControlFlow",
	required: ["schemaName", "nIteration"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "repeat_flow",
		},
		nIteration: {
			type: "number",
			description: "The number of time to repeat the contained nodes",
		},
	},
	"output": {}
};

const data = {
	key: "abc",
	schemaName: "repeat_flow",
	nIteration: 0,
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: 'Repeat',
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "repeat_flow",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditNumberNode,
			description: "The number of time to repeat the contained nodes",
			key: "nIteration",
			required: true,
			minimum: undefined,
			maximum: undefined
		}
	],
};
const io = [input, output, data, moustache];
export { io as default };