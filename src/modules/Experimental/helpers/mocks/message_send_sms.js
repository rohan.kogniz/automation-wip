import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Send SMS",
	description: "Send SMS to many enrolled person",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "message_send_sms",
		},
		schema: {
			const: "message",
		},
		path: {
			const: "/message/sms",
		},
		method: {
			const: "post",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						body: {
							type: "object",
							additionalProperties: false,
							required: ["personId", "message"],
							properties: {
								personId: {
									type: "string",
									description:
										"Unique identifier of an enrolled person (message target)",
								},
								message: {
									type: "string",
									description: "The text message to transmit",
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {},
	output: {},
	components: {
		Message: {
			type: "object",
			properties: {
				personId: {
					type: "string",
					description:
						"Unique identifier of an enrolled person (message target)",
				},
				message: {
					type: "string",
					description: "The text message to transmit",
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "message_send_sms",
	schema: "message",
	path: "/message/sms",
	method: "post",
	parameters: {
		body: {
			personId: "0",
			message: "Hi There!",
		},
	},
};

const moustache = () => ([]) 

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Send SMS",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "message_send_sms",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "message",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/message/sms",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "post",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "body",
					required: false,
					items: [
						{
							builder: ComponentType.automationEditStringNode,
							key: "personId",
							description:
								"Unique identifier of an enrolled person (message target)",
							required: true,
						},
						{
							builder: ComponentType.automationEditStringNode,
							key: "message",
							description: "The text message to transmit",
							required: true,
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
