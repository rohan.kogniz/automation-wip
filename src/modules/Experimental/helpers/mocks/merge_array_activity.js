import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Merge Array",
	description: "Merge several arrays together into one array of objects",
	nodeType: "activity",
	nodeSubtype: "MergeArrayActivity",
	required: ["mergeInstruction", "schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "merge_array_activity",
		},
		mergeInstruction: {
			type: "array",
			description: "Instructions for the merge.",
			minItems: 1,
			items: {
				type: "object",
				additionalProperties: false,
				required: ["key", "value"],
				properties: {
					key: {
						type: "string",
						description:
							"Name of the key to retrieve the merged value",
					},
					value: {
						type: "string",
						description: "Array of values to merge",
					},
				},
			},
		},
	},
	output: {
		type: "object",
		description:
			"Results of the merge operation with the defined keys and values",
	},
};

// example
const data = {
	key: "abc",
	schemaName: "merge_array_activity",
	mergeInstruction: [
		{
			key: "a",
			value: "b",
		},
		{
			key: "d",
			value: "f"
		}
	],
};

const moustache = () => ([
	'{{ abc.a }}',
	'{{ abc.d }}'
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Merge Array",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "merge_array_activity",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditArrayNode,
			key: "mergeInstruction",
			description: "Instructions for the merge.",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditStringNode,
					key: "key",
					description: "Name of the key to retrieve the merged value",
					required: true,
				},
				{
					builder: ComponentType.automationEditStringNode,
					key: "value",
					description: "Array of values to merge",
					required: true,
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
