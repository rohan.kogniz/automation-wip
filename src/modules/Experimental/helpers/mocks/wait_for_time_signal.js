import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Wait for time",
	description: "Wait for a specified time",
	nodeType: "signal",
	nodeSubtype: "TemporalSignal",
	required: ["absoluteTime", "schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "wait_for_time_signal",
		},
		absoluteTime: {
			type: "string",
			description: "Datetime to wait for in an ISO format",
			example: "2021-06-08T20:47:38Z",
		},
	},
};

const data = {
	key: "abc",
	schemaName: "wait_for_time_signal",
	absoluteTime: "2021-06-08T20:47:38Z",
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: 'Wait for time',
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "wait_for_time_signal",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditStringNode,
			description: "Datetime to wait for in an ISO format",
			key: "absoluteTime",
			required: true,
		}
	],
};
const io = [input, output, data, moustache];
export { io as default };

