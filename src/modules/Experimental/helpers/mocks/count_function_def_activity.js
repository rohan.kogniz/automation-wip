import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Count Activity",
	description: "Count the number of items in an array.",
	nodeType: "activity",
	nodeSubtype: "FunctionDefActivity",
	required: ["schemaName", "type", "config"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "count_function_def_activity",
		},
		type: {
			const: "CountFunctionDef",
		},
		config: {
			$ref: "#/components/CountFunctionDef",
		},
	},
	output: {
		type: "number",
		description: "The length of the input",
	},
	components: {
		CountFunctionDef: {
			name: "Count Function",
			description: "Count the number of items in an array.",
			required: ["input"],
			properties: {
				input: {
					oneOf: [
						{
							description:
								"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
							type: "string",
						},
						{
							description: "Find the length of the array",
							type: "array",
						},
					],
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "count_function_def_activity",
	type: "CountFunctionDef",
	config: {
		input: [1, 2, 3],
	},
};

const moustache = () => ([ '{{ abc }}' ])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Count Activity",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "count_function_def_activity",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "CountFunctionDef",
			key: "type",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "config",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditJsonNode,
					key: "input",
					required: true,
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
