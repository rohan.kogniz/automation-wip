import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "New incident instance is created",
	description: "A new incident instance was created",
	nodeType: "signal",
	nodeSubtype: "MatchSignal",
	required: ["match", "schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "incident_management_new_instance_signal",
		},
		match: {
			type: "object",
			required: ["name", "path", "method"],
			properties: {
				name: {
					const: "incident_management",
				},
				path: {
					const: "/cust_incidents/instances",
				},
				method: {
					const: "POST",
				},
			},
		},
		condition: {
			type: "object",
			oneOf: [
				{
					type: "object",
					required: ["type", "config"],
					properties: {
						type: {
							const: "FilterCondition",
						},
						config: {
							$ref: "#/components/FilterCondition",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					properties: {
						type: {
							const: "GroupCondition",
						},
						config: {
							$ref: "#/components/GroupCondition",
						},
					},
				},
			],
		},
	},
	output: {
		allOf: [
			{
				type: "object",
				properties: {
					id: {
						type: "number",
						description: "Identifier of this instance",
						readOnly: true,
					},
					templateId: {
						type: "string",
						description:
							"The template used to create this instance",
					},
					parentInstanceId: {
						type: "string",
						description:
							"The parent instance holding this instance",
					},
					date: {
						type: "string",
						description: "Date of the incident",
					},
					status: {
						type: "string",
						description: "Status of the incident",
					},
					locationId: {
						type: "string",
						description: "The location of this incident",
					},
					requiresVerification: {
						type: "boolean",
						description:
							"True if the incident instance requires a verification",
					},
					employeeId: {
						type: "string",
						description: "The person identifier in this instance",
					},
					userId: {
						type: "string",
						description: "The operator identifier in this instance",
					},
				},
			},
			{
				type: "object",
				properties: {
					employee: {
						type: "object",
						properties: {
							id: {
								type: "number",
								description: "The person internal identifier",
								readOnly: true,
							},
							idAlt: {
								type: "number",
								description: "The person company identifier",
								readOnly: true,
							},
							camgrpid: {
								type: "number",
								description: "The person location id",
								readOnly: true,
							},
							firstName: {
								type: "string",
								description: "First name of the person",
								readOnly: true,
							},
							lastName: {
								type: "string",
								description: "Last name of the person",
								readOnly: true,
							},
							email: {
								type: "string",
								description: "Email of the person",
								readOnly: true,
							},
						},
					},
					fieldsFlat: {
						type: "object",
					},
				},
			},
		],
	},
	components: {
		FilterCondition: {
			name: "Filter condition",
			description:
				"Assert the output of an activity using equality and comparison symbols",
			required: ["left", "condition", "right"],
			additionalProperties: false,
			properties: {
				left: {
					oneOf: [
						{
							type: ["string", "number"],
							description:
								"Left-hand side of the operator, can be a path to an output of a previous activity",
						},
						{
							$ref: "#/components/FunctionDef",
						},
					],
				},
				condition: {
					type: "string",
					description: "The equality or comparison symbols",
					enum: ["=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN"],
				},
				right: {
					oneOf: [
						{
							type: ["string", "number"],
							description:
								"Right-hand side of the operator, can be a path to an ouput of a previous activity",
						},
						{
							$ref: "#/components/FunctionDef",
						},
					],
				},
			},
		},
		GroupCondition: {
			name: "Group condition",
			description:
				"Use a logical operator (AND, OR) to compare multiple conditions with each other",
			additionalProperties: false,
			required: ["operator", "conditions"],
			properties: {
				operator: {
					type: "string",
					description: "The operator used to compare conditions",
					enum: ["AND", "OR"],
				},
				conditions: {
					type: "array",
					description: "The conditions that needs to be compared",
					minItems: 1,
					items: {
						anyOf: [
							{
								type: "object",
								required: ["type", "config"],
								properties: {
									type: {
										const: "FilterCondition",
									},
									config: {
										$ref: "#/components/FilterCondition",
									},
								},
							},
							{
								type: "object",
								required: ["type", "config"],
								properties: {
									type: {
										const: "GroupCondition",
									},
									config: {
										$ref: "#/components/GroupCondition",
									},
								},
							},
						],
					},
				},
			},
		},
		FunctionDef: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Functions",
			description:
				"Manipulate inputs to produce different kind of outputs",
			oneOf: [
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "CountFunctionDef",
							description: "Count Function",
						},
						config: {
							$ref: "#/components/CountFunctionDef",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "EmptyFunctionDef",
							description: "Is Empty Function",
						},
						config: {
							$ref: "#/components/EmptyFunctionDef",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "TimestampFunctionDef",
							description: "Timestamp Function",
						},
						config: {
							$ref: "#/components/TimestampFunctionDef",
						},
					},
				},
			],
			components: {
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
		CountFunctionDef: {
			name: "Count Function",
			description: "Count the number of items in an array.",
			required: ["input"],
			properties: {
				input: {
					oneOf: [
						{
							description:
								"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
							type: "string",
						},
						{
							description: "Find the length of the array",
							type: "array",
						},
					],
				},
			},
		},
		EmptyFunctionDef: {
			name: "Is Empty Function",
			description: "Return true if the string, array of object is empty",
			required: ["input"],
			properties: {
				input: {
					oneOf: [
						{
							description:
								"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
							type: "string",
						},
						{
							description: "Check if the array is empty",
							type: "array",
						},
						{
							description: "Check if the object is empty",
							type: "object",
						},
					],
				},
			},
		},
		TimestampFunctionDef: {
			name: "Timestamp Function",
			description:
				"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
		},
	},
};

const data = {
	key: "abc",
	schemaName: "incident_management_new_instance_signal",
	match: {
		name: "incident_management",
		path: "/cust_incidents/instances",
		method: "POST",
	},
	condition: {
		type: "FilterCondition",
		config: {
			left: {
				type: "CountFunctionDef",
				config: {
					input: "{{pqr}}",
				},
			},
			condition: "=",
			right: 0,
		},
	},
};

const moustache = () => ([
	'{{ abc.id }}',
	'{{ abc.templateId }}',
	'{{ abc.parentInstanceId }}',
	'{{ abc.date }}',
	'{{ abc.status }}',
	'{{ abc.locationId }}',
	'{{ abc.requiresVerification }}',
	'{{ abc.employeeId }}',
	'{{ abc.userId }}',
	'{{ abc.employee.id }}',
	'{{ abc.employee.idAlt }}',
	'{{ abc.employee.camgrpid }}',
	'{{ abc.employee.firstName }}',
	'{{ abc.employee.lastName }}',
	'{{ abc.employee.email }}',
	'{{ abc.fieldsFlat.<dynamicField> }}'
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "New incident instance is created",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management_new_instance_signal",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "match",
			required: true,
			items: [
				{
					builder: ComponentType.automationConstNode,
					const: "incident_management",
					key: "name",
					required: true,
				},
				{
					builder: ComponentType.automationConstNode,
					const: "/cust_incidents/instances",
					key: "path",
					required: true,
				},
				{
					builder: ComponentType.automationConstNode,
					const: "POST",
					key: "method",
					required: true,
				},
			],
		},
		{
			builder: ComponentType.automationEditExpressionNode,
			key: "condition",
			required: false,
			tokens: {
				comparison: ["=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN"],
				groupCondition: ["AND", "OR"],
				functionDef: [
					"CountFunctionDef",
					"EmptyFunctionDef",
					"TimestampFunctionDef",
				],
			},
		},
	],
};
const io = [input, output, data, moustache];
export { io as default };
