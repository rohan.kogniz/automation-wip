import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Wait",
	description: "Wait for a specified number of seconds",
	nodeType: "signal",
	nodeSubtype: "TemporalSignal",
	required: ["relativeTime", "schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "wait_second_signal",
		},
		relativeTime: {
			type: "number",
			description: "Number of seconds to wait",
			minimum: 60,
		},
	},
};

const data = {
	key: "abc",
	schemaName: "wait_second_signal",
	relativeTime: 70,
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: 'Wait',
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "wait_second_signal",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationEditNumberNode,
			description: "Number of seconds to wait",
			key: "relativeTime",
			required: true,
			minimum: 60,
			maximum: undefined
		}
	],
};
const io = [input, output, data, moustache];
export { io as default };

