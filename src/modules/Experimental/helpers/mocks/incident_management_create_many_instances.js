import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Create one or many instances",
	description: "Create incident instances",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "incident_management_create_many_instances",
		},
		schema: {
			const: "incident_management",
		},
		path: {
			const: "/incident/instance",
		},
		method: {
			const: "post",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						body: {
							type: "object",
							additionalProperties: false,
							required: ["templateId", "employeeId"],
							properties: {
								templateId: {
									type: "string",
									description:
										"The template used to create this instance",
									autocomplete: [
										{
											resource:
												"incident_management/template",
											url: "/cust_incidents/templates",
											method: "get",
											display: "name",
											value: "id",
										},
									],
								},
								parentInstanceId: {
									type: "string",
									description:
										"The parent instance holding this instance",
									autocomplete: [
										{
											resource:
												"incident_management/template",
											url: "/cust_incidents/templates",
											method: "get",
											display: "name",
											value: "id",
										},
										{
											resource:
												"incident_management/instance",
											url: "/cust_incidents/templates/{#[0].value}/instances",
											method: "get",
											display: [
												"{employee.firstName} {employee.lastName}",
												"{locationId}",
											],
											value: "id",
										},
									],
								},
								date: {
									type: "string",
									description: "Date of the incident",
								},
								status: {
									type: "string",
									description: "Status of the incident",
									enum: [0, 1],
								},
								locationId: {
									type: "string",
									description:
										"The location of this incident",
									autocomplete: [
										{
											resource: "camgrpid",
											url: "/cameragrp_list",
											method: "post",
											display: "camgrps.name",
											value: "camgrps.camgrpid",
										},
									],
								},
								requiresVerification: {
									type: "boolean",
									description:
										"True if the incident instance requires a verification",
								},
								employeeId: {
									type: "string",
									description:
										"The person identifier in this instance",
								},
								userId: {
									type: "string",
									description:
										"The operator identifier in this instance",
									autocomplete: [
										{
											resource: "operator",
											url: "/operators_list",
											method: "post",
											display:
												"{operators.op_name_first} {operators.op_name_last}",
											value: "operators.operatorid",
										},
									],
								},
								fields: {
									description: "The list of fields to create",
									type: "array",
									items: {
										type: "object",
										additionalProperties: false,
										required: ["fieldName", "fieldValue"],
										properties: {
											fieldName: {
												type: "string",
												description:
													"Unique name of the field",
											},
											fieldValue: {
												type: ["string", "object"],
												description:
													"Value of the field",
											},
										},
									},
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					description:
						"The list of identifiers of the new incident instances",
					type: "array",
					items: {
						description:
							"The results containing new instances identifiers",
						type: "object",
						properties: {
							incidentInstanceId: {
								description:
									"The identifier of the new incident instance",
								type: "number",
							},
						},
					},
				},
			},
		},
	},
	output: {
		description: "The list of identifiers of the new incident instances",
		type: "array",
		items: {
			type: "number",
		},
	},
	components: {
		IncidentManagementInstance: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "Identifier of this instance",
					readOnly: true,
				},
				templateId: {
					type: "string",
					description: "The template used to create this instance",
				},
				parentInstanceId: {
					type: "string",
					description: "The parent instance holding this instance",
				},
				date: {
					type: "string",
					description: "Date of the incident",
				},
				status: {
					type: "string",
					description: "Status of the incident",
					enum: [0, 1],
				},
				locationId: {
					type: "string",
					description: "The location of this incident",
				},
				requiresVerification: {
					type: "boolean",
					description:
						"True if the incident instance requires a verification",
				},
				employeeId: {
					type: "string",
					description: "The person identifier in this instance",
				},
				userId: {
					type: "string",
					description: "The operator identifier in this instance",
				},
			},
		},
		IncidentManagementInstancePerson: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "The person identifier",
					readOnly: true,
				},
				firstName: {
					type: "string",
					description: "First name of the person",
					readOnly: true,
				},
				lastName: {
					type: "string",
					description: "Last name of the person",
					readOnly: true,
				},
				email: {
					type: "string",
					description: "Email of the person",
					readOnly: true,
				},
			},
		},
		IncidentManagementInstanceField: {
			type: "object",
			properties: {
				fieldId: {
					type: "number",
					description: "The field identifier",
					readOnly: true,
				},
				fieldName: {
					type: "string",
					description: "Unique name of the field",
				},
				fieldValue: {
					type: ["string", "object"],
					description: "Value of the field",
				},
				instanceId: {
					type: "number",
					description:
						"Identifier of the instance this field belongs to",
					readOnly: true,
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "incident_management_create_many_instances",
	schema: "incident_management",
	path: "/incident/instance",
	method: "post",
	parameters: {
		body: {
			templateId: "0",
			parentInstanceId: "0",
			date: "2021-05-11",
			locationId: "0",
			requiresVerification: true,
			employeeId: "1",
			userId: "4",
			fields: [
				{
					fieldName: "pqr",
					fieldValue: "uvw",
				},
			],
		},
	},
};

const moustache = () => ([
	'{{ abc }}'
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Create one or many instances",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management_create_many_instances",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/incident/instance",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "post",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "body",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description:
								"The template used to create this instance",
							key: "templateId",
							required: true,
							autocomplete: [
								{
									resource: "incident_management/template",
									url: "/cust_incidents/templates",
									method: "get",
									display: "name",
									value: "id",
								},
							],
						},
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description:
								"The parent instance holding this instance",
							key: "parentInstanceId",
							required: false,
							autocomplete: [
								{
									resource: "incident_management/template",
									url: "/cust_incidents/templates",
									method: "get",
									display: "name",
									value: "id",
								},
								{
									resource: "incident_management/instance",
									url: "/cust_incidents/templates/{#[0].value}/instances",
									method: "get",
									display: [
										"{employee.firstName} {employee.lastName}",
										"{locationId}",
									],
									value: "id",
								},
							],
						},
						{
							builder: ComponentType.automationEditStringNode,
							description: "Date of the incident",
							key: "date",
							required: false,
						},
						{
							builder: ComponentType.automationEditEnumNode,
							description: "Status of the incident",
							key: "status",
							required: false,
							enum: [0, 1],
						},
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description: "The location of this incident",
							key: "locationId",
							required: false,
							autocomplete: [
								{
									resource: "camgrpid",
									url: "/cameragrp_list",
									method: "post",
									display: "camgrps.name",
									value: "camgrps.camgrpid",
								},
							],
						},
						{
							builder: ComponentType.automationEditToggleNode,
							description:
								"True if the incident instance requires a verification",
							key: "requiresVerification",
							required: false,
						},
						{
							builder: ComponentType.automationEditStringNode,
							description:
								"The person identifier in this instance",
							key: "employeeId",
							required: true,
						},
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description:
								"The operator identifier in this instance",
							key: "userId",
							required: false,
							autocomplete: [
								{
									resource: "operator",
									url: "/operators_list",
									method: "post",
									display:
										"{operators.op_name_first} {operators.op_name_last}",
									value: "operators.operatorid",
								},
							],
						},
						{
							builder: ComponentType.automationEditArrayNode,
							description: "The list of fields to create",
							key: "fields",
							required: false,
							items: [
								{
									builder:
										ComponentType.automationEditStringNode,
									description: "Unique name of the field",
									key: "fieldName",
									required: true,
								},
								{
									builder:
										ComponentType.automationEditStringOrObjectNode,
									description: "Value of the field",
									key: "fieldValue",
									required: true,
								},
							],
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
