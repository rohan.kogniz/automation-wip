import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Get many survey links",
	description: "Get the survey links for many users",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "survey_get_many_links",
		},
		schema: {
			const: "survey",
		},
		path: {
			const: "/survey/{surveyId}/links",
		},
		method: {
			const: "get",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						path: {
							type: "object",
							additionalProperties: false,
							required: ["surveyId"],
							properties: {
								surveyId: {
									in: "path",
									name: "surveyId",
									description:
										"Existing survey to get the link",
									type: "string",
									autocomplete: [
										{
											resource: "survey",
											url: "/survey_list",
											method: "get",
											display: "rows.name",
											value: "rows.id",
										},
									],
								},
							},
						},
						query: {
							type: "object",
							additionalProperties: false,
							required: ["personIds"],
							properties: {
								personIds: {
									in: "query",
									name: "personIds",
									description:
										"Person ids to get the survey links for",
									type: "string",
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					type: "array",
					items: {
						$ref: "#/components/SurveyLink",
					},
				},
			},
		},
	},
	output: {
		type: "array",
		items: {
			$ref: "#/components/SurveyLink",
		},
	},
	components: {
		SurveyLink: {
			type: "object",
			properties: {
				personid: {
					type: "number",
					description: "Identifier of an enrolled person",
					readOnly: true,
				},
				surveyid: {
					type: "number",
					description: "Identifier of a survey",
					readOnly: true,
				},
				link: {
					type: "string",
					description: "URL to the survey for that person",
					readOnly: true,
				},
			},
		},
		SurveyAnswer: {
			type: "object",
			properties: {
				personid: {
					type: "number",
					description: "Identifier of an enrolled person",
					readOnly: true,
				},
				surveyid: {
					type: "number",
					description: "Identifier of a survey",
					readOnly: true,
				},
				successful: {
					type: "number",
					description: "Indicate if the survey was successful",
					readOnly: true,
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "survey_get_many_links",
	schema: "survey",
	path: "/survey/{surveyId}/links",
	method: "get",
	parameters: {
		path: {
			surveyId: "0",
		},
		query: {
			personIds: "0,1",
		},
	},
};

const moustache = () => ([
	'{{ abc.personid }}',
	'{{ abc.surveyid }}',
	'{{ abc.link }}'
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Get many survey links",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "survey_get_many_links",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "survey",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/survey/{surveyId}/links",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "get",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "path",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							key: "surveyId",
							description: "Existing survey to get the link",
							required: true,
							autocomplete: [
								{
									resource: "survey",
									url: "/survey_list",
									method: "get",
									display: "rows.name",
									value: "rows.id",
								},
							],
						},
					],
				},
				{
					builder: ComponentType.automationEditObjectNode,
					key: "query",
					required: false,
					items: [
						{
							builder: ComponentType.automationEditStringNode,
							key: "personIds",
							description:
								"Person ids to get the survey links for",
							required: true,
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
