import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Edit one incident instance",
	description: "Edit the different fields of an existing incident instance",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "incident_management_edit_one_instance",
		},
		schema: {
			const: "incident_management",
		},
		path: {
			const: "/incident/instance/{instance_id}",
		},
		method: {
			const: "patch",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						path: {
							type: "object",
							additionalProperties: false,
							required: ["instance_id"],
							properties: {
								instance_id: {
									in: "path",
									name: "instance_id",
									description:
										"The identifier of the incident instance to edit",
									type: "string",
								},
							},
						},
						body: {
							type: "object",
							additionalProperties: false,
							required: [],
							properties: {
								parentInstanceId: {
									type: "string",
									description:
										"The parent instance holding this instance",
									autocomplete: [
										{
											resource:
												"incident_management/template",
											url: "/cust_incidents/templates",
											method: "get",
											display: "name",
											value: "id",
										},
										{
											resource:
												"incident_management/instance",
											url: "/cust_incidents/templates/{#[0].value}/instances",
											method: "get",
											display: [
												"{employee.firstName} {employee.lastName}",
												"{locationId}",
											],
											value: "id",
										},
									],
								},
								date: {
									type: "string",
									description: "Date of the incident",
								},
								status: {
									type: "string",
									description: "Status of the incident",
									enum: [0, 1],
								},
								locationId: {
									type: "string",
									description:
										"The location of this incident",
									autocomplete: [
										{
											resource: "camgrpid",
											url: "/cameragrp_list",
											method: "post",
											display: "camgrps.name",
											value: "camgrps.camgrpid",
										},
									],
								},
								requiresVerification: {
									type: "boolean",
									description:
										"True if the incident instance requires a verification",
								},
								fields: {
									type: "array",
									description: "The list of fields to update",
									items: {
										type: "object",
										additionalProperties: false,
										required: ["fieldName", "fieldValue"],
										properties: {
											fieldName: {
												type: "string",
												description:
													"Unique name of the field",
											},
											fieldValue: {
												type: ["string", "object"],
												description:
													"Value of the field",
											},
										},
									},
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {},
	output: {},
	components: {
		IncidentManagementInstance: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "Identifier of this instance",
					readOnly: true,
				},
				templateId: {
					type: "string",
					description: "The template used to create this instance",
				},
				parentInstanceId: {
					type: "string",
					description: "The parent instance holding this instance",
				},
				date: {
					type: "string",
					description: "Date of the incident",
				},
				status: {
					type: "string",
					description: "Status of the incident",
					enum: [0, 1],
				},
				locationId: {
					type: "string",
					description: "The location of this incident",
				},
				requiresVerification: {
					type: "boolean",
					description:
						"True if the incident instance requires a verification",
				},
				employeeId: {
					type: "string",
					description: "The person identifier in this instance",
				},
				userId: {
					type: "string",
					description: "The operator identifier in this instance",
				},
			},
		},
		IncidentManagementInstancePerson: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "The person identifier",
					readOnly: true,
				},
				firstName: {
					type: "string",
					description: "First name of the person",
					readOnly: true,
				},
				lastName: {
					type: "string",
					description: "Last name of the person",
					readOnly: true,
				},
				email: {
					type: "string",
					description: "Email of the person",
					readOnly: true,
				},
			},
		},
		IncidentManagementInstanceField: {
			type: "object",
			properties: {
				fieldId: {
					type: "number",
					description: "The field identifier",
					readOnly: true,
				},
				fieldName: {
					type: "string",
					description: "Unique name of the field",
				},
				fieldValue: {
					type: ["string", "object"],
					description: "Value of the field",
				},
				instanceId: {
					type: "number",
					description:
						"Identifier of the instance this field belongs to",
					readOnly: true,
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "incident_management_edit_one_instance",
	schema: "incident_management",
	path: "/incident/instance/{instance_id}",
	method: "patch",
	parameters: {
		path: {
			instance_id: "0",
		},
		body: {
			parentInstanceId: "0",
			date: "2021-05-11",
			locationId: "0",
			requiresVerification: true,
			fields: [
				{
					fieldName: "pqr",
					fieldValue: "uvw",
				},
			],
		},
	},
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Edit one incident instance",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management_edit_one_instance",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "incident_management",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/incident/instance/{instance_id}",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "patch",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "path",
					required: false,
					items: [
						{
							builder: ComponentType.automationEditStringNode,
							description:
								"The identifier of the incident instance to edit",
							key: "instance_id",
							required: true,
						},
					],
				},
				{
					builder: ComponentType.automationEditObjectNode,
					key: "body",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description:
								"The parent instance holding this instance",
							key: "parentInstanceId",
							required: false,
							autocomplete: [
								{
									resource: "incident_management/template",
									url: "/cust_incidents/templates",
									method: "get",
									display: "name",
									value: "id",
								},
								{
									resource: "incident_management/instance",
									url: "/cust_incidents/templates/{#[0].value}/instances",
									method: "get",
									display: [
										"{employee.firstName} {employee.lastName}",
										"{locationId}",
									],
									value: "id",
								},
							],
						},
						{
							builder: ComponentType.automationEditStringNode,
							description: "Date of the incident",
							key: "date",
							required: false,
						},
						{
							builder: ComponentType.automationEditEnumNode,
							description: "Status of the incident",
							key: "status",
							required: false,
							enum: [0, 1],
						},
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							description: "The location of this incident",
							key: "locationId",
							required: false,
							autocomplete: [
								{
									resource: "camgrpid",
									url: "/cameragrp_list",
									method: "post",
									display: "camgrps.name",
									value: "camgrps.camgrpid",
								},
							],
						},
						{
							builder: ComponentType.automationEditToggleNode,
							description:
								"True if the incident instance requires a verification",
							key: "requiresVerification",
							required: false,
						},
						{
							builder: ComponentType.automationEditArrayNode,
							description: "The list of fields to update",
							key: "fields",
							required: false,
							items: [
								{
									builder:
										ComponentType.automationEditStringNode,
									description: "Unique name of the field",
									key: "fieldName",
									required: true,
								},
								{
									builder:
										ComponentType.automationEditStringOrObjectNode,
									description: "Value of the field",
									key: "fieldValue",
									required: true,
								},
							],
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
