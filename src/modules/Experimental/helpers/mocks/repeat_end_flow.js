import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "End Repeat",
	description: "Nodes to execute when the repeat loop is completed",
	nodeType: "flow",
	nodeSubtype: "EndRepeatControlFlow",
	required: ["schemaName"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "repeat_end_flow",
		},
	},
};

const data = {
	key: "abc",
	schemaName: "repeat_end_flow",
};

const moustache = () => ([])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: 'End Repeat',
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "repeat_end_flow",
			key: "schemaName",
			required: true,
		}
	],
};
const io = [input, output, data, moustache];
export { io as default };
