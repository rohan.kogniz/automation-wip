import ComponentType from "../ComponentType";
import { trimMoustache } from "../moustacheOutput";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Filter Array",
	description:
		"Keep all the elements of an input array that satisfies one or many conditions.",
	nodeType: "activity",
	nodeSubtype: "FilterArrayActivity",
	required: ["input", "condition"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "filter_array_activity",
		},
		input: {
			type: "string",
			description: "Key to the output of a node to filter.",
		},
		condition: {
			type: "object",
			oneOf: [
				{
					type: "object",
					required: ["type", "config"],
					properties: {
						type: {
							const: "FilterCondition",
						},
						config: {
							$ref: "#/components/FilterCondition",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					properties: {
						type: {
							const: "GroupCondition",
						},
						config: {
							$ref: "#/components/GroupCondition",
						},
					},
				},
			],
		},
	},
	components: {
		FilterCondition: {
			name: "Filter condition",
			description:
				"Assert the output of an activity using equality and comparison symbols",
			required: ["left", "condition", "right"],
			additionalProperties: false,
			properties: {
				left: {
					oneOf: [
						{
							type: ["string", "number"],
							description:
								"Left-hand side of the operator, can be a path to an output of a previous activity",
						},
						{
							$ref: "#/components/FunctionDef",
						},
					],
				},
				condition: {
					type: "string",
					description: "The equality or comparison symbols",
					enum: ["=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN"],
				},
				right: {
					oneOf: [
						{
							type: ["string", "number"],
							description:
								"Right-hand side of the operator, can be a path to an ouput of a previous activity",
						},
						{
							$ref: "#/components/FunctionDef",
						},
					],
				},
			},
		},
		GroupCondition: {
			name: "Group condition",
			description:
				"Use a logical operator (AND, OR) to compare multiple conditions with each other",
			additionalProperties: false,
			required: ["operator", "conditions"],
			properties: {
				operator: {
					type: "string",
					description: "The operator used to compare conditions",
					enum: ["AND", "OR"],
				},
				conditions: {
					type: "array",
					description: "The conditions that needs to be compared",
					minItems: 1,
					items: {
						anyOf: [
							{
								type: "object",
								required: ["type", "config"],
								properties: {
									type: {
										const: "FilterCondition",
									},
									config: {
										$ref: "#/components/FilterCondition",
									},
								},
							},
							{
								type: "object",
								required: ["type", "config"],
								properties: {
									type: {
										const: "GroupCondition",
									},
									config: {
										$ref: "#/components/GroupCondition",
									},
								},
							},
						],
					},
				},
			},
		},
		FunctionDef: {
			$schema: "http://json-schema.org/draft-07/schema#",
			name: "Functions",
			description:
				"Manipulate inputs to produce different kind of outputs",
			oneOf: [
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "CountFunctionDef",
							description: "Count Function",
						},
						config: {
							$ref: "#/components/CountFunctionDef",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "EmptyFunctionDef",
							description: "Is Empty Function",
						},
						config: {
							$ref: "#/components/EmptyFunctionDef",
						},
					},
				},
				{
					type: "object",
					required: ["type", "config"],
					additionalProperties: false,
					properties: {
						type: {
							const: "TimestampFunctionDef",
							description: "Timestamp Function",
						},
						config: {
							$ref: "#/components/TimestampFunctionDef",
						},
					},
				},
			],
			components: {
				CountFunctionDef: {
					name: "Count Function",
					description: "Count the number of items in an array.",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
									type: "string",
								},
								{
									description: "Find the length of the array",
									type: "array",
								},
							],
						},
					},
				},
				EmptyFunctionDef: {
					name: "Is Empty Function",
					description:
						"Return true if the string, array of object is empty",
					required: ["input"],
					properties: {
						input: {
							oneOf: [
								{
									description:
										"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
									type: "string",
								},
								{
									description: "Check if the array is empty",
									type: "array",
								},
								{
									description: "Check if the object is empty",
									type: "object",
								},
							],
						},
					},
				},
				TimestampFunctionDef: {
					name: "Timestamp Function",
					description:
						"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
				},
			},
		},
		CountFunctionDef: {
			name: "Count Function",
			description: "Count the number of items in an array.",
			required: ["input"],
			properties: {
				input: {
					oneOf: [
						{
							description:
								"Find the length of a string. If a key is entered, find the corresponding output and find its length.",
							type: "string",
						},
						{
							description: "Find the length of the array",
							type: "array",
						},
					],
				},
			},
		},
		EmptyFunctionDef: {
			name: "Is Empty Function",
			description: "Return true if the string, array of object is empty",
			required: ["input"],
			properties: {
				input: {
					oneOf: [
						{
							description:
								"Check if the string is empty. If a key is entered, find the corresponding output and checks if it is empty.",
							type: "string",
						},
						{
							description: "Check if the array is empty",
							type: "array",
						},
						{
							description: "Check if the object is empty",
							type: "object",
						},
					],
				},
			},
		},
		TimestampFunctionDef: {
			name: "Timestamp Function",
			description:
				"Returns the current time in ISO format without milliseconds (e.g., 2021-06-15T21:33:28Z).",
		},
	},
	output: {
		type: "array",
		description: "A new array with all elements that pass the condition",
	},
};

const _1 = {
	key: "abc",
	schemaName: "filter_array_activity",
	input: "def",
	condition: {
		type: "FilterCondition",
		config: {
			left: "{{ pqr }}",
			condition: "=",
			right: 0,
		},
	},
};

const _2 = {
	key: "abc",
	schemaName: "filter_array_activity",
	input: "def",
	condition: {
		type: "FilterCondition",
		config: {
			left: {
				type: "CountFunctionDef",
				config: {
					input: "{{pqr}}",
				},
			},
			condition: "=",
			right: 0,
		},
	},
};

const data = {
	key: "abc",
	schemaName: "filter_array_activity",
	input: "def",
	condition: {
		type: "GroupCondition",
		config: {
			operator: "AND",
			conditions: [
				{
					type: "FilterCondition",
					config: {
						left: {
							type: "CountFunctionDef",
							config: {
								input: "{{pqr}}",
							},
						},
						condition: "=",
						right: 0,
					},
				},
				{
					type: "FilterCondition",
					config: {
						left: {
							type: "CountFunctionDef",
							config: {
								input: "{{rst}}",
							},
						},
						condition: "=",
						right: 0,
					},
				},
			],
		},
	},
};

const moustache = input => {
	const array = []
	for(const e of input) {
		array.push(`{{ abc.${trimMoustache(e)} }}`)
	}
	return array
}

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Filter Array",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "filter_array_activity",
			key: "schemaName",
			required: false,
		},
		{
			builder: ComponentType.automationEditStringNode,
			description: "Key to the output of a node to filter.",
			key: "input",
			required: true,
		},
		{
			builder: ComponentType.automationEditExpressionNode,
			key: "condition",
			required: true,
			tokens: {
				comparison: ["=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN"],
				groupCondition: ["AND", "OR"],
				functionDef: [
					"CountFunctionDef",
					"EmptyFunctionDef",
					"TimestampFunctionDef",
				],
			},
		},
	],
};
const io = [input, output, data, moustache];
export { io as default };
