import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Get many people",
	description: "Get many enrolled person",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "people_get_many",
		},
		schema: {
			const: "people",
		},
		path: {
			const: "/people",
		},
		method: {
			const: "post",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						query: {
							type: "object",
							additionalProperties: false,
							required: [],
							properties: {
								camgrpid: {
									in: "query",
									name: "camgrpid",
									description:
										"Location id of the people to retrieve",
									type: "string",
									autocomplete: [
										{
											resource: "camgrpid",
											url: "/cameragrp_list",
											method: "post",
											display: "camgrps.name",
											value: "camgrps.camgrpid",
										},
									],
								},
								enrolledId: {
									in: "query",
									name: "enrolledId",
									description:
										"Identifier of the enrolled person.",
									type: "string",
								},
								employeeId: {
									in: "query",
									name: "employeeId",
									description:
										"Company identifier of the enrolled person.",
									type: "string",
								},
							},
						},
						body: {
							type: "object",
							additionalProperties: false,
							required: [],
							properties: {
								attributes: {
									type: "array",
									items: {
										type: "object",
										additionalProperties: false,
										required: ["n", "v"],
										properties: {
											n: {
												type: "string",
												description:
													"Name of the property.",
											},
											v: {
												type: "string",
												description:
													"Value of the property.",
											},
										},
									},
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					description: "A list of enrolled people.",
					type: "array",
					items: {
						allOf: [
							{
								$ref: "#/components/Person",
							},
							{
								description: "The attributes component",
								type: "object",
								properties: {
									attributes: {
										$ref: "#/components/PersonAttribute",
									},
								},
							},
						],
					},
				},
			},
		},
	},
	output: {
		description: "A list of enrolled people.",
		type: "array",
		items: {
			allOf: [
				{
					$ref: "#/components/Person",
				},
				{
					description: "The attributes component",
					type: "object",
					properties: {
						attributes: {
							type: "object",
							description:
								"The employee attributes that are set for every person",
						},
					},
				},
			],
		},
	},
	components: {
		Person: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "Unique identifier of this person",
					readOnly: true,
				},
				firstName: {
					type: "string",
					description: "First name of the user",
				},
				lastName: {
					type: "string",
					description: "Last name of the user",
				},
				defaultLocationId: {
					type: "string",
					description: "Location ID of the user",
				},
			},
		},
		PersonAttribute: {
			type: "object",
			additionalProperties: false,
			properties: {
				n: {
					type: "string",
					description: "Name of the property.",
				},
				v: {
					type: "string",
					description: "Value of the property.",
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "people_get_many",
	schema: "people",
	path: "/people",
	method: "post",
	parameters: {
		query: {
			camgrpid: "0",
			enrolledId: "0",
			employeeId: "0",
		},
		body: {
			attributes: [
				{
					n: "key",
					v: "value",
				},
			],
		},
	},
};

const moustache = () => ([
	'{{ abc.id }}',
	'{{ abc.firstName }}',
	'{{ abc.lastName }}',
	'{{ abc.defaultLocationId }}',
	'{{ abc.attributes.key }}',
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Get many people",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "people_get_many",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "people",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/people",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "post",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "query",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							key: "camgrpid",
							description:
								"Location id of the people to retrieve",
							required: false,
							autocomplete: [
								{
									resource: "camgrpid",
									url: "/cameragrp_list",
									method: "post",
									display: "camgrps.name",
									value: "camgrps.camgrpid",
								},
							],
						},
						{
							builder: ComponentType.automationEditStringNode,
							key: "enrolledId",
							description: "Identifier of the enrolled person.",
							required: false,
						},
						{
							builder: ComponentType.automationEditStringNode,
							key: "employeeId",
							description:
								"Company identifier of the enrolled person.",
							required: false,
						},
					],
				},
				{
					builder: ComponentType.automationEditObjectNode,
					key: "body",
					required: false,
					items: [
						{
							builder: ComponentType.automationEditArrayNode,
							description: undefined,
							key: "attributes",
							required: false,
							items: [
								{
									builder:
										ComponentType.automationEditStringNode,
									description: "Name of the property.",
									key: "n",
									required: true,
								},
								{
									builder:
										ComponentType.automationEditStringNode,
									description: "Value of the property.",
									key: "v",
									required: true,
								},
							],
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
