const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Get one incident instance",
	description: "Retrieve one incident instance by its ID",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "incident_management_get_one_instances",
		},
		schema: {
			const: "incident_management",
		},
		path: {
			const: "/incident/instance/{instance_id}",
		},
		method: {
			const: "get",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						path: {
							type: "object",
							additionalProperties: false,
							required: ["instance_id"],
							properties: {
								instance_id: {
									in: "path",
									name: "instance_id",
									description:
										"The identifier of the incident instance to retrieve",
									type: "string",
								},
							},
						},
					},
					required: ["path"],
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					allOf: [
						{
							$ref: "#/components/IncidentManagementInstance",
						},
						{
							type: "object",
							properties: {
								employee: {
									$ref: "#/components/IncidentManagementInstancePerson",
								},
								fieldsFlat: {
									type: "object",
								},
							},
						},
					],
				},
			},
		},
	},
	output: {
		allOf: [
			{
				$ref: "#/components/IncidentManagementInstance",
			},
			{
				type: "object",
				properties: {
					employee: {
						$ref: "#/components/IncidentManagementInstancePerson",
					},
					fieldsFlat: {
						type: "object",
					},
				},
			},
		],
	},
	components: {
		IncidentManagementInstance: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "Identifier of this instance",
					readOnly: true,
				},
				templateId: {
					type: "string",
					description: "The template used to create this instance",
				},
				parentInstanceId: {
					type: "string",
					description: "The parent instance holding this instance",
				},
				date: {
					type: "string",
					description: "Date of the incident",
				},
				status: {
					type: "string",
					description: "Status of the incident",
				},
				locationId: {
					type: "string",
					description: "The location of this incident",
				},
				requiresVerification: {
					type: "boolean",
					description:
						"True if the incident instance requires a verification",
				},
				employeeId: {
					type: "string",
					description: "The person identifier in this instance",
				},
				userId: {
					type: "string",
					description: "The operator identifier in this instance",
				},
			},
		},
		IncidentManagementInstancePerson: {
			type: "object",
			properties: {
				id: {
					type: "number",
					description: "The person internal identifier",
					readOnly: true,
				},
				idAlt: {
					type: "number",
					description: "The person company identifier",
					readOnly: true,
				},
				camgrpid: {
					type: "number",
					description: "The person location id",
					readOnly: true,
				},
				firstName: {
					type: "string",
					description: "First name of the person",
					readOnly: true,
				},
				lastName: {
					type: "string",
					description: "Last name of the person",
					readOnly: true,
				},
				email: {
					type: "string",
					description: "Email of the person",
					readOnly: true,
				},
			},
		},
		IncidentManagementInstanceField: {
			type: "object",
			properties: {
				fieldId: {
					type: "number",
					description: "The field identifier",
					readOnly: true,
				},
				fieldName: {
					type: "string",
					description: "Unique name of the field",
				},
				fieldValue: {
					type: ["string", "object"],
					description: "Value of the field",
				},
				instanceId: {
					type: "number",
					description:
						"Identifier of the instance this field belongs to",
					readOnly: true,
				},
			},
		},
	},
};
