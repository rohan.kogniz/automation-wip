import ComponentType from "../ComponentType";

const input = {
	$schema: "http://json-schema.org/draft-07/schema#",
	name: "Get one answer to a survey",
	description: "Get the latest answer to a survey from a person",
	nodeType: "activity",
	nodeSubtype: "InternalAPIActivity",
	required: ["path", "method", "schema", "schemaName", "parameters"],
	properties: {
		key: {
			type: "string",
			description: "Identifier of this node.",
		},
		schemaName: {
			const: "survey_get_one_answer",
		},
		schema: {
			const: "survey",
		},
		path: {
			const: "/survey/{surveyId}/answer",
		},
		method: {
			const: "get",
		},
		parameters: {
			allOf: [
				{
					additionalProperties: false,
					properties: {
						path: {
							type: "object",
							additionalProperties: false,
							required: ["surveyId"],
							properties: {
								surveyId: {
									in: "path",
									name: "surveyId",
									description:
										"Existing survey to get the link",
									type: "string",
									autocomplete: [
										{
											resource: "survey",
											url: "/survey_list",
											method: "get",
											display: "rows.name",
											value: "rows.id",
										},
									],
								},
							},
						},
						query: {
							type: "object",
							additionalProperties: false,
							required: ["personId"],
							properties: {
								personId: {
									in: "query",
									name: "personId",
									description:
										"The person id to get the answer for",
									type: "string",
								},
							},
						},
					},
				},
			],
		},
	},
	responses: {
		200: {
			"application/json": {
				schema: {
					type: "array",
					items: {
						$ref: "#/components/SurveyAnswer",
					},
				},
			},
		},
	},
	output: {
		type: "array",
		items: {
			$ref: "#/components/SurveyAnswer",
		},
	},
	components: {
		SurveyLink: {
			type: "object",
			properties: {
				personid: {
					type: "number",
					description: "Identifier of an enrolled person",
					readOnly: true,
				},
				surveyid: {
					type: "number",
					description: "Identifier of a survey",
					readOnly: true,
				},
				link: {
					type: "string",
					description: "URL to the survey for that person",
					readOnly: true,
				},
			},
		},
		SurveyAnswer: {
			type: "object",
			properties: {
				personid: {
					type: "number",
					description: "Identifier of an enrolled person",
					readOnly: true,
				},
				surveyid: {
					type: "number",
					description: "Identifier of a survey",
					readOnly: true,
				},
				successful: {
					type: "number",
					description: "Indicate if the survey was successful",
					readOnly: true,
				},
			},
		},
	},
};

const data = {
	key: "abc",
	schemaName: "survey_get_one_answer",
	schema: "survey",
	path: "/survey/{surveyId}/answer",
	method: "get",
	parameters: {
		path: {
			surveyId: "0",
		},
		query: {
			personId: "0",
		},
	},
};

const moustache = () => ([
	'{{ abc.personid }}',
	'{{ abc.surveyid }}',
	'{{ abc.successful }}'
])

const output = {
	builder: ComponentType.automationEditObjectNode,
	key: 'rootNode',
	required: true,
	description: "Get one answer to a survey",
	items: [
		{
			builder: ComponentType.automationEditStringNode,
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "survey_get_one_answer",
			key: "schemaName",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "survey",
			key: "schema",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			key: "path",
			const: "/survey/{surveyId}/answer",
			required: true,
		},
		{
			builder: ComponentType.automationConstNode,
			const: "get",
			key: "method",
			required: true,
		},
		{
			builder: ComponentType.automationEditObjectNode,
			key: "parameters",
			required: true,
			items: [
				{
					builder: ComponentType.automationEditObjectNode,
					key: "path",
					required: false,
					items: [
						{
							builder:
								ComponentType.automationEditAutoCompleteNode,
							key: "surveyId",
							description: "Existing survey to get the link",
							required: true,
							autocomplete: [
								{
									resource: "survey",
									url: "/survey_list",
									method: "get",
									display: "rows.name",
									value: "rows.id",
								},
							],
						},
					],
				},
				{
					builder: ComponentType.automationEditObjectNode,
					key: "query",
					required: false,
					items: [
						{
							builder: ComponentType.automationEditStringNode,
							key: "personId",
							description: "The person id to get the answer for",
							required: true,
						},
					],
				},
			],
		},
	],
};

const io = [input, output, data, moustache];
export { io as default };
