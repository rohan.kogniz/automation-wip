import _ from "lodash";
import ComponentType from "./ComponentType";

const isObject = (v) => {
	return (
		(v.type && v.type === "object") ||
		(!v.hasOwnProperty("type") && !v.hasOwnProperty("const"))
	);
};

const traverseExpression = (k, v, req, components) => {
	const functionDef = Object.keys(components.FunctionDef.components);
	const comparison = components.FilterCondition.properties.condition.enum;
	const groupCondition = components.GroupCondition.properties.operator.enum;

	let builder = {
		builder: ComponentType.automationEditExpressionNode,
		key: k,
		required: req.includes(k),
		tokens: {
			functionDef,
			comparison,
			groupCondition,
		},
	};
	return builder;
};

const traverse = (k, v, req = []) => {
	if (v.hasOwnProperty("autocomplete")) {
		return {
			builder: ComponentType.automationEditAutoCompleteNode,
			description: v.description,
			key: k,
			required: req.includes(k),
			autocomplete: v.autocomplete,
		};
	}
	if (v.hasOwnProperty("enum")) {
		return {
			builder: ComponentType.automationEditEnumNode,
			description: v.description,
			enum: v.enum,
			key: k,
			required: req.includes(k),
		};
	}
	if (v.type && v.type === "string") {
		return {
			builder: ComponentType.automationEditStringNode,
			description: v.description,
			key: k,
			required: req.includes(k),
		};
	}
	if (v.type && _.isEqual(v.type, ["string", "object"])) {
		return {
			builder: ComponentType.automationEditStringOrObjectNode,
			description: v.description,
			key: k,
			required: req.includes(k),
		};
	}
	if (v.type && v.type === "boolean") {
		return {
			builder: ComponentType.automationEditToggleNode,
			description: v.description,
			key: k,
			required: req.includes(k),
		};
	}
	if (v.type && v.type === "number") {
		return {
			builder: ComponentType.automationEditNumberNode,
			description: v.description,
			key: k,
			required: req.includes(k),
			minimum: v.minimum,
			maximum: v.maximum
		};
	}
	if (v.hasOwnProperty("const")) {
		return {
			builder: ComponentType.automationConstNode,
			const: v.const,
			key: k,
			required: req.includes(k),
		};
	}
	if (v.type && v.type === "array") {
		let builder = {
			builder: ComponentType.automationEditArrayNode,
			description: v.description,
			key: k,
			required: req.includes(k),
			items: [],
		};
		if (isObject(v.items)) {
			let req0 = v.items.required;
			Object.keys(v.items.properties).forEach((k0) => {
				const v0 = v.items.properties[k0];
				builder.items.push(traverse(k0, v0, req0));
			});
		} else {
			throw new Error("schema_error: expected array of objects")
		}
		return builder;
	}
	if (isObject(v)) {
		if (v.hasOwnProperty('oneOf')) {
			return {
				builder: ComponentType.automationEditJsonNode,
				description: v.description,
				key: k,
				required: req.includes(k),
			};
		}
		if (v.hasOwnProperty("allOf")) {
			let builder = {
				builder: ComponentType.automationEditObjectNode,
				key: k,
				required: req.includes(k),
				items: [],
			};
			v.allOf.forEach((e) => {
				if (isObject(e)) {
					let req0 = e.required;
					Object.keys(e.properties).forEach((k0) => {
						const v0 = e.properties[k0];
						builder.items.push(traverse(k0, v0, req0));
					});
				} else {
					throw new Error("schema_error: expected allOf objects")
				}
			});
			return builder;
		}
		if (v.hasOwnProperty("properties")) {
			let builder = {
				builder: ComponentType.automationEditObjectNode,
				key: k,
				required: req.includes(k),
				items: [],
			};
			let req0 = v.required;
			Object.keys(v.properties).forEach((k0) => {
				const v0 = v.properties[k0];
				builder.items.push(traverse(k0, v0, req0));
			});
			return builder;
		} else {
			return {
				builder: ComponentType.automationEditJsonNode,
				description: v.description,
				key: k,
				required: req.includes(k),
			};
		}
	}
};


// interpretor: given a JSON schema for an automation node, convert it to a
// corresponding viewBuilder schema. if no schmea could be generated,
// this function throws an error. it is expected the schema is already
// dereferenced
const interpretor = (json) => {
	if (typeof json === "undefined") {
		return;
	}

	if (_.isEmpty(json)) {
		return;
	}
	
	let builder = {
		builder:ComponentType.automationEditObjectNode,
		key: 'rootNode',
		required: true,
		description: json.name,
		items: [],
	};
	let req0 = json.required;
	Object.keys(json.properties).forEach((k0) => {
		const v0 = json.properties[k0];
		if (k0 === "condition") {
			builder.items.push(
				traverseExpression(k0, v0, req0, json.components)
			);
		} else {
			builder.items.push(traverse(k0, v0, req0));
		}
	});

	return builder;
};

export { interpretor as default };
