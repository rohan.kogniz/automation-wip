const ComponentType = {
	automationConstNode: "constBuilder",
	automationEditEnumNode: "enumBuilder",
	automationEditJsonNode: "jsonBuilder",
	automationEditArrayNode: "arrayBuilder",
	automationEditNumberNode: "numberBuilder",
	automationEditObjectNode: "objectBuilder",
	automationEditStringNode: "stringBuilder",
	automationEditToggleNode: "booleanBuilder",
	automationEditExpressionNode: "expressionBuilder",
	automationEditAutoCompleteNode: "autocompleteBuilder",
	automationEditStringOrObjectNode: "stringObjectBuilder"
};

export default ComponentType;
