import _ from 'lodash';

import nodeOutput, {trimMoustache} from "./moustacheOutput";
import data1 from "./mocks/incident_management_get_one_instances";
import data2 from "./mocks/incident_management_get_many_instances";
import data3 from "./mocks/incident_management_create_many_instances";
import data4 from "./mocks/incident_management_edit_one_instance";
import data5 from "./mocks/survey_get_many_links";
import data6 from "./mocks/survey_get_one_answer";
import data7 from "./mocks/message_send_sms";
import data8 from "./mocks/count_function_def_activity";
import data9 from "./mocks/merge_array_activity";
import data10 from "./mocks/filter_array_activity";
import data11 from "./mocks/trigger";
import data12 from "./mocks/repeat_flow";
import data13 from "./mocks/repeat_each_flow";
import data14 from "./mocks/repeat_end_flow";
import data15 from "./mocks/repeat_exit_flow";
import data16 from "./mocks/wait_second_signal";
import data17 from "./mocks/wait_for_time_signal";
import data18 from "./mocks/match_signal";
import data19 from "./mocks/incident_management_new_instance_signal";
import data20 from "./mocks/survey_new_answer_signal";
import data21 from "./mocks/people_get_many";

import $RefParser from "@apidevtools/json-schema-ref-parser";

let nodeInput = [];

beforeEach(() => {
	nodeInput = ["{{ pqr }}", "{{ rst }}"];
});

test("default", () => {
	expect(nodeOutput({}, {}, nodeInput)).toEqual(nodeInput);
});

test("key: undefined", () => {
	const [ inputSchema, , nodeData, ] = data1;
	let cloneNodeData = _.cloneDeep(nodeData)
	cloneNodeData.key = undefined
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(cloneNodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput]);
		}
	});
});

test("incident_management_get_one_instances", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data1;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("incident_management_get_many_instances", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data2;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("incident_management_create_many_instances", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data3;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("incident_management_edit_one_instance", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data4;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("survey_get_many_links", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data5;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("survey_get_one_answer", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data6;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("message_send_sms", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data7;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("count_function_def_activity", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data8;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("merge_array_activity", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data9;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("filter_array_activity", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data10;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("trigger", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data11;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("repeat_flow", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data12;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("repeat_each_flow", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data13;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("repeat_end_flow", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data14;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("repeat_exit_flow", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data15;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("wait_second_signal", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data16;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("wait_for_time_signal", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data17;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("match_signal", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data18;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("incident_management_new_instance_signal", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data19;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("survey_new_answer_signal", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data20;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test("people_get_many", () => {
	const [ inputSchema, _, nodeData, nodeMoustacheFn ] = data21;
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			const output = nodeOutput(nodeData, outputSchema, nodeInput);
			expect(output).toEqual([...nodeInput, ...nodeMoustacheFn(nodeInput)]);
		}
	});
});

test('trim moustache', () => {
	expect(trimMoustache(undefined)).toEqual(undefined)
	expect(trimMoustache('')).toEqual('')
	expect(trimMoustache('{{abc}}')).toEqual('abc')
	expect(trimMoustache('{{ abc }}')).toEqual('abc')
	expect(trimMoustache(' abc')).toEqual('abc')
})