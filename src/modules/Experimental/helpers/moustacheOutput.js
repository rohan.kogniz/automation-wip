const processObject = (key, node, nodeData) => {
	let array = [];
	if (node.hasOwnProperty("properties")) {
		Object.keys(node.properties).forEach((e) => {
			if (node.properties[e].type === "object") {
				const newArray = [
					...array,
					...processObject(`${key}.${e}`, node.properties[e], nodeData),
				];
				array = newArray;
			} else {
				array.push(`{{ ${key}.${e} }}`);
			}
		});
	} else {
		if (node.hasOwnProperty("description")) {
			if (
				node.description ===
				"The employee attributes that are set for every person"
			) {
				if (
					nodeData.hasOwnProperty("parameters") &&
					nodeData.parameters.hasOwnProperty("body") &&
					nodeData.parameters.body.hasOwnProperty("attributes")
				) {
					let array = [];
					for (const e of nodeData.parameters.body.attributes) {
						array.push(`{{ ${key}.${e.n} }}`);
					}
					return array;
				}
			}
		}
		array.push(`{{ ${key}.<dynamicField> }}`);
	}
	return array;
};

const processAllOf = (key, node, nodeData) => {
	let array = [];
	node.forEach((e) => {
		const newArray = [...array, ...processObject(key, e, nodeData)];
		array = newArray;
	});
	return array;
};

const processOutput = (key, nodeSchema, nodeInput, nodeData) => {
	if (nodeSchema.hasOwnProperty("allOf")) {
		return [...processAllOf(key, nodeSchema.allOf, nodeData)];
	} else if (nodeSchema.hasOwnProperty("type")) {
		if (nodeSchema.hasOwnProperty("description")) {
			if (
				nodeSchema.description ===
				"A new array with all elements that pass the condition"
			) {
				let array = [];
				for (const e of nodeInput) {
					array.push(`{{ ${key}.${ trimMoustache(e) } }}`);
				}
				return array;
			} else if (
				nodeSchema.description ===
				"Results of the merge operation with the defined keys and values"
			) {
				if (nodeData.hasOwnProperty("mergeInstruction")) {
					let array = [];
					for (const e of nodeData.mergeInstruction) {
						array.push(`{{ ${key}.${e.key} }}`);
					}
					return array;
				}
			} 
		}
		if (nodeSchema.type === "object") {
			return [...processObject(key, nodeSchema, nodeData)];
		} else {
			return [`{{ ${key} }}`];
		}
	}
	return [];
};

export const trimMoustache = (str) => {
	if (typeof str !== 'string') {
		return undefined
	}
	let l = 0
	let r = str.length - 1
	while(str[l] == '{' || str[l] == ' ') l++
	while(str[r] == '}' || str[r] == ' ') r--
	return str.substring(l, r+1)
}

const Output = (nodeData, nodeSchema, nodeInput) => {
	if (typeof nodeSchema === "undefined") {
		return nodeInput;
	}

	if (!nodeSchema.hasOwnProperty("output")) {
		return nodeInput;
	}

	const nodeKey = nodeData.key;
	if (typeof nodeKey === 'undefined' || nodeKey.trim() === '') {
		return nodeInput
	}

	let array = [...nodeInput];

	if (nodeSchema.hasOwnProperty("output")) {
		if (nodeSchema.output.hasOwnProperty("items")) {
			const newArray = [
				...array,
				...processOutput(
					nodeKey,
					nodeSchema.output.items,
					nodeInput,
					nodeData
				),
			];
			array = newArray;
		} else {
			const newArray = [
				...array,
				...processOutput(
					nodeKey,
					nodeSchema.output,
					nodeInput,
					nodeData
				),
			];
			array = newArray;
		}
	}
	return array;
};

export default Output;
