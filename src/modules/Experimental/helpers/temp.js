import _ from "lodash";

const interpretor = (json) => {
	if (typeof json === "undefined") {
		return;
	}

	if (_.isEmpty(json)) {
		return [];
	}

	let arr = [];
	let req = json.required;
	Object.keys(json.properties).forEach((k0) => {
		const p0 = json.properties[k0];
		if (k0 === "config") {
			let configBuilder = {
				builder: "configBuilder",
				pathToObjectKey: "config",
				required: req.includes("config"),
				items: []
			};
			const req1 = p0.required || [];
			Object.keys(p0.properties).forEach((k1) => {
				const p1 = p0.properties[k1]
				if (p1.hasOwnProperty("oneOf")) {
					configBuilder.items.push({
						builder: "stringBuilder",
						pathToObjectKey: k1,
						required: req1.includes(k1)
					})
				}
			});
			arr.push(configBuilder);
		} else if (p0.type && p0.type === "string") {
			arr.push({
				builder: "stringBuilder",
				description: p0.description,
				pathToObjectKey: k0,
				required: req.includes(k0),
			});
		} else if (p0.hasOwnProperty("const")) {
			arr.push({
				builder: "constBuilder",
				const: p0.const,
				pathToObjectKey: k0,
				required: req.includes(k0),
			});
		} else if (p0.hasOwnProperty("allOf")) {
			if (k0 === "parameters") {
				let parameterBuilder = {
					builder: "parameterBuilder",
					pathToObjectKey: "parameters",
					required: req.includes("parameters"),
					items: [],
				};
				if (_.isArray(p0.allOf)) {
					parameterBuilder.allOf = true;
					p0.allOf.forEach((r0) => {
						Object.keys(r0.properties).forEach((k3) => {
							const p3 = r0.properties[k3];
							let parameterType = undefined;
							let builder;
							if (k3 === "path") {
								parameterType = "path";
								builder = {
									builder: "pathBuilder",
									pathToObjectKey: "path",
									required: req.includes("path"),
									items: [],
								};
							} else if (k3 === "query") {
								parameterType = "query";
								builder = {
									builder: "queryBuilder",
									pathToObjectKey: "query",
									required: req.includes("query"),
									items: [],
								};
							} else if (k3 === "body") {
								parameterType = "body";
								builder = {
									builder: "bodyBuilder",
									pathToObjectKey: "body",
									required: req.includes("body"),
									items: [],
								};
							}
							if (parameterType) {
								const s0 = r0.properties[parameterType];
								const req1 = s0.required || [];
								Object.keys(s0.properties).forEach((k1) => {
									const p1 = s0.properties[k1];
									if (k1 === "message") {
										builder.items.push({
											builder: "messageBuilder",
											description: p1.description,
											pathToObjectKey: k1,
											required: req1.includes(k1),
										});
									} else if (
										p1.hasOwnProperty("autocomplete")
									) {
										builder.items.push({
											builder: "autocompleteBuilder",
											description: p1.description,
											pathToObjectKey: k1,
											required: req1.includes(k1),
											autocomplete: p1.autocomplete,
										});
									} else if (p1.hasOwnProperty("enum")) {
										builder.items.push({
											builder: "enumBuilder",
											description: p1.description,
											enum: p1.enum,
											pathToObjectKey: k1,
											required: req1.includes(k1),
										});
									} else if (
										p1.type &&
										p1.type === "string"
									) {
										builder.items.push({
											builder: "stringBuilder",
											description: p1.description,
											pathToObjectKey: k1,
											required: req1.includes(k1),
										});
									} else if (
										p1.type &&
										p1.type === "boolean"
									) {
										builder.items.push({
											builder: "booleanBuilder",
											description: p1.description,
											pathToObjectKey: k1,
											required: req1.includes(k1),
										});
									} else if (p1.type && p1.type === "array") {
										let arrayBuilder = {
											builder: "arrayBuilder",
											description: p1.description,
											pathToObjectKey: k1,
											items: [],
										};
										const s1 = p1.items;
										const req2 = s1.required || [];
										Object.keys(s1.properties).forEach(
											(k2) => {
												const p2 = s1.properties[k2];
												arrayBuilder.items.push({
													builder: "stringBuilder",
													description: p2.description,
													pathToObjectKey: k2,
													required: req2.includes(k2),
												});
											}
										);
										builder.items.push(arrayBuilder);
									}
								});
							}
							parameterBuilder.items.push(builder);
						});
					});
				}

				arr.push(parameterBuilder);
			}
		}
	});

	return arr;
};

export { interpretor as default };
