import schemaInterpretor from "./schemaInterpretor";
import data1 from "./mocks/incident_management_get_one_instances";
import data2 from "./mocks/incident_management_get_many_instances";
import data3 from "./mocks/incident_management_create_many_instances";
import data4 from "./mocks/incident_management_edit_one_instance";
import data5 from "./mocks/survey_get_many_links";
import data6 from "./mocks/survey_get_one_answer";
import data7 from "./mocks/message_send_sms";
import data8 from "./mocks/count_function_def_activity";
import data9 from './mocks/merge_array_activity';
import data10 from './mocks/filter_array_activity';
import data11 from './mocks/trigger';
import data12 from './mocks/repeat_flow';
import data13 from './mocks/repeat_each_flow';
import data14 from './mocks/repeat_end_flow';
import data15 from './mocks/repeat_exit_flow';
import data16 from './mocks/wait_second_signal';
import data17 from './mocks/wait_for_time_signal';
import data18 from './mocks/match_signal';
import data19 from './mocks/incident_management_new_instance_signal';
import data20 from './mocks/survey_new_answer_signal';
import data21 from './mocks/people_get_many';

import $RefParser from "@apidevtools/json-schema-ref-parser";

test("undefined entry", () => {
	expect(schemaInterpretor(undefined)).toEqual(undefined);
});

test("empty entry", () => {
	expect(schemaInterpretor({})).toEqual(undefined);
});

test("invalid schema", () => {
	expect(() => {schemaInterpretor('some_random_string')}).toThrow(Error);
});

test("invalid schema - no properties", () => {
	expect(() => {schemaInterpretor({'some_random_key': 0})}).toThrow(Error);
});

test("incident_management_get_one_instances", () => {
	const inputSchema = data1[0];
	const expectedOutput = data1[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("incident_management_get_many_instances", () => {
	const inputSchema = data2[0];
	const expectedOutput = data2[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("incident_management_create_many_instances", () => {
	const inputSchema = data3[0];
	const expectedOutput = data3[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("incident_management_edit_one_instance", () => {
	const inputSchema = data4[0];
	const expectedOutput = data4[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("survey_get_many_links", () => {
	const inputSchema = data5[0];
	const expectedOutput = data5[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("survey_get_one_answer", () => {
	const inputSchema = data6[0];
	const expectedOutput = data6[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("message_send_sms", () => {
	const inputSchema = data7[0];
	const expectedOutput = data7[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("count_function_def_activity", () => {
	const inputSchema = data8[0];
	const expectedOutput = data8[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("merge_array_activity", () => {
	const inputSchema = data9[0];
	const expectedOutput = data9[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("filter_array_activity", () => {
	const inputSchema = data10[0];
	const expectedOutput = data10[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("trigger", () => {
	const inputSchema = data11[0];
	const expectedOutput = data11[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("repeat_flow", () => {
	const inputSchema = data12[0];
	const expectedOutput = data12[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("repeat_each_flow", () => {
	const inputSchema = data13[0];
	const expectedOutput = data13[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("repeat_end_flow", () => {
	const inputSchema = data14[0];
	const expectedOutput = data14[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("repeat_exit_flow", () => {
	const inputSchema = data15[0];
	const expectedOutput = data15[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("wait_second_signal", () => {
	const inputSchema = data16[0];
	const expectedOutput = data16[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("wait_for_time_signal", () => {
	const inputSchema = data17[0];
	const expectedOutput = data17[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("match_signal", () => {
	const inputSchema = data18[0];
	const expectedOutput = data18[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("incident_management_new_instance_signal", () => {
	const inputSchema = data19[0];
	const expectedOutput = data19[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("survey_new_answer_signal", () => {
	const inputSchema = data20[0];
	const expectedOutput = data20[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});

test("people_get_many", () => {
	const inputSchema = data21[0];
	const expectedOutput = data21[1];
	$RefParser.dereference(inputSchema, (err, outputSchema) => {
		if (err) {
		} else {
			expect(schemaInterpretor(outputSchema)).toEqual(expectedOutput);
		}
	});
});
