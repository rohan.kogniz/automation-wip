import React from 'react'
import styles from './style.scss'
import useCodeMirror from '../CodeMirror/useCodeMirror'
import moustachePlugin from '../CodeMirror/moustachePlugin'

const Dialog = () => {
    const [ref, dispatch] = useCodeMirror({
        doc: '',
        extensions: [moustachePlugin]
    })

    return (
        <div>
            <div ref={ref} className='editor'></div>
        </div>
    )
}

export { Dialog as default }