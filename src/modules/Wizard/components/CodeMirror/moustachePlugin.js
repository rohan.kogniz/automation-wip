import { WidgetType, Decoration } from "@codemirror/view";
import { RegExpCursor } from "@codemirror/search";
import { ViewPlugin } from "@codemirror/view";

class MoustacheWidget extends WidgetType {
	constructor(text) {
		super();
        const tokens = text.split('.')
        this.id = tokens[0]
		this.text = tokens[1];
	}

	eq(other) {
		other.text = this.text;
	}

	toDOM() {
		let wrap = document.createElement("span");
		wrap.className = "cm-combo-box";
		let c1 = document.createElement("span")
		let c2 = document.createElement("span")
		c1.appendChild(document.createTextNode("Get Employee "));
		c2.appendChild(document.createTextNode(this.text));
		wrap.appendChild(c1)
		wrap.appendChild(c2)
        wrap.onclick = () => {
            console.log('on click')
        }
		return wrap;
	}

	ignoreEvent() {
		return false;
	}
}

const comboBoxWidget = (text) => Decoration.replace({
	widget: new MoustacheWidget(text),
});

function comboBoxes(view) {
	let iter = new RegExpCursor(view.state.doc, "\\{\\{\-?[\\sa-z0-9\.]+\\}\\}");
    let widgets = []
	while(!iter.done) {
        let {from, to} = iter.value
        if (to > -1) {
            const text = view.state.doc.sliceString(from + 2, to - 2)
			console.log(text)
            widgets.push(comboBoxWidget(text).range(from, to))
        }
	    iter = iter.next()
	}
    return Decoration.set(widgets)
}

const plugin = ViewPlugin.fromClass(
	class {
		constructor(view) {
			this.decorations = comboBoxes(view)
		}

		update(view) {
			if (view.docChanged) {
				this.decorations = comboBoxes(view);
			}
		}

		destroy() {}
	}, {
        decorations: v => v.decorations
    }
);

export { plugin as default };
