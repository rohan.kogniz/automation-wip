import { useEffect, useRef } from "react";
import { EditorState } from "@codemirror/state";
import { EditorView } from "@codemirror/view";

const useCodeMirror = (config) => {
	const ref = useRef();
	const view = useRef();

	// get all selected ranges
	const selectedRange = (state) => {
		let ret;
		state.selection.ranges.forEach((range) => {
			if (range.from > -1) {
				ret = range;
			}
		});
		return ret;
	};

	useEffect(() => {
		const eventHandler = EditorView.domEventHandlers({
			focus() {
				config.onFocus && config.onFocus();
			},
			blur() {
				config.onBlur && config.onBlur();
			},
			click() {
				config.onClick && config.onClick();
			},
		});

		const updateListenerExtension = EditorView.updateListener.of(
			(update) => {
				if (update.docChanged) {
					config.onChange && config.onChange(update.state.doc);
				}
				if (config.onSelectionChange) {
					const range = selectedRange(update.state);
					if (range) {
						config.onSelectionChange(range);
					}
				}
			}
		);

		// intrinsic extensions are available to all instances of CodeMirror
		const intrinsicExtensions = [updateListenerExtension, eventHandler];
		// extrinsic extensions allow customizations for different instances
		// of CodeMirrors
		const extrinsicExtensions = config.extensions
			? [...config.extensions]
			: [];
		const extensions = [...intrinsicExtensions, ...extrinsicExtensions];

		// EditorView is a display component that knows how to show the editor
		// state to the user, and translates basic editing actions into state
		// updates
		view.current = new EditorView({
			state: EditorState.create({ ...config, extensions }),
			parent: ref.current,
		});
		return () => {
			view.current?.destroy();
		};
	}, []);

	const dispatch = (action) => {
		switch (action.type) {
			default:
				break;
			case "INSERT_TEXT":
				// from and to are document offsets in UTF-16 codeunits
				const { from, to } = selectedRange(view.current.state); 

				// transactions describe changes to the document, selections, or
				// other state fields
				let transaction = view.current.state.update({
					changes: {
						from,
						to,
						insert: action.text,
					},
				});
				// at this point the view still shows the old state
				view.current.dispatch(transaction);
		}
	};

	return [ref, dispatch];
};

export { useCodeMirror as default };
