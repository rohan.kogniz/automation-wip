import { isMoustacheExpression } from './regex'

test('moustacheExpression: non-string', () => {
    expect(isMoustacheExpression(0)).toEqual(false)
    expect(isMoustacheExpression(false)).toEqual(false)
    expect(isMoustacheExpression({})).toEqual(false)
})

test('moustacheExpression: correct expression', () => {
    expect(isMoustacheExpression('{{ abc }}')).toEqual(true)
    expect(isMoustacheExpression('{{abc}}')).toEqual(true)
})

test('moustacheExpression: mangled expression', () => {
    expect(isMoustacheExpression('{{ a{bc }}')).toEqual(false)
})