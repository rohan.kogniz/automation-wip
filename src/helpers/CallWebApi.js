import $ from 'jquery';
import Base64 from './Base64';

const auth_call = ({url, method, data, token}) => {
    if (token == null) {
        throw Error('token cannot be null');
    }

    const base64 = new Base64();
    const utfArr = base64.strToUTF8Arr(token)
    const enc64 = base64.base64EncArr(utfArr); 
    const authHeader = 'Basic ' + enc64;

    return new Promise((resolve, reject) => {
        $.ajax({
            url,
            method,
            beforeSend: (xhr) => {
                xhr.setRequestHeader('Authorization', authHeader); 
            },
            data
        })
        .done((data) => {
            resolve(data);
        })
        .fail((e) => reject(e));
    });
}

const no_auth_call = ({url, method, data}) => {

    return new Promise((resolve, reject) => {
        $.ajax({
            url,
            method,
            data
        })
        .done((data) => {
            resolve(data);
        })
        .fail((e) => reject(e));
    });

}


const CallWebApi = ({url, method, data, token, auth = false}) => {
    if (auth) {
        return auth_call({url, method, data, token});
    } else {
        return no_auth_call({url, method, data, token});
    }
}

export default CallWebApi;