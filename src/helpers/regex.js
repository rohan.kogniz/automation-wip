
export function isMoustacheExpression(e) {
    if (typeof e !== 'string') { 
        return false
    }
    const regex = new RegExp('{{[^}{}]+}}')
    return regex.test(e)
}