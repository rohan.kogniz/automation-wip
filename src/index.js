import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";

import "./style.scss";
import App11 from "apps/App11";
import App8 from "apps/App8";
import App20 from "apps/App20";
import App21 from "apps/App21";
import App22 from "apps/App22";
import App23 from "apps/App23";
import App24 from 'apps/App24';

ReactDOM.render(
	<React.StrictMode>
		<App24 />
	</React.StrictMode>,
	document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
