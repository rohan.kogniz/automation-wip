import React from 'react'
import TextArea from 'core/TextArea'

const App = () => {
    return <TextArea spellCheck='false' autoAdjustHeight />
}

export { App as default }