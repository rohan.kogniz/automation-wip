export const NodeType = {
	activity: "activity",
	trigger: "trigger",
	flow: "flow",
	signal: "signal"
}

export const Nodes = {
	"a.goii": {
		name: "Get One Incident Instance",
		desc: "Retreive one incident instance by its ID",
		type: NodeType.activity
	},
	"a.gmi": {
		name: "Get many instances",
		desc: "Get available instances",
		type: NodeType.activity
	},
	"a.coomi": {
		name: "Create one or many instances",
		desc: "Create incident instances",
		type: NodeType.activity
	},
	"a.eoii": {
		name: "Edit one incident instance",
		desc: "Edit the different fields of an existing incident instance",
		type: NodeType.activity
	},
	"a.gmsl": {
		name: "Get many survey links",
		desc: "Get the survey links for many users",
		type: NodeType.activity
	},
	"a.goatas": {
		name: "Get one answer to a survey",
		desc: "Get the latest answer to a survey from a person",
		type: NodeType.activity
	},
	"a.gmp": {
		name: "Get many people",
		desc: "Get many enrolled person",
		type: NodeType.activity
	},
	"a.ssms": {
		name: "Send SMS",
		desc: "Send SMS to many enrolled person",
		type: NodeType.activity
	},
	"a.stmaavcm": {
		name: "Send text message as a voice call message",
		desc: "Send text message as a voice call message to many enrolled person",
		type: NodeType.activity
	},
	"a.ca": {
		name: "Count activity",
		desc: "Count the number of items in an array",
		type: NodeType.activity
	},
	"a.ma": {
		name: "Merge array",
		desc: "Merge several arrays together into one array of objects",
		type: NodeType.activity
	},
	"a.fa": {
		name: "Filter array",
		desc: "Keep all the elements of an input array that satisfies one or many conditions",
		type: NodeType.activity
	},
	"t.trigger": {
		name: "Trigger",
		desc: "Evaluate different conditions and connect them with logical operators",
		type: NodeType.trigger
	},
	"f.repeat": {
		name: "Repeat",
		desc: "Repeat the contained nodes, running them the specified number of times",
		type: NodeType.flow
	},
	"f.repeateach": {
		name: "Repeat Each",
		desc: "Take a list of items and repeat the contained node for every items in the list",
		type: NodeType.flow
	},
	"f.endrepeat": {
		name: "End Repeat",
		desc: "Nodes to execute when the repeat loop is completed",
		type: NodeType.flow
	},
	"f.break": {
		name: "Exit from loop",
		desc: "Stop the current repeat loop iteration",
		type: NodeType.flow
	},
	"s.wait": {
		name: "Wait",
		desc: "wait for a specified number of seconds",
		type: NodeType.signal
	},
	"s.waitfortime": {
		name: "Wait for time",
		desc: "Wait for a specified time",
		type: NodeType.signal
	},
	"s.waitforsignal": {
		name: "Wait for signal",
		desc: "Wait for an external signal",
		type: NodeType.signal
	},
	"s.niiic": {
		name: "New incident instance is created",
		desc: "A new incident instance was created",
		type: NodeType.signal
	},
	"s.natas": {
		name: "New answer to a survey",
		desc: "A new answer to a survey was received",
		type: NodeType.signal

	},
	"s.nleic": {
		name: "New logbook entry is created",
		desc: "A rule was triggered based due to a new logbook entry",
		type: NodeType.signal
	},
};

export const Diag1 = {
	id: "t.trigger",
	children: [{ id: "a.eoii", children: [] }],
};

export const Diag2 = {
	id: "s.niiic",
	children: [{ id: "a.gmp", children: [
		{id: 't.trigger', children: []},
		{id: 't.trigger', children: []}
	] }],
};

export const Diag3 = {
	id: "s.niiic",
	children: [{ id: "a.gmp", children: [
		{id: 't.trigger', children: []},
		{id: 't.trigger', children: [
			{ id: 'a.coomi', children: []}
		]}
	] }],
};

export const Diag4 = {
	id: "f.repeat",
	children: [
		{ id: "s.wait", children: []},
		{ id: "f.endrepeat", exit: true, children: []}
	]
}

export const Diag4_1 = {
	id: "f.repeat",
	children: [
		{ id: "f.endrepeat", exit: true, children: []}
	]
}

export const Diag5 = {
	id: "f.repeat",
	children: [
		{ id: "s.wait", children: [
			{id: "a.goatas", children: [
				{id: "t.trigger", children: [
					{id: "a.eoii", children: []},
					{id: "f.break", break: true, children: []}
				]},
				{id: "t.trigger", children: []}
			]}
		]},
		{ id: "f.endrepeat", exit: true, children: []}
	]
}

export const Diag6 = {
	id: "f.repeat",
	children: [
		{ id: "s.wait", children: [
			{id: "a.goatas", children: [
				{id: "t.trigger", children: [
					{id: "a.eoii", children: []}
				]},
				{id: "t.trigger", children: [
					{id: "f.break", break: true, children: []}
				]}
			]}
		]},
		{ id: "f.endrepeat", exit: true, children: []}
	]
}

export const Diag7 = {
	id: "f.repeat",
	children: [
		{ id: "s.wait", children: [
			{id: "a.goatas", children: [
				{id: "t.trigger", children: [
					{id: "a.eoii", children: []}
				]},
				{id: "t.trigger", children: [
					{id: "f.break", break: true, children: []}
				]}
			]}
		]},
		{ id: "f.endrepeat", exit: true, children: [
			{id: "a.goatas", children: [
				{id: "t.trigger", children: []},
				{id: "t.trigger", children: []}
			]}
		]}
	]
}

export const Diag8 = {
	id: "f.repeat",
	children: [
		{ id: "s.natas", children: [
			{ id: "t.trigger", children: [
				{ id: "a.eoii", children: [
					{id: "f.break", break: true, children: []}
				]}
			]},
			{ id: "t.trigger", children: [
				{ id: "a.eoii", children: []}
			]}
		]},
		{ id: "f.endrepeat", exit: true, children: [
			{id: "a.goatas", children: [
				{id: "t.trigger", children: []},
				{id: "t.trigger", children: []}
			]}
		]}
	]
}

export const Diag9 = {
	id: "f.repeateach", children: [
		Diag7,
		Diag8,
		{ id: "f.endrepeat", exit: true, children: [] }
	]
}

export const Diag10 = {
	id: "t.trigger", children: [
		{ id: "a.coomi", children: [
			{ id: "a.gmsl", children: [
				{ id: "a.ma", children: [
					{ id: "a.ssms", children: [
						Diag9
					] }
				] }
			]}
		]}
	]
}

export const Diag11 = {
	id: "s.niiic", children: [
		{ id: "a.gmp", children: [
			Diag1,
			Diag10
		] }
	]
}