import AutomationLayout from "modules/Automation/components/AutomationLayout";
import React from "react";
import {
	getNodeChildren,
	getLoopExitNode,
	isLoopBreakNode,
} from "modules/Automation/helpers/layoutHelpers";
import classname from "classnames";
import styles from "./style.module.scss";
import nodeClassType from "modules/Automation/helpers/nodeClassType";
import {
	Diag1,
	Diag10,
	Diag11,
	Diag2,
	Diag3,
	Diag4,
	Diag4_1,
	Diag5,
	Diag6,
	Diag7,
	Diag8,
	Diag9,
	Nodes,
} from "./mocks/automation";
import automationTreeFromSchemaAlt from "modules/Automation/helpers/testUtilities/automationTreeFromSchemaAlt";

const NodeView = ({ node }) => {
	const data = node.getAttribute("data-node-id");
	const type = Nodes[data].type;
	return (
		<div className={classname(styles.node, nodeClassType(type, styles))}>
			<p>{data}</p>
		</div>
	);
};

const Foo = ({ data }) => {
	const automationTree = automationTreeFromSchemaAlt({ nodes: data });
	const renderNode = (n) => {
		return <NodeView node={n} />;
	};
	return (
		<AutomationLayout
			renderNode={renderNode}
			automationTree={automationTree}
			getNodeChildren={getNodeChildren}
			getLoopExitNode={getLoopExitNode}
			isLoopBreakNode={isLoopBreakNode}
		/>
	);
};
const App8 = () => {
	return (
		<div>
			<Foo data={Diag1} />
			<br />
			<Foo data={Diag2} />
			<br />
			<Foo data={Diag3} />
			<br />
			<Foo data={Diag4} />
			<br />
			<Foo data={Diag4_1} />
			<br />
			<Foo data={Diag5} />
			<br />
			<Foo data={Diag6} />
			<br />
			<Foo data={Diag7} />
			<br />
			<Foo data={Diag8} />
			<br />
			<Foo data={Diag9} />
			<br />
			<Foo data={Diag10} />
			<br />
			<Foo data={Diag11} />
		</div>
	);
};

export { App8 as default };
