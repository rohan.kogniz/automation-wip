import React from "react";
import AutomationEditor from "modules/Automation/components/AutomationEditor";
import Nodes from "apps/App11/mocks/evacuation";
import Schema from "apps/App11/mocks/schema_autocomplete";
import automationTree from "modules/Automation/selector/automationTree";

const App11 = () => {
	const tree = automationTree(Nodes);
	return (
		<div>
			<AutomationEditor
				automationTree={tree}
				automationSchema={Schema}
				automationNodes={Nodes}
			/>
		</div>
	);
};

export { App11 as default };
