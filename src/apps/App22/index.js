import React from 'react'
import style from './style.module.scss'

const Component = () => {
    return (
        <div>
            <div><label className={style.h1}>Heading 1</label></div>
            <div><label className={style.h2}>Heading 2</label></div>
            <div><label className={style.h3}>Heading 3</label></div>
            <div><label className={style.h4}>Heading 4</label></div>
            <div><label className={style.h5}>Heading 5</label></div>
            <div><label className={style.h6}>Heading 6</label></div>
            <div><label className={style.subTitle1}>SubTitle 1</label></div>
            <div><label className={style.subTitle2}>SubTitle 2</label></div>
            <div><label className={style.body1}>Body 1</label></div>
            <div><label className={style.body2}>Body 2</label></div>
            <div><label className={style.button}>Button</label></div>
            <div><label className={style.caption}>Caption</label></div>
            <div><label className={style.overline}>Overline</label></div>
        </div>
    )
}

export default Component;