const output = {
	builder: "objectBuilder",
	key: "rootNode",
	required: true,
	description: 'some random stuff',
	items: [
		{
			builder: "stringBuilder",
			description: "Identifier of this node.",
			key: "key",
			required: false,
		},
		{
			builder: "stringBuilder",
			description: "Duplicate Identifier of this node.",
			key: "key1",
			required: false,
		},
		{
			builder: "constBuilder",
			const: "repeat_flow",
			key: "schemaName",
			required: true,
		},
		{
			builder: "numberBuilder",
			description: "The number of time to repeat the contained nodes",
			key: "nIteration",
			required: true,
			minimum: 0,
			maximum: 10
		},
		{
			builder: "objectBuilder",
			key: "match",
			required: true,
			items: [
				{
					builder: 'constBuilder',
					const: 'survey',
					key: 'name',
					required: true
				},
				{
					builder: 'constBuilder',
					const: '/pub_submit_survey',
					key: 'path',
					required: true
				},
				{
					builder: 'constBuilder',
					const: 'POST',
					key: 'method',
					required: true
				}
			]
		},
		{
			builder: "expressionBuilder",
			key: "condition",
			description: 'Build expression',
			required: false,
			tokens: {
				comparison: ["=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN"],
				groupCondition:  ["AND", "OR"],
				functionDef: ['CountFunctionDef', 'EmptyFunctionDef', 'TimestampFunctionDef']
			}
		},
		{
			builder: "objectBuilder",
			key: "parameters",
			required: true,
			items: [
				{
					builder: "objectBuilder",
					key: "path",
					required: false,
					items: [
						{
							builder: "autocompleteBuilder",
							key: "countryId",
							description: "Existing survey to get the link",
							required: true,
							autocomplete: [
								{
									resource: "survey",
									url: "https://countriesnow.space/api/v0.1/countries",
									method: "get",
									display: "data.country",
									value: "data.country",
								},
							],
						},
					],
				},
				{
					builder: "objectBuilder",
					key: "query",
					required: false,
					items: [
						{
							builder: "stringBuilder",
							key: "personId",
							description: "The person id to get the answer for",
							required: true,
						},
					],
				},
			],
		},
		{
			builder: "objectBuilder",
			key: "query",
			required: false,
			items: [
				{
					builder: "autocompleteBuilder",
					key: "camgrpid",
					description:
						"Location id of the people to retrieve",
					required: false,
					autocomplete: [
						{
							resource: "camgrpid",
							url: "/cameragrp_list",
							method: "post",
							display: "camgrps.name",
							value: "camgrps.camgrpid",
						},
					],
				},
				{
					builder: "stringBuilder",
					key: "enrolledId",
					description: "Identifier of the enrolled person.",
					required: false,
				},
				{
					builder: "stringBuilder",
					key: "employeeId",
					description:
						"Company identifier of the enrolled person.",
					required: false,
				},
			],
		},
		{
			builder: "arrayBuilder",
			key: "mergeInstruction",
			description: "Instructions for the merge.",
			required: true,
			items: [
				{
					builder: "stringBuilder",
					key: "key",
					description: "Name of the key to retrieve the merged value",
					required: true,
				},
				{
					builder: "stringBuilder",
					key: "value",
					description: "Array of values to merge",
					required: true,
				},
			],
		},
		{
			builder: "booleanBuilder",
			description:
				"True if the incident instance requires a verification",
			key: "requiresVerification",
			required: false,
		},
		{
			builder: "enumBuilder",
			description: "Status of the incident",
			key: "status",
			required: false,
			enum: [0, 1],
		},
		{
			builder: "jsonBuilder",
			description: "The signal structure to wait for.",
			key: "match",
			required: true,
		},
	],
};

export { output as default }