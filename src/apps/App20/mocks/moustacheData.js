const data = [
    '{{ input.a }}',
    '{{ input.b }}',
    '{{ output.a }}',
    '{{ output.b }}',
    '{{ process1.p }}',
    '{{ process1.q }}',
    '{{ process2.p }}',
    '{{ process2.q }}'
]

export { data as default }