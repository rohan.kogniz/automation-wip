import { render, screen, fireEvent } from '@testing-library/react';
import Factory from "modules/Experimental/components/AutomationDialog/ViewBuilder";
import data1 from "./mocks/data";
import data2 from "./mocks/moustacheData";

test('dummy', () => {
    const utils = render(<div data-testid='something'><input data-testid='inputter' /></div>)
    // render(<Factory viewSchema={data1} moustache={data2} handleGetValue={()=>null} />)
    // screen.debug()
    utils.getByTestId('something')
    const input = utils.getByTestId('inputter')
    fireEvent.change(input, {target: {value: '123'}})
    console.log(input.value)

})