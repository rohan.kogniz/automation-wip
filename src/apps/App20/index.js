import React, { useState } from "react";
import data1 from "./mocks/data";
import data2 from "./mocks/moustacheData";
import Factory from "modules/Experimental/components/AutomationDialog/ViewBuilder";

const Foo = () => {
	const p = {
		rootNode: {
			key: "abcdef",
			key1: "A",
			nIteration: 8,
			requiresVerification: true,
			status: 0,
			match: {
				a: 'abc',
				j: 5
			},
			parameters: {
				path: {
					countryId: "Canada"
				}
			},
			mergeInstruction: [{
				key: "hello",
				value: "world"
			}],
			condition: {
				type: "GroupCondition",
				config: {
					operator: "OR",
					conditions: [
						{
							type: "GroupCondition",
							config: {
								operator: "AND",
								conditions: [
									{
										type: "FilterCondition",
										config: {
											left: {
												type: "CountFunctionDef",
												config: {
													input: "{{pqr}}",
												},
											},
											condition: "=",
											right: 0,
										},
									},
									{
										type: "FilterCondition",
										config: {
											left: {
												type: "CountFunctionDef",
												config: {
													input: "{{rst}}",
												},
											},
											condition: "=",
											right: 0,
										},
									},
								],
							},
						},
						{
							type: "GroupCondition",
							config: {
								operator: "AND",
								conditions: [
									{
										type: "FilterCondition",
										config: {
											left: {
												type: "CountFunctionDef",
												config: {
													input: "{{uvw}}",
												},
											},
											condition: "=",
											right: 0,
										},
									},
									{
										type: "FilterCondition",
										config: {
											left: {
												type: "CountFunctionDef",
												config: {
													input: "{{xyz}}",
												},
											},
											condition: "=",
											right: 0,
										},
									},
								],
							},
						},
					],
				},
			}
		}
	}

	let [value, setValue] = useState(p);

	const handleSetValue = (keyValue) => {
		console.log('VALUE', keyValue);
		setValue({
			...value,
			...keyValue
		});
	};

	const handleGetValue = (key) => {
		return value[key];
	};

	return (
		<Factory
			viewSchema={data1}
			setValue={handleSetValue}
			getValue={handleGetValue}
			moustache={data2}
		/>
	);
};

export { Foo as default };
