import React from 'react'
import Ajv from 'ajv'
import data1 from '../../modules/Experimental/helpers/mocks/incident_management_create_many_instances'
import data2 from '../../modules/Experimental/helpers/mocks/trigger'

const ajv = new Ajv()
const App = () => {
    const schema = data2[0] 
    const sample = data2[2]

    const validate = ajv.compile(schema)
    const valid = validate(sample)
    console.log('valid', valid)
    return (<div>Hello world</div>)
}

export default App;