import React from "react";
import styles from "./style.module.scss";
import classnames from "classnames";
import colorSystem from "core/ColorSystem/style.module.scss";

const IconButton = ({
	renderIcon,
	onClick,
	variant = "default",
	className,
	onMouseDown,
}) => {
	
	const containerClass = classnames({
		[styles.button]: true,
		[styles.defaultVariant]: variant === "default",
		[styles.containedVariant]: variant === "contained",
		[className]: className
	});

	const penColor = colorSystem.primaryColor; 

	return (
		<button
			type="button"
			onClick={onClick}
			className={containerClass}
			onMouseDown={onMouseDown}
		>
			<span>{renderIcon({ size: 24, fill: penColor })}</span>
		</button>
	);
};

export { IconButton as default };
