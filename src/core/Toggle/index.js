import React, { useEffect, useState } from "react";
import classname from "classnames";
import styles from "./style.module.scss";

const Toggle = React.forwardRef(({ checked, onChange, className }, ref) => {
	let [boolValue, setBoolValue] = useState(checked);

	useEffect(() => {
		onChange && onChange(boolValue);
	}, [onChange, boolValue]);

	const handleOnChange = () => {
		setBoolValue(!boolValue);
	};

	const containerClass = classname({
		[styles.container]: true,
		[className]: classname,
	});

	return (
		<div className={containerClass} onClick={handleOnChange}>
			<input
				ref={ref}
				type="checkbox"
				checked={boolValue}
				onChange={() => {}}
			></input>
			<label></label>
		</div>
	);
});

export { Toggle as default };
