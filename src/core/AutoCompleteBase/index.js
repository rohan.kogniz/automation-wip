import React from "react";
import styles from "./style.module.scss";
import Divider from "../Divider";
import classname from "classnames";


const AutoCompleteBase = ({
	open,
	groupBy,
	options,
	className,
	renderInput,
	renderListItem,
	renderListHeader,
}) => {
	const renderNoGroup = () => {
		return options.map((el, index) => {
			return (
				<React.Fragment key={index}>
					{renderListItem(el, index)}
				</React.Fragment>
			);
		});
	};

	const renderGroupBy = () => {
		var prevGroup;
		return options.map((el, index) => {
			let group = groupBy(el);
			let first = index === 0 ? true : false;
			if (group !== prevGroup) {
				prevGroup = group;
				return (
					<React.Fragment key={index}>
						{!first && <Divider />}
						{renderListHeader(el, index)}
						{renderListItem(el, index)}
					</React.Fragment>
				);
			} else {
				prevGroup = group;
				return (
					<React.Fragment key={index}>
						{renderListItem(el, index)}
					</React.Fragment>
				);
			}
		});
	};

	// prevent the input from losing focus (i.e onBlur from being called).
	// this relies on the assumption that onMouseEvent always occure before
	// onBlur
	const handleMouseDown = (e) => {
		e.preventDefault();
	};

	const containerClass = classname({
		[styles.container]: true,
		[className]: className
	});

	return (
		<div className={containerClass}>
			{renderInput({})}
			{open && (
				<div className={styles.popper} onMouseDown={handleMouseDown}>
					{groupBy ? renderGroupBy() : renderNoGroup()}
				</div>
			)}
		</div>
	);
};

export { AutoCompleteBase as default };
