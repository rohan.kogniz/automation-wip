import React from 'react'
import styles from './style.module.scss'

const ListItem = ( { data, title, onClick }) => {
    const handleClick = () => {
        onClick && onClick(data)
    }

    return (
        <div className={styles.container} onClick={handleClick}>
            <span>{ title }</span>
        </div>
    )
}

export { ListItem as default }