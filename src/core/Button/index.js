import React from "react";
import style from "./style.module.scss";
import PropTypes from 'prop-types'
import classname from "classnames";
import colorSystem from "core/ColorSystem/style.module.scss";

const Button = ({
	variant = "contained",
	onClick,
	children,
	className,
	renderBeginAdornment,
	...passthroughtProps
}) => {
	const buttonClass = classname({
		[className]: className,
		[style.button]: true,
		[style.containedVariant]: variant === "contained",
		[style.outlinedVariant]: variant === "outlined",
		[style.textVariant]: variant === "text"
	});

	const penColor =
		variant === "contained"
			? colorSystem.onPrimaryColor
			: colorSystem.primaryColor;

	return (
		<button
			className={buttonClass}
			onClick={onClick}
			{...passthroughtProps}
		>
			<div className={style.container}>
				{renderBeginAdornment && (
					<div className={style.beginAdornment}>
						{renderBeginAdornment({ size: 18, fill: penColor })}
					</div>
				)}
				<div className={style.label}>{children}</div>
			</div>
		</button>
	);
};

Button.propTypes = {
	variant: PropTypes.oneOf(['contained', 'outlined', 'text']),
	onClick: PropTypes.func,
	children: PropTypes.string,
	className: PropTypes.string,
	renderBeginAdornment: PropTypes.func
}

export { Button as default };
