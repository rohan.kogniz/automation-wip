import React from 'react'

const PATH = 'M7 10l5 5 5-5z'

const pinStyle = {
    cursor: 'pointer',
    fill: '#f00',
    stroke: 'none'
};

const Icon = ({ size, fill }) => {
    return (
        <svg
            height={size}
            style={{...pinStyle, fill}}
            viewBox='0 0 24 24'>
            <path d={PATH}>
            </path>
        </svg>
    )
}

export { Icon as default }