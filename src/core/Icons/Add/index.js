import React from 'react'

const PATH = `M 11 2 L 11 11 L 2 11 L 2 13 L 11 13 L 11 22 L 13 22 L 13 13 L 22 13 L 22 11 L 13 11 L 13 2 Z`

const pinStyle = {
    cursor: 'pointer',
    fill: '#f00',
    stroke: 'none'
};

const Icon = ({ size, fill }) => {
    return (
        <svg
            height={size}
            style={{...pinStyle, fill}}
            viewBox='0 0 24 24'>
            <path d={PATH}>
            </path>
        </svg>
    )
}

export { Icon as default }