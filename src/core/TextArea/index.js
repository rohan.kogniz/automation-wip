import React, { useEffect, useState } from "react";
import styles from "./style.module.scss";
import classname from "classnames";
import PropTypes from "prop-types";

// https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/

const TextArea = React.forwardRef(
	(
		{
			rows,
			cols,
			value,
			margin,
			onBlur,
			onFocus,
			variant = 'filled',
			onKeyUp,
			onChange,
			className,
			onMouseDown,
			autoAdjustHeight,
			...passthroughProps
		},
		ref
	) => {
		let [focus, setFocus] = useState(false);
		let [replicatedValue, setReplicatedValue] = useState(value);

		useEffect(() => {
			// add the whitespace to prevent jumping
			setReplicatedValue(value + " ");
		}, [value]);

		const handleFocus = (e) => {
			setFocus(true);
			onFocus && onFocus(e);
		};

		const handleBlur = (e) => {
			setFocus(false);
			onBlur && onBlur(e);
		};

		const handleOnInput = (e) => {
			// add the whitespace to prevent jumping
			setReplicatedValue(e.target.value + " ");
		};

		const handleMouseDown = (e) => {
			onMouseDown && onMouseDown(e);
		};

		const containerClass = classname({
			[styles.container]: true,
			[styles.dense]: margin === "dense",
			[styles.filledVariant]: variant === "filled",
			[styles.outlinedVariant]: variant === "outlined",
		});

		const textAreaClass = classname({
			[styles.textArea]: true,
			focus,
			[className]: className,
			[styles.noResize]: autoAdjustHeight,
		});

		const textAreaReplicaClass = classname({
			[styles.textAreaReplica]: true,
			focus,
			[className]: className,
		});

		const showUnderline = (margin === "normal" && variant === "filled");

		return (
			<div className={containerClass}>
				{showUnderline && <div className={styles.underLine} />}
				<textarea
					ref={ref}
					rows={autoAdjustHeight ? 1 : rows}
					cols={cols}
					value={value}
					onBlur={handleBlur}
					onInput={handleOnInput}
					onFocus={handleFocus}
					onChange={onChange}
					onKeyUp={onKeyUp}
					className={textAreaClass}
					onMouseDown={handleMouseDown}
					{...passthroughProps}
				/>
				{/* the following element used to automatically adjust height */}

				{autoAdjustHeight && (
					<div className={textAreaReplicaClass}>
						{replicatedValue}
					</div>
				)}
			</div>
		);
	}
);

TextArea.propTypes = {
	rows: PropTypes.number,
	cols: PropTypes.number,
	value: PropTypes.string,
	margin: PropTypes.oneOf(["normal", "dense"]),
	onBlur: PropTypes.func,
	variant: PropTypes.oneOf(["filled", "outlined"]),
	onFocus: PropTypes.func,
	onKeyUp: PropTypes.func,
	onChange: PropTypes.func,
	className: PropTypes.string,
	onMouseDown: PropTypes.func,
	autoAdjustHeight: PropTypes.bool,
};

export { TextArea as default };
