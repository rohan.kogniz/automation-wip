import React from "react";
import style from "./style.module.scss";
import PropTypes from "prop-types";
import classnames from "classnames";

const Chip = ({ variant = "contained", label, onClick }) => {
	const componentClass = classnames({
		[style.component]: true,
		[style.containedVariant]: variant === "contained",
		[style.outlinedVariant]: variant === "outlined",
	});
	return (
		<button className={componentClass} onClick={onClick}>
			{label}
		</button>
	);
};

Chip.propType = {
	variant: PropTypes.oneOf(["contained", "outlined"]),
};
export default Chip;
