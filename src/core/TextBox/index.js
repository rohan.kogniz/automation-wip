import React, { useState } from "react";
import styles from "./style.module.scss";
import PropTypes from "prop-types";
import classname from "classnames";

const TextBox = React.forwardRef(
	(
		{
			type = "text",
			value,
			margin = "normal",
			onBlur,
			onFocus,
			variant = "filled",
			onChange,
			className,
			onMouseDown,
			renderEndAdornment,
			...passthroughProps
		},
		ref
	) => {
		let [focus, setFocus] = useState(false);

		const handleFocus = (e) => {
			setFocus(true);
			onFocus && onFocus(e);
		};

		const handleBlur = (e) => {
			setFocus(false);
			onBlur && onBlur(e);
		};

		const handleMouseDown = (e) => {
			onMouseDown && onMouseDown(e);
		};

		const containerClass = classname({
			[styles.container]: true,
			[styles.dense]: margin === "dense",
			[styles.filledVariant]: variant === "filled",
			[styles.outlinedVariant]: variant === "outlined",
			focus,
			[className]: className,
		});

		const showUnderline = (margin === "normal" && variant === "filled");

		return (
			<div className={containerClass}>
				{showUnderline && <div className={styles.underLine} />}
				<input
					ref={ref}
					type={type}
					value={value}
					onBlur={handleBlur}
					onFocus={handleFocus}
					onChange={onChange}
					onMouseDown={handleMouseDown}
					{...passthroughProps}
				/>
				{renderEndAdornment && (
					<div className={styles.endAdornment}>
						{renderEndAdornment()}
					</div>
				)}
			</div>
		);
	}
);

TextBox.propTypes = {
	type: PropTypes.oneOf(["text", "number"]),
	value: PropTypes.string,
	margin: PropTypes.oneOf(["normal", "dense"]),
	onBlur: PropTypes.func,
	variant: PropTypes.oneOf(["filled", "outlined"]),
	onFocus: PropTypes.func,
	onChange: PropTypes.func,
	className: PropTypes.string,
	onMouseDown: PropTypes.func,
	renderEndAdornment: PropTypes.func,
};

export { TextBox as default };
